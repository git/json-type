# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

TARGETS = default test clean allclean all litex test-litex

.PHONY: ${TARGETS}

default all:
	cd lib && $(MAKE) $@
	cd src && $(MAKE) $@
	cd test/libs && ${MAKE} $@

clean allclean:
	cd lib && $(MAKE) $@
	cd src && $(MAKE) $@
	cd test/libs && ${MAKE} $@

test:
	cd test && ./test.sh

litex:
	cd lib && ${MAKE} $@
	cd test/libs && ${MAKE} $@

test-litex:
	cd test && \
	JSON_LITEX_TEST_PCRE2_LIB="${PCRE2_LIB}" \
	./test-litex.sh


