#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

libpcre2-version()
{
    set +o pipefail &&
    "${2:-gcc}" -Xlinker --verbose -L "${1:-/usr/local/lib}" -lpcre2-8 2>/dev/null|
    sed -nr '
        /^-lpcre2-8\s*\((.*)\)\s*$/!b
        s//\1/
        s/^\s+//
        s/\s+$//
        p
        q'|
    xargs strings|
    sed -nr '
        /^([0-9]{2}\.[0-9]{2})\s+([0-9]{4}-[0-9]{2}-[0-9]{2})$/!b
        s//\1 \2/
        p'|
    awk -F '\n' '{
        if (match($0, /^([0-9]+)\.([0-9]+) ([0-9]+)-([0-9]+)-([0-9]+)$/, a) &&
            mktime(a[3] " " a[4] " " a[5] " 00 00 00") != -1) {
            printf("%d%02d\n", a[1], a[2])
            exit 0
        }
    }'
}

libpcre2-version-check()
{
    local p="$1"
    local v="$2"
    local g="$3"

    local V
    V="$(libpcre2-version "$p" ${g:+"$g"})" && [ -n "$V" ] || {
        echo >&2 "error: cannot determine the version of PCRE2"
        return 1
    }
    [ "$v" -gt "$V" ] && {
        echo >&2 "error: expected PCRE2 version newer than $v: got $V"
        return 1
    }

    return 0
}

