# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

.PHONY:: default clean allclean all depend silent

default: all

# ensure the top makefile runs out of its home dir

file-id = $(shell stat -Lc %m:%i '$1')

WORK_DIR := $(realpath $(PWD))
MAKE_DIR := $(dir $(realpath $(firstword ${MAKEFILE_LIST})))

ifneq ($(call file-id,${MAKE_DIR}),$(call file-id,${WORK_DIR}))
$(error makefile '$(firstword ${MAKEFILE_LIST})' \
	is not running out of its home directory)
endif

# ensure the existence of prerequisite variables

ifndef SRCS
$(error SRCS is not defined)
endif

ifndef CFLAGS
$(error CFLAGS is not defined)
endif

ifndef BIN
$(error BIN is not defined)
endif

ifdef OBJS
ifndef LDFLAGS
$(error LDFLAGS is not defined, yet OBJS is)
endif
endif

# verify external parameters and
# update dependent variables

ifdef OPT
ifneq ($(words ${OPT}),1)
$(error invalid OPT='${OPT}')
endif
ifneq ($(filter-out 0 1 2 3,${OPT}),)
$(error invalid OPT='${OPT}')
endif
CFLAGS += -O$(strip ${OPT})
endif

ifdef 32BIT
ifneq ($(words ${32BIT}),1)
$(error invalid 32BIT='${32BIT}')
endif
ifneq ($(filter-out no yes,${32BIT}),)
$(error invalid 32BIT='${32BIT}')
endif
ifeq ($(strip ${32BIT}),yes)
CFLAGS += -m32
LDFLAGS += -m32
endif
endif

ifdef SANITIZE
ifneq ($(words ${SANITIZE}),1)
$(error invalid SANITIZE='${SANITIZE}')
endif
ifneq ($(filter-out undefined address,${SANITIZE}),)
$(error invalid SANITIZE='${SANITIZE}')
endif
CFLAGS += -fsanitize=$(strip ${SANITIZE})
LDFLAGS += -fsanitize=$(strip ${SANITIZE})
endif

ifdef COVERAGE
ifneq ($(words ${COVERAGE}),1)
$(error invalid COVERAGE='${COVERAGE}')
endif
ifneq ($(filter-out no yes,${COVERAGE}),)
$(error invalid COVERAGE='${COVERAGE}')
endif
ifeq ($(strip ${COVERAGE}),yes)
CFLAGS += --coverage
ifdef LDFLAGS
LDFLAGS += --coverage
endif
endif
endif

ifdef SILENT
ifneq ($(words ${SILENT}),1)
$(error invalid SILENT='${SILENT}')
endif
ifneq ($(filter-out no yes,${SILENT}),)
$(error invalid SILENT='${SILENT}')
endif
endif

# common variables

GCC := gcc

ifeq ($(strip ${COVERAGE}),yes)
GCOVS := $(patsubst %.c,%.gcno, ${SRCS}) \
			$(patsubst %.c,%.gcda, ${SRCS})
else
GCOVS :=
endif

# dependency rules

ifdef DEP
.PHONY:: depend-${DEP}
ifeq (.depend-${DEP}, $(wildcard .depend-${DEP}))
include .depend-${DEP}
endif
else
ifeq (.depend, $(wildcard .depend))
include .depend
endif
endif

ifdef OBJS
${BIN}: ${OBJS}
else
${BIN}: ${SRCS}
endif

ifeq ($(strip ${SILENT}),yes)
${BIN}: silent
endif

# building rules

%.o: %.c
	${GCC} ${CFLAGS} -c $< -o $@ 

${BIN}:
ifndef OBJS
	${GCC} ${CFLAGS} ${SRCS} -o $@ 
else ifdef LIBS
	${GCC} ${LDFLAGS} ${LIBS} $^ -o $@
else
	${GCC} ${LDFLAGS} $^ -o $@
endif

# main targets

all:: ${BIN}

ifdef OBJS
clean::
	rm -f ${OBJS}

allclean:: clean
endif

allclean::
ifdef GCOVS
	rm -f *~ ${GCOVS} ${BIN}
else
	rm -f *~ ${BIN}
endif

depend::
ifdef DEP
	${GCC} ${CFLAGS} -c ${SRCS} -MM| \
	sed -r 's/^[^ \t]+\.o:/${BIN}:/' > .depend-${DEP}
else
	${GCC} ${CFLAGS} -c ${SRCS} -MM > .depend
endif

silent:


