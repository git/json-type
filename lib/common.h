// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __COMMON_H
#define __COMMON_H

#include <sys/time.h>
#include <stdbool.h>

#include "json-defs.h"

#ifndef __GNUC__
#error we need a GCC compiler
#endif

#define PRAGMA_(s) \
    _Pragma(#s)
#define PRAGMA_DIAG_(s) \
    PRAGMA_(GCC diagnostic s)
#define PRAGMA_DIAG_POP_() \
    PRAGMA_DIAG_(pop)
#define PRAGMA_DIAG_PUSH_(s) \
    PRAGMA_DIAG_(push) \
    PRAGMA_DIAG_(s)
#define PRAGMA_DIAG_PUSH_IGNORED_(s) \
    PRAGMA_DIAG_PUSH_(ignored s)

#if GCC_VERSION >= 70000
#define PRAGMA_DIAG_PUSH_IGNORED_IMPLICIT_FALLTHROUGH \
    PRAGMA_DIAG_PUSH_IGNORED_("-Wimplicit-fallthrough")
#define PRAGMA_DIAG_POP_IGNORED_IMPLICIT_FALLTHROUGH \
    PRAGMA_DIAG_POP_()
#else
#define PRAGMA_DIAG_PUSH_IGNORED_IMPLICIT_FALLTHROUGH
#define PRAGMA_DIAG_POP_IGNORED_IMPLICIT_FALLTHROUGH
#endif

#define UNUSED    __attribute__((unused))
#define PRINTF(F) __attribute__((format(printf, F, F + 1)))
#define NORETURN  __attribute__((noreturn))

#define STRINGIFY_(s) #s
#define STRINGIFY(s)  STRINGIFY_(s)

// stev: important requirement: VERIFY evaluates E only once!

#define VERIFY(E)             \
    do {                      \
        if (!(E))             \
            UNEXPECT_ERR(#E); \
    }                         \
    while (0)

// stev: important requirement: ENSURE evaluates E only once!

#define ENSURE(E, M, ...)                     \
    do {                                      \
        if (!(E))                             \
            ensure_failed(__FILE__, __LINE__, \
                __func__, M, ## __VA_ARGS__); \
    }                                         \
    while (0)

#define UNEXPECT_ERR(M, ...)               \
    do {                                   \
        unexpect_error(__FILE__, __LINE__, \
            __func__, M, ## __VA_ARGS__);  \
    }                                      \
    while (0)

#define UNEXPECT_VAR(F, N) UNEXPECT_ERR(#N "=" F, N)

#define INVALID_ARG(F, N)                    \
    do {                                     \
        invalid_argument(__FILE__, __LINE__, \
            __func__, #N, F, N);             \
    }                                        \
    while (0)

#define NOT_YET_IMPL()                   \
    do {                                 \
        not_yet_impl(__FILE__, __LINE__, \
            __func__);                   \
    }                                    \
    while (0)

#ifdef JSON_DEBUG
# define ASSERT(E)                            \
    do {                                      \
        if (!(E))                             \
            assert_failed(__FILE__, __LINE__, \
                __func__, #E);                \
    }                                         \
    while (0)
#else
# define ASSERT(E) \
    do {} while (0)
#endif

#if !CONFIG_ERROR_FUNCTION_ATTRIBUTE
#error we need GCC to support the 'error' function attribute
#else
#define STATIC(E)                                   \
    ({                                              \
        extern int __attribute__                    \
            ((error("assertion failed: '" #E "'"))) \
            static_assert();                        \
        (void) ((E) ? 0 : static_assert());         \
    })
#endif

void fatal_error(const char* fmt, ...)
    PRINTF(1)
    NORETURN;

void ensure_failed(
    const char* file, int line, const char* func, const char* msg, ...)
    PRINTF(4)
    NORETURN;

void assert_failed(
    const char* file, int line, const char* func, const char* expr)
    NORETURN;

void unexpect_error(
    const char* file, int line, const char* func, const char* msg, ...)
    PRINTF(4)
    NORETURN;

void invalid_argument(
    const char* file, int line, const char* func,
    const char* name, const char* msg, ...)
    PRINTF(5)
    NORETURN;

void not_yet_impl(
    const char* file, int line, const char* func)
    NORETURN;

struct json_lib_version_t
{
    unsigned short major;
    unsigned short minor;
    unsigned short patch;
};

struct json_lib_version_t json_lib_version(
    size_t);

bool json_timeval_get_time(
    struct timeval*);

size_t json_timeval_subtract(
    const struct timeval*,
    const struct timeval*);

size_t json_timeval_to_usecs(
    const struct timeval*);

bool json_get_current_time(
    char* buf, size_t len);

size_t json_text_count(
    const uchar_t* buf, size_t len,
    uchar_t ch);

#define json_text_pos_t json_error_pos_t

struct json_text_pos_t;

size_t json_text_update_pos(
    struct json_text_pos_t* pos,
    const uchar_t* buf,
    size_t len);

#endif/*__COMMON_H*/

