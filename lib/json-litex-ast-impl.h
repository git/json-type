// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef JSON_LITEX_AST_VISITOR_NAME
#error  JSON_LITEX_AST_VISITOR_NAME is not defined
#endif

#ifndef JSON_LITEX_AST_VISITOR_RESULT_TYPE
#error  JSON_LITEX_AST_VISITOR_RESULT_TYPE is not defined
#endif

#ifndef JSON_LITEX_AST_VISITOR_OBJECT_TYPE
#error  JSON_LITEX_AST_VISITOR_OBJECT_TYPE is not defined
#endif

#define JSON_LITEX_AST_VISITOR_MAKE_NAME__(n, s) n ## s
#define JSON_LITEX_AST_VISITOR_MAKE_NAME_(n, s)  \
        JSON_LITEX_AST_VISITOR_MAKE_NAME__(n, s)
#define JSON_LITEX_AST_VISITOR_MAKE_NAME(s) \
        JSON_LITEX_AST_VISITOR_MAKE_NAME_(  \
            JSON_LITEX_AST_VISITOR_NAME, s)

#define JSON_LITEX_AST_VISITOR_TYPE_NAME \
        JSON_LITEX_AST_VISITOR_MAKE_NAME(_visitor_t)

#define JSON_LITEX_AST_VISITOR_VISIT_FUNC_NAME \
        JSON_LITEX_AST_VISITOR_MAKE_NAME(_visit)

#define JSON_LITEX_AST_VISITOR_FUNC_TYPE_NAME(n) \
        JSON_LITEX_AST_VISITOR_MAKE_NAME(_ ## n)

#define JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_(t, n, o)   \
    static JSON_LITEX_AST_VISITOR_RESULT_TYPE            \
        JSON_LITEX_AST_VISITOR_FUNC_TYPE_NAME(n)(        \
            JSON_LITEX_AST_VISITOR_OBJECT_TYPE* this,    \
            json_litex_expr_ast_ ## t ## _node_t o node)
#define JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_REF(t, n) \
        JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_(t, n, *)
#define JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(t, n) \
        JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_(t, n, )

JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(btv, const_id);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(num, const_num);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(str, const_str);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(str, count_str);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(rex, count_rex);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(str, match_str);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL(rex, match_rex);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_REF(btf, call_builtin);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_REF(bin, bin_op);
JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_REF(not, not_op);

#define JSON_LITEX_AST_VISITOR_CALL_(n, o)        \
    (                                             \
        JSON_LITEX_AST_VISITOR_FUNC_TYPE_NAME(n)( \
            obj, o(node->n))                      \
    )
#define JSON_LITEX_AST_VISITOR_CALL_REF(n) \
        JSON_LITEX_AST_VISITOR_CALL_(n, &)
#define JSON_LITEX_AST_VISITOR_CALL_VAL(n) \
        JSON_LITEX_AST_VISITOR_CALL_(n, )

static JSON_LITEX_AST_VISITOR_RESULT_TYPE
    JSON_LITEX_AST_VISITOR_VISIT_FUNC_NAME(
        JSON_LITEX_AST_VISITOR_OBJECT_TYPE* obj,
        struct json_litex_expr_ast_node_t* node)
{
    VERIFY(node != NULL);

    switch (node->type) {
    case json_litex_expr_ast_node_const_id:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(const_id);
    case json_litex_expr_ast_node_const_num:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(const_num);
    case json_litex_expr_ast_node_const_str:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(const_str);
    case json_litex_expr_ast_node_count_str:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(count_str);
    case json_litex_expr_ast_node_count_rex:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(count_rex);
    case json_litex_expr_ast_node_match_str:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(match_str);
    case json_litex_expr_ast_node_match_rex:
        return JSON_LITEX_AST_VISITOR_CALL_VAL(match_rex);
    case json_litex_expr_ast_node_call_builtin:
        return JSON_LITEX_AST_VISITOR_CALL_REF(call_builtin);
    case json_litex_expr_ast_node_bin_op:
        return JSON_LITEX_AST_VISITOR_CALL_REF(bin_op);
    case json_litex_expr_ast_node_not_op:
        return JSON_LITEX_AST_VISITOR_CALL_REF(not_op);
    default:
        UNEXPECT_VAR("%d", node->type);
    }
}

#undef JSON_LITEX_AST_VISITOR_MAKE_NAME
#undef JSON_LITEX_AST_VISITOR_MAKE_NAME_
#undef JSON_LITEX_AST_VISITOR_MAKE_NAME__
#undef JSON_LITEX_AST_VISITOR_FUNC_TYPE_NAME
#undef JSON_LITEX_AST_VISITOR_TYPE_NAME
#undef JSON_LITEX_AST_VISITOR_VISIT_FUNC_NAME
#undef JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_VAL
#undef JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_REF
#undef JSON_LITEX_AST_VISITOR_FUNC_TYPE_DEF_
#undef JSON_LITEX_AST_VISITOR_CALL_REF
#undef JSON_LITEX_AST_VISITOR_CALL_VAL
#undef JSON_LITEX_AST_VISITOR_CALL_


