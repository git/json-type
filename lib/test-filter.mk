# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

PROGRAM := test-filter.so

GCC_STD := gnu99
CFLAGS := -Wall -Wextra \
          -std=${GCC_STD} -g -I. -I.. \
          -fPIC -fvisibility=hidden -shared \
          -fdata-sections -ffunction-sections -Wl,--gc-sections \
          -Wl,--entry=test_filter_main \
          -DPROGRAM=${PROGRAM} \
          -DJSON_NO_LIB_MAIN \
          -DJSON_DEBUG

SRCS := common.c \
        test-filter.c

DEP  := test-filter
BIN  := ${PROGRAM}

include common.mk


