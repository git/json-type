// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __JSON_FILTER_H
#define __JSON_FILTER_H

#include "json.h"

#define JSON_FILTER_VERSION_MAJOR 0
#define JSON_FILTER_VERSION_MINOR 1
#define JSON_FILTER_VERSION_PATCH 0

#define JSON_FILTER_VERSION                 \
    (                                       \
        JSON_FILTER_VERSION_MAJOR * 10000 + \
        JSON_FILTER_VERSION_MINOR * 100 +   \
        JSON_FILTER_VERSION_PATCH           \
    )

struct json_filter_options_t
{
    size_t       argc;
    char* const* argv;
};

struct json_handler_obj_t
{
    const struct json_handler_t*
                      handler;
    void*             ptr;
};

typedef size_t (*json_filter_get_version_func_t)(void);

typedef void* (*json_filter_create_filter_func_t)(
    const struct json_filter_options_t* opts,
    const struct json_handler_obj_t* handler,
    const struct json_handler_t** filter);

typedef void* (*json_filter_create_exec_func_t)(
    const struct json_filter_options_t* opts);

typedef void (*json_filter_destroy_func_t)(void*);

typedef bool (*json_filter_get_is_error_func_t)(void*);

typedef struct json_error_pos_t
    (*json_filter_get_error_pos_func_t)(void*);

typedef const struct json_file_info_t*
    (*json_filter_get_error_file_func_t)(void*);

typedef void (*json_filter_print_error_desc_func_t)(void*, FILE*);

typedef bool (*json_filter_exec_func_t)(void*);

struct json_filter_funcs_t
{
    json_filter_get_version_func_t      get_version;
    json_filter_create_filter_func_t    create_filter;
    json_filter_create_exec_func_t      create_exec;
    json_filter_destroy_func_t          destroy;
    json_filter_get_is_error_func_t     get_is_error;
    json_filter_get_error_pos_func_t    get_error_pos;
    json_filter_get_error_file_func_t   get_error_file;
    json_filter_print_error_desc_func_t print_error_desc;
    json_filter_exec_func_t             exec;
};

#ifdef JSON_FILTER_NAME

#define JSON_FILTER_MAKE_NAME_(n, s) n ## _filter_ ## s
#define JSON_FILTER_MAKE_NAME(n, s)  JSON_FILTER_MAKE_NAME_(n, s)

#define JSON_FILTER_TYPE \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, t)
#define JSON_FILTER_GET_VERSION \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, get_version)
#define JSON_FILTER_CREATE_FILTER \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, create_filter)
#define JSON_FILTER_CREATE_EXEC \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, create_exec)
#define JSON_FILTER_DESTROY \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, destroy)
#define JSON_FILTER_GET_IS_ERROR \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, get_is_error)
#define JSON_FILTER_GET_ERROR_POS \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, get_error_pos)
#define JSON_FILTER_GET_ERROR_FILE \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, get_error_file)
#define JSON_FILTER_PRINT_ERROR_DESC \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, print_error_desc)
#define JSON_FILTER_EXEC \
        JSON_FILTER_MAKE_NAME(JSON_FILTER_NAME, exec)

JSON_API size_t JSON_FILTER_GET_VERSION(void);

JSON_API struct JSON_FILTER_TYPE* JSON_FILTER_CREATE_FILTER(
    const struct json_filter_options_t* opts,
    const struct json_handler_obj_t* handler,
    const struct json_handler_t** filter);

JSON_API struct JSON_FILTER_TYPE* JSON_FILTER_CREATE_EXEC(
    const struct json_filter_options_t* opts);

JSON_API void JSON_FILTER_DESTROY(struct JSON_FILTER_TYPE*);

JSON_API bool JSON_FILTER_GET_IS_ERROR(struct JSON_FILTER_TYPE*);

JSON_API struct json_error_pos_t JSON_FILTER_GET_ERROR_POS(
    struct JSON_FILTER_TYPE*);

JSON_API const struct json_file_info_t* JSON_FILTER_GET_ERROR_FILE(
    struct JSON_FILTER_TYPE*);

JSON_API void JSON_FILTER_PRINT_ERROR_DESC(struct JSON_FILTER_TYPE*,
    FILE*);

JSON_API bool JSON_FILTER_EXEC(struct JSON_FILTER_TYPE*);

#undef JSON_FILTER_EXEC
#undef JSON_FILTER_PRINT_ERROR_DESC
#undef JSON_FILTER_GET_ERROR_FILE
#undef JSON_FILTER_GET_ERROR_POS
#undef JSON_FILTER_GET_IS_ERROR
#undef JSON_FILTER_DESTROY
#undef JSON_FILTER_CREATE_EXEC
#undef JSON_FILTER_CREATE_FILTER
#undef JSON_FILTER_GET_VERSION
#undef JSON_FILTER_TYPE

#endif // JSON_FILTER_NAME

#endif /* __JSON_FILTER_H */

