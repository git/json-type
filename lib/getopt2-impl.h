// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GETOPT2_NAME
#define GETOPT2_MAKE_NAME(s)      s
#else
#define GETOPT2_MAKE_NAME__(n, s) n ## _ ## s
#define GETOPT2_MAKE_NAME_(n, s ) GETOPT2_MAKE_NAME__(n, s)
#define GETOPT2_MAKE_NAME(s)      GETOPT2_MAKE_NAME_(GETOPT2_NAME, s)
#endif

#ifdef  GETOPT2_NAME
#define GETOPT2_GETOPT_LONG       GETOPT2_MAKE_NAME(getopt_long)
#else
#define GETOPT2_GETOPT_LONG       GETOPT2_MAKE_NAME(getopt_long2)
#endif
#define GETOPT2_LOOKUP_LONG_OPT   GETOPT2_MAKE_NAME(lookup_long_opt)

static int GETOPT2_GETOPT_LONG(
    int argc, char* const argv[],
    const char* shorts, const struct option* longs,
    int* index)
{
    const size_t n = sizeof(struct option);

    static struct option l[2];
    char *p, *q;
    int k;

    ASSERT(argc > 0);
    ASSERT(argv != NULL);
    ASSERT(shorts != NULL);
    ASSERT(longs != NULL);

    if (optind < argc) {
        ASSERT(optind > 0);
        p = argv[optind];

        if (*p ++ == '-' && *p ++ == '-' && *p) {
            if ((q = strchr(p, '=')))
                *q = 0;

            if ((k = GETOPT2_LOOKUP_LONG_OPT(p)) >= 0) {
                memcpy(l, &longs[k], n);
                longs = l;
            }
            else
                longs = l + 1;

            if (q) *q = '=';
        }
    }

    return getopt_long(argc, argv, shorts, longs, index);
}

// stev: overwrite the C library function name:
#define getopt_long GETOPT2_GETOPT_LONG


