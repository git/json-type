# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# Grammar of Json-Litex's Literal Expressions
#

# Note: the all lowercase identifiers below
# are non-terminals while the all uppercase
# identifiers are terminal symbols.

# Lexical rules:

# The whitespace characters -- SPACE ' ', NL
# '\n', FF '\f', HT '\t', VT '\v' and CR '\r'
# -- can freely be used in expressions and
# are removed during tokenization when are
# not part of terminal symbols STR and REX.

# STR symbols start with '`'; any symbol of
# this kind that needs to contain that char
# has to escape each occurrence of it: i.e.
# it has to double that instance of '`'.

# For REX symbols -- which start with either
# character '/' or '\'' -- a similar escape
# rule applies: any REX that needs contain
# the character that it starts with has to
# double each instance of that char for to
# make that instance part of REX itself.

# Grammar rules:

expr : term ( "||" term )*
     ;

term : cmp ( "&&" cmp )*
     ;

cmp  : rel ( ( "==" | "!=" ) rel )*
     ;

rel  : prim ( ( "<" | ">" | "<=" | ">=" ) prim )*
     ;

prim : "!" prim
     | "(" expr ")"
     | "#" ( STR | REX )
     | ID [ "(" args ")" ]
     | STR
     | REX
     | NUM
     ;

args : [ arg ( "," arg )* ]
     ;

arg  : NUM
     | STR
     | ID
     ;

STR  : "`" ( [^`] | "``" )+ "`"
     ;

REX  : "/" ( [^/] | "//" )+ "/" OPT
     | "'" ( [^'] | "''" )+ "'" OPT
     ;

OPT  : [ISUX]*
     ;

ID   : [a-zA-Z_][a-zA-Z0-9_]*
     ;

NUM  : [0-9]+
     ;


