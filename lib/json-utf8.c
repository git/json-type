// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

#include "common.h"
#include "json-defs.h"
#include "int-traits.h"
#include "ptr-traits.h"

#include "json-utf8.h"

#define E(c) (c | json_utf8_esc_bit)
const uchar_t json_utf8_escapes[json_utf8_ascii_size] = {
    ['\x00'] = E(0),   ['\x01'] = E(0),    ['\x02'] = E(0),   ['\x03'] = E(0), 
    ['\x04'] = E(0),   ['\x05'] = E(0),    ['\x06'] = E(0),   ['\x07'] = E(0), 
    ['\x08'] = E('b'), ['\x09'] = E('t'),  ['\x0a'] = E('n'), ['\x0b'] = E(0), 
    ['\x0c'] = E('f'), ['\x0d'] = E('r'),  ['\x0e'] = E(0),   ['\x0f'] = E(0), 
    ['\x10'] = E(0),   ['\x11'] = E(0),    ['\x12'] = E(0),   ['\x13'] = E(0), 
    ['\x14'] = E(0),   ['\x15'] = E(0),    ['\x16'] = E(0),   ['\x17'] = E(0), 
    ['\x18'] = E(0),   ['\x19'] = E(0),    ['\x1a'] = E(0),   ['\x1b'] = E(0), 
    ['\x1c'] = E(0),   ['\x1d'] = E(0),    ['\x1e'] = E(0),   ['\x1f'] = E(0), 

    ['\x22'] = E('"'), ['\x5c'] = E('\\'), ['\x7f'] = E(0),
};
#undef E

#define JSON_UTF8_PRINT_CHAR(c, f)          \
    do {                                    \
        uchar_t __c;                        \
        STATIC(TYPEOF_IS(c, uchar_t));      \
        ASSERT((c) < json_utf8_ascii_size); \
        if (!(__c = json_utf8_escapes[c]))  \
            fputc(c, f);                    \
        else                                \
        if ((__c &= ~json_utf8_esc_bit))    \
            fprintf(f, "\\%c", __c);        \
        else                                \
            fprintf(f, "\\u%04x", c);       \
    } while (0)

enum json_code_point_type_t json_encode_utf8(
    struct json_surrogate_pair_t* surrogates,
    uchar_t* buf, size_t* len)
{
    uchar_t* p = buf;
    uint16_t c = 0;

    STATIC(TYPE_WIDTH(uchar_t) >= 8);

    for (; p < buf + 4; p ++) {
        ASSERT((*p & 0xf0u) == 0);
        c = (c << 4) | *p;
    }

    // stev: see the man page UTF-8(7)

    // stev: see Table 3-6, UTF-8 Bit Distribution
    // The Unicode Standard Version 8.0 - Core Specification, Chapter 3, p. 125
    // http://www.unicode.org/versions/Unicode8.0.0/ch03.pdf

    if (c >= 0xd800u && c <= 0xdbffu) {
        surrogates->high = c;
        return json_high_surrogate_code_point;
    }
    if (c >= 0xdc00u && c <= 0xdfffu) {
        surrogates->low = c;
        return json_low_surrogate_code_point;
    }

    p = buf;
    if (c < 0x80u)
        *p ++ = (uchar_t) c;  
    else
    if (c < 0x800u) {
        *p ++ = (uchar_t) ((c >> 6) | 0xc0u);
        *p ++ = (uchar_t) ((c & 0x3fu) | 0x80u);
    }
    else {
        *p ++ = (uchar_t) ((c >> 12) | 0xe0u);
        *p ++ = (uchar_t) (((c >> 6) & 0x3fu) | 0x80u);
        *p ++ = (uchar_t) ((c & 0x3fu) | 0x80u);
    }
    *len = PTR_DIFF(p, buf);

    return json_basic_plane_code_point;
}

void json_encode_utf8_surrogate_pair(
    const struct json_surrogate_pair_t* surrogates, uchar_t* buf)
{
    uint32_t c;

    ASSERT(
        surrogates->high >= 0xd800u &&
        surrogates->high <= 0xdbffu);
    ASSERT(
        surrogates->low  >= 0xdc00u &&
        surrogates->low  <= 0xdfffu);

    c = surrogates->high & 0x3ffu;
    c <<= 10;
    c += surrogates->low & 0x3ffu;
    c += 0x10000u;

    *buf ++ = (uchar_t) ((c >> 18) | 0xf0u);
    *buf ++ = (uchar_t) (((c >> 12) & 0x3fu) | 0x80u);
    *buf ++ = (uchar_t) (((c >> 6) & 0x3fu) | 0x80u);
    *buf ++ = (uchar_t) ((c & 0x3fu) | 0x80u);
}

// stev: json_validate_utf8

#define JSON_UTF8_VALIDATE_ERROR_(e) \
    do { *err = e; return false; } while (0)

#define JSON_UTF8_VALIDATE_CHR(c)         do {} while (0)
#define JSON_UTF8_VALIDATE_SEQ(n, p)      do {} while (0)
#define JSON_UTF8_VALIDATE_ERROR(n, b, p) JSON_UTF8_VALIDATE_ERROR_(PTR_DIFF(p, b))
#define JSON_UTF8_VALIDATE_ERROR_END(l)   JSON_UTF8_VALIDATE_ERROR_(l)

bool json_validate_utf8(
    const uchar_t* buf, size_t len, size_t* err)
{
#define JSON_UTF8_NEED_VALIDATE_ALGO
#include "json-utf8-impl.h"
#undef  JSON_UTF8_NEED_VALIDATE_ALGO

    return true;
}

#undef  JSON_UTF8_VALIDATE_ERROR_END
#undef  JSON_UTF8_VALIDATE_ERROR
#undef  JSON_UTF8_VALIDATE_SEQ
#undef  JSON_UTF8_VALIDATE_CHR

// stev: json_print_escaped_utf8

// The Unicode Standard Version 8.0 - Core Specification, Chapter 3,
// http://www.unicode.org/versions/Unicode8.0.0/ch03.pdf
//
// Best Practices for Using U+FFFD, pages 127-129

#define JSON_UTF8_VALIDATE_SEQ_INIT(p, n) \
    do {                                  \
        STATIC(TYPEOF_IS_UCHAR_PTR(p));   \
        STATIC(TYPEOF_IS_INTEGER(n));     \
        STATIC(IS_CONSTANT(n));           \
        STATIC(n > 0);                    \
        STATIC(n < 4);                    \
        esc.ptr = (p) - n;                \
        esc.point = (*esc.ptr ++) &       \
            ((SZ(1) << (SZ(7) - n)) - 1); \
    } while (0)

#define JSON_UTF8_VALIDATE_SEQ_DONE()         \
    do {                                      \
        esc.point <<= SZ(6);                  \
        esc.point |= (*esc.ptr ++) & ~0xc0u;  \
    contn2:                                   \
        esc.point <<= SZ(6);                  \
        esc.point |= (*esc.ptr ++) & ~0xc0u;  \
    contn1:                                   \
        esc.point <<= SZ(6);                  \
        esc.point |= (*esc.ptr) & ~0xc0u;     \
        /* stev: the surrogate code points */ \
        /* must have already been rejected */ \
        ASSERT(                               \
            esc.point < 0xd800u ||            \
            esc.point > 0xdfffu);             \
        if (esc.point >= 0x10000u) {          \
            if (!surrogates) {                \
                JSON_UTF8_VALIDATE_ERROR__(); \
                goto next_iter;               \
            }                                 \
            esc.point -= 0x10000u;            \
            fprintf(file, "\\u%04zx",         \
                0xd800u | (esc.point >> 10)); \
            esc.point &= 0x3ffu;              \
            esc.point |= 0xdc00u;             \
        }                                     \
        fprintf(file, "\\u%04zx", esc.point); \
    } while (0)

#define JSON_UTF8_VALIDATE_SEQ_GOTO_(n) \
    goto contn ## n
#define JSON_UTF8_VALIDATE_SEQ_1_() \
    JSON_UTF8_VALIDATE_SEQ_GOTO_(1)
#define JSON_UTF8_VALIDATE_SEQ_2_() \
    JSON_UTF8_VALIDATE_SEQ_GOTO_(2)
#define JSON_UTF8_VALIDATE_SEQ_3_() \
    JSON_UTF8_VALIDATE_SEQ_DONE()

#define JSON_UTF8_VALIDATE_SEQ_(p, n)        \
    do {                                     \
        JSON_UTF8_VALIDATE_SEQ_INIT(p, n);   \
        JSON_UTF8_VALIDATE_SEQ_ ## n ## _(); \
    } while (0)

#define JSON_UTF8_VALIDATE_ERROR__() \
    fputs("\\ufffd", file)

#undef  JSON_UTF8_VALIDATE_ERROR_
#define JSON_UTF8_VALIDATE_ERROR_(s)  \
    do {                              \
        JSON_UTF8_VALIDATE_ERROR__(); \
        s;                            \
    } while (0)
#define JSON_UTF8_VALIDATE_ERROR_GOTO_(l) \
    JSON_UTF8_VALIDATE_ERROR_(goto l ## _iter)

#define JSON_UTF8_VALIDATE_ERROR_0()      JSON_UTF8_VALIDATE_ERROR_GOTO_(next)
#define JSON_UTF8_VALIDATE_ERROR_1()      JSON_UTF8_VALIDATE_ERROR_GOTO_(begin)
#define JSON_UTF8_VALIDATE_ERROR_2()      JSON_UTF8_VALIDATE_ERROR_GOTO_(begin)
#define JSON_UTF8_VALIDATE_ERROR_3()      JSON_UTF8_VALIDATE_ERROR_GOTO_(begin)
#define JSON_UTF8_VALIDATE_ERROR_4()      JSON_UTF8_VALIDATE_ERROR__()

#define JSON_UTF8_VALIDATE_CHR(c)         JSON_UTF8_PRINT_CHAR(c, file)
#define JSON_UTF8_VALIDATE_SEQ(n, p)      JSON_UTF8_VALIDATE_SEQ_(p, n)
#define JSON_UTF8_VALIDATE_ERROR(n, b, p) JSON_UTF8_VALIDATE_ERROR_ ## n()
#define JSON_UTF8_VALIDATE_ERROR_END(l)   JSON_UTF8_VALIDATE_ERROR_(return)

void json_print_escaped_utf8(
    const uchar_t* buf, size_t len, bool surrogates, FILE* file)
{
    struct
    {
        const uchar_t* ptr;
        size_t point;
    } esc;

#define JSON_UTF8_NEED_VALIDATE_ALGO
#define JSON_UTF8_NEED_VALIDATE_ITER_LABELS
#include "json-utf8-impl.h"
#undef  JSON_UTF8_NEED_VALIDATE_ITER_LABELS
#undef  JSON_UTF8_NEED_VALIDATE_ALGO
}

#undef  JSON_UTF8_VALIDATE_ERROR_END
#undef  JSON_UTF8_VALIDATE_ERROR
#undef  JSON_UTF8_VALIDATE_SEQ
#undef  JSON_UTF8_VALIDATE_CHR


