// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "json.h"
#include "int-traits.h"
#include "ptr-traits.h"
#include "pretty-print.h"
#include "json-utf8.h"
#include "mem-buf.h"
#include "common.h"
#include "error.h"

#ifdef JSON_DEBUG
#define PRINT_DEBUG_COND esc->debug
#include "debug.h"
#endif

enum { debug_bits = 1 };

struct json_esc_t
{
    size_t                       config;
    size_t                       current;
    size_t                       contn;
    size_t                       point;
    uchar_t                      utf8[4];
    size_t                       n_utf8;
    struct json_esc_sizes_t      sizes;
    enum json_parse_status_t     status;
    struct mem_buf_t*            spaces;
    struct json_error_pos_t      pt_pos;
    struct json_error_pos_t      pos;
    struct json_esc_error_info_t error;
#ifdef JSON_DEBUG
    bits_t                       debug:
                                 debug_bits;
#endif
    bits_t                       init: 1;
    bits_t                       printed: 1;
    bits_t                       done: 1;
};

struct json_esc_t* json_esc_create(
    const struct json_esc_sizes_t* sizes)
{
    struct json_esc_t* esc;

    // stev: the config parameters accepted by
    // 'json_esc' must form a series as seen in
    // the STATIC assertion below; this property
    // is checked only once in this function
    STATIC(
        json_allow_surrogate_pairs_config + 1 <=
        json_validate_utf8_config &&

        json_validate_utf8_config + 1 <=
        json_trim_spaces_config &&

        json_trim_spaces_config + 1 <=
        json_quote_text_config);

    JSON_ESC_SIZES_VALIDATE(sizes);

    esc = malloc(sizeof(struct json_esc_t));
    ENSURE(esc != NULL, "malloc failed");

    memset(esc, 0, sizeof(struct json_esc_t));

    esc->sizes = *sizes;
    esc->pos.line = 1;
    esc->pos.col = 1;

#ifdef JSON_DEBUG
    esc->debug = SIZE_TRUNC_BITS(
        json_debug_get_level(json_debug_escape_class),
        debug_bits);
#endif

    return esc;
}

void json_esc_destroy(struct json_esc_t* esc)
{
    if (esc->spaces != NULL)
        mem_buf_destroy(esc->spaces);
    free(esc);
}

enum {
    ascii_size = json_utf8_ascii_size,
    esc_bit    = json_utf8_esc_bit
};

static const uchar_t json_esc_spaces[ascii_size] = {
    ['\f'] = 1, ['\n'] = 1, ['\r'] = 1, ['\t'] = 1, ['\v'] = 1, [' '] = 1
};

#define json_esc_escapes json_utf8_escapes

#define JSON_ESC_PRINT_CHAR(c)         \
    do {                               \
        uchar_t __c;                   \
        STATIC(TYPEOF_IS(c, uchar_t)); \
        __c = json_esc_escapes[c];     \
        ASSERT(__c & esc_bit);         \
        if ((__c &= ~esc_bit))         \
            printf("\\%c", __c);       \
        else                           \
            printf("\\u%04x", c);      \
    } while (0)

#define JSON_ESC_STATUS(s)                     \
    (                                          \
        esc->status = json_parse_status_ ## s  \
    )
#define JSON_ESC_STATUS_IS(s)                  \
    (                                          \
        esc->status == json_parse_status_ ## s \
    )
#define JSON_ESC_CURRENT_STATUS() (esc->status)

#define JSON_ESC_CURRENT_CONF(p)                       \
    (                                                  \
        (esc->current & (SZ(1) <<                      \
            (json_ ## p ## _config -                   \
             json_allow_surrogate_pairs_config))) != 0 \
    )

#define JSON_ESC_ERROR_(t, p)                   \
    ({                                          \
        esc->error.pos = p;                     \
        esc->error.type = json_esc_error_ ## t; \
        JSON_ESC_STATUS(error);                 \
    })
#define JSON_ESC_ERROR_PT(t) \
    JSON_ESC_ERROR_(t, esc->pt_pos)
#define JSON_ESC_ERROR(t) \
    JSON_ESC_ERROR_(t, esc->pos)

static void json_esc_print_spaces(struct json_esc_t* esc)
{
    ASSERT(JSON_ESC_CURRENT_CONF(trim_spaces));

    if (esc->printed &&
        esc->spaces->len) {
        const uchar_t *p, *e;

        for (p = esc->spaces->ptr,
             e = p + esc->spaces->len;
             p < e;
             p ++) {
            ASSERT(json_esc_spaces[*p]);

            if (*p == ' ')
                putchar(' ');
            else
                JSON_ESC_PRINT_CHAR(*p);
        }
    }

    mem_buf_update(
        esc->spaces, NULL, 0, mem_buf_reset);
}

#define printb(b, l)                    \
    (                                   \
        STATIC(TYPEOF_IS_UCHAR_PTR(b)), \
        STATIC(TYPEOF_IS_SIZET(l)),     \
        fwrite(b, 1, l, stdout)         \
    )

enum json_parse_status_t json_esc_do_parse(
    struct json_esc_t* esc, const uchar_t* buf,
    size_t len, bool done)
{
    const uchar_t* end;

    STATIC(TYPE_WIDTH(uchar_t) >= 8);
    STATIC(TYPE_WIDTH(size_t) >= 32);

    STATIC(ARRAY_SIZE(json_esc_escapes) == ascii_size);

    if (!JSON_ESC_STATUS_IS(ok) || esc->done)
        return JSON_ESC_CURRENT_STATUS();

    if (!esc->init) {
        esc->init = true;
        esc->current = esc->config;

        if (JSON_ESC_CURRENT_CONF(trim_spaces) &&
            esc->spaces == NULL)
            esc->spaces = mem_buf_create(
                esc->sizes.buf_max,
                esc->sizes.buf_init);

        if (JSON_ESC_CURRENT_CONF(quote_text))
            putchar('"');
    }

    if (done) {
        esc->done = true;

        if (esc->contn) {
            if (JSON_ESC_CURRENT_CONF(validate_utf8))
                return JSON_ESC_ERROR(invalid_utf8);

            if (esc->n_utf8)
                printb(esc->utf8, esc->n_utf8);

            esc->printed = true;
            esc->n_utf8 = 0;
        }

        if (JSON_ESC_CURRENT_CONF(quote_text))
            putchar('"');
        putchar('\n');

        return JSON_ESC_STATUS(ok);
    }

    if (len == 0)
        return JSON_ESC_STATUS(ok);

    ASSERT(buf != NULL);

    // stev: see Table 3-6, UTF-8 Bit Distribution
    // The Unicode Standard Version 8.0 - Core Specification, Chapter 3, p. 125
    // http://www.unicode.org/versions/Unicode8.0.0/ch03.pdf

    // stev: quote: Because surrogate code points are not Unicode
    // scalar values, any UTF-8 byte sequence that would otherwise
    // map to code points U+D800..U+DFFF is ill-formed.

    for (end = buf + len; buf < end; buf ++) {
        if (esc->contn == 0) {
            const uchar_t *p, *q;
            size_t n;

            if (!JSON_ESC_CURRENT_CONF(trim_spaces) ||
                esc->printed)
                goto escapes;

        spaces:
            for (p = buf, q = NULL, n = 0; p < end; p ++) {
                if (*p == '\n') {
                    q = p;
                    n ++;
                }
                if (*p >= ascii_size ||
                        !json_esc_spaces[*p])
                    break;
            }

            if (p > buf) {
                size_t d = PTR_DIFF(p, buf);

                mem_buf_update(
                    esc->spaces, buf, d,
                    mem_buf_append);

                if (q != NULL) {
                    ASSERT(n);

                    ASSERT_SIZE_ADD_NO_OVERFLOW(
                        esc->pos.line, n);
                    esc->pos.line += n;

                    n = PTR_DIFF(p, q);
                    ASSERT(n > 0);
                    esc->pos.col = n;
                }
                else {
                    ASSERT_SIZE_ADD_NO_OVERFLOW(
                        esc->pos.col, d);
                    esc->pos.col += d;
                }

                buf += d;
            }

            if (p >= end)
                return JSON_ESC_STATUS(ok);

        escapes:
            for (p = buf; p < end; p ++) {
                if (*p >= ascii_size
                        || (JSON_ESC_CURRENT_CONF(trim_spaces)
                            && json_esc_spaces[*p])
                        || json_esc_escapes[*p])
                    break;
            }

            if (p > buf) {
                size_t d = PTR_DIFF(p, buf);

                if (JSON_ESC_CURRENT_CONF(trim_spaces))
                    json_esc_print_spaces(esc);

                printb(buf, d);
                esc->printed = true;

                ASSERT_SIZE_ADD_NO_OVERFLOW(
                    esc->pos.col, d);
                esc->pos.col += d;

                buf += d;
            }

            if (p >= end)
                return JSON_ESC_STATUS(ok);

            ASSERT(buf == p);

            if (JSON_ESC_CURRENT_CONF(trim_spaces) &&
                *p < ascii_size && json_esc_spaces[*p])
                goto spaces;

            if (JSON_ESC_CURRENT_CONF(trim_spaces))
                json_esc_print_spaces(esc);

            if (*p < ascii_size) {
                JSON_ESC_PRINT_CHAR(*p);
                esc->printed = true;
            }
            else {
                ASSERT(esc->contn == 0);

                if ((*p & 0xe0u) == 0xc0u)
                    esc->contn = 1;
                else
                if ((*p & 0xf0u) == 0xe0u)
                    esc->contn = 2;
                else
                if ((*p & 0xf8u) == 0xf0u)
                    esc->contn = 3;

                if (esc->contn == 0) {
                    if (JSON_ESC_CURRENT_CONF(validate_utf8))
                        return JSON_ESC_ERROR(invalid_utf8);

                    putchar(*p);
                    esc->printed = true;
                }
                else {
                    esc->point =
                        *p & ((SZ(1) << (SZ(7) - esc->contn)) - 1);

                    esc->pt_pos = esc->pos;

                    esc->utf8[0] = *p;
                    esc->n_utf8 = 1;
                }
            }
        }
        else
        if ((*buf & 0xc0u) != 0x80u) {
            uchar_t c;

            if (JSON_ESC_CURRENT_CONF(validate_utf8))
                return JSON_ESC_ERROR(invalid_utf8);

            if (esc->n_utf8)
                printb(esc->utf8, esc->n_utf8);

            if (*buf < ascii_size &&
                (c = json_esc_escapes[*buf]) &&
                (c &= ~esc_bit))
                printf("\\%c", c);
            else
                putchar(*buf);

            esc->printed = true;
            esc->n_utf8 = 0;
            esc->contn = 0;
        }
        else {
            ASSERT(esc->n_utf8 < ARRAY_SIZE(esc->utf8));
            esc->utf8[esc->n_utf8 ++] = *buf;

            ASSERT_SIZE_DEC_NO_OVERFLOW(esc->contn);
            esc->contn --;

            if (JSON_ESC_CURRENT_CONF(validate_utf8)) {
                esc->point <<= SZ(6);
                esc->point |= *buf & ~0xc0u;
            }

            if (JSON_ESC_CURRENT_CONF(validate_utf8) &&
                esc->contn == 0) {
                size_t e = 0;

                if (!json_validate_utf8(
                        esc->utf8, esc->n_utf8, &e)) {
#ifdef JSON_DEBUG
                    PRINT_DEBUG_BEGIN("utf8=");
                    pretty_print_repr(
                        stderr, esc->utf8, esc->n_utf8,
                        pretty_print_repr_quote_non_print |
                        pretty_print_repr_print_quotes);
                    PRINT_DEBUG_FMT(" err=%zu", e);
                    PRINT_DEBUG_END();
#endif
                    return JSON_ESC_ERROR_PT(invalid_utf8);
                }

                ASSERT(esc->pt_pos.line == esc->pos.line);
                ASSERT(esc->pt_pos.col < esc->pos.col);

                PRINT_DEBUG(
                    "pos={.line=%zu .cols=%zu..%zu} point=0x%zx",
                    esc->pt_pos.line, esc->pt_pos.col, esc->pos.col,
                    esc->point);

                // stev: the surrogate code points must have
                // been rejected by json_validate_utf8 above
                ASSERT(esc->point < 0xd800u || esc->point > 0xdfffu);

                if (esc->point >= 0x10000u) {
                    if (!JSON_ESC_CURRENT_CONF(allow_surrogate_pairs))
                        return JSON_ESC_ERROR_PT(non_bmp_utf8);

                    esc->point -= 0x10000u;
                    printf("\\u%04zx", 0xd800u | (esc->point >> 10));

                    esc->point &= 0x3ffu;
                    esc->point |= 0xdc00u;
                }
                printf("\\u%04zx", esc->point);
                esc->printed = true;
            }
            else
            if (esc->contn == 0) {
                ASSERT(esc->n_utf8);
                printb(esc->utf8, esc->n_utf8);

                esc->printed = true;
                esc->n_utf8 = 0;
            }
        }

        if (*buf == '\n') {
            ASSERT_SIZE_INC_NO_OVERFLOW(
                esc->pos.line);
            esc->pos.line ++;
            esc->pos.col = 1;
        }
        else {
            ASSERT_SIZE_INC_NO_OVERFLOW(
                esc->pos.col);
            esc->pos.col ++;
        }
    }

    return JSON_ESC_STATUS(ok);
}

enum json_parse_status_t json_esc_parse(
    struct json_esc_t* esc, const uchar_t* buf, size_t len)
{
    return json_esc_do_parse(esc, buf, len, false);
}

enum json_parse_status_t json_esc_parse_done(
    struct json_esc_t* esc)
{
    return json_esc_do_parse(esc, NULL, 0, true);
}

bool json_esc_config_get_param(
    struct json_esc_t* esc, enum json_config_param_t param)
{
    switch (param) {
    case json_allow_surrogate_pairs_config:
    case json_validate_utf8_config:
    case json_trim_spaces_config:
    case json_quote_text_config:
        param -= json_allow_surrogate_pairs_config;
        return esc->config & (SZ(1) << param);
    default:
        INVALID_ARG("%d", param);
    }
}

void json_esc_config_set_param(
    struct json_esc_t* esc, enum json_config_param_t param,
    bool val)
{
    switch (param) {
    case json_allow_surrogate_pairs_config:
    case json_validate_utf8_config:
    case json_trim_spaces_config:
    case json_quote_text_config:
        param -= json_allow_surrogate_pairs_config;
        if (val)
            esc->config |= (SZ(1) << param);
        else
            esc->config &= ~(SZ(1) << param);
        break;
    default:
        INVALID_ARG("%d", param);
    }
}

bool json_esc_get_is_error(
    struct json_esc_t* esc)
{
    return esc->error.type != json_esc_error_none;
}

struct json_error_pos_t json_esc_get_error_pos(
    struct json_esc_t* esc)
{
    return esc->error.pos;
}

const struct json_file_info_t* json_esc_get_error_file(
    struct json_esc_t* esc UNUSED)
{
    return NULL;
}

#ifdef JSON_DEBUG

#undef  CASE
#define CASE(E) case json_esc_error_ ## E: return #E;

static const char* json_esc_error_get_name(
    enum json_esc_error_type_t type)
{
    switch (type) {
    CASE(none);
    CASE(invalid_utf8);
    CASE(non_bmp_utf8);
    default:
        UNEXPECT_VAR("%d", type);
    }
}

#endif

static const char* json_esc_error_get_desc(
    enum json_esc_error_type_t err)
{
    switch (err) {
    case json_esc_error_none:
        return "none";
    case json_esc_error_invalid_utf8:
        return "invalid utf-8 encoding";
    case json_esc_error_non_bmp_utf8:
        return "non-BMP utf-8 encoding";
    default:
        UNEXPECT_VAR("%d", err);
    }
}

void json_esc_print_error_desc(
    struct json_esc_t* esc, FILE* file)
{
    fputs(json_esc_error_get_desc(esc->error.type), file);
}

#ifdef JSON_DEBUG
void json_esc_print_error_debug(
    struct json_esc_t* esc, FILE* file)
{
    fprintf(file, "{.type=%s .pos={.line=%zu .col=%zu}}",
        json_esc_error_get_name(esc->error.type),
        esc->error.pos.line,
        esc->error.pos.col);
}
#endif


