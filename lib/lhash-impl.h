// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LHASH_NAME
#error  LHASH_NAME is not defined
#endif

#ifndef LHASH_KEY_TYPE
#error  LHASH_KEY_TYPE is not defined
#endif

#ifndef LHASH_NULL_KEY
#error  LHASH_NULL_KEY is not defined
#endif

#ifndef LHASH_KEY_IS_NULL
#error  LHASH_KEY_IS_NULL is not defined
#endif

#if !defined(LHASH_NEED_HSUM_ARG) || \
    !defined(LHASH_NEED_FIXED_SIZE) && \
    !defined(LHASH_NEED_NODE_HASH)
#ifndef LHASH_KEY_HASH
#error  LHASH_KEY_HASH is not defined
#endif
#endif

#if defined(LHASH_NEED_NODE_HASH) || \
    defined(LHASH_NEED_HSUM_ARG)
#ifndef LHASH_HASH_TYPE
#error  LHASH_HASH_TYPE is not defined
#endif
#endif

#ifndef LHASH_KEY_EQ
#error  LHASH_KEY_EQ is not defined
#endif

#define LHASH_MAKE_NAME_(n, s) n ## _lhash_ ## s
#define LHASH_MAKE_NAME(n, s)  LHASH_MAKE_NAME_(n, s)

#define LHASH_TYPE      LHASH_MAKE_NAME(LHASH_NAME, t)
#define LHASH_NODE_TYPE LHASH_MAKE_NAME(LHASH_NAME, node_t)
#define LHASH_INIT      LHASH_MAKE_NAME(LHASH_NAME, init)
#define LHASH_DONE      LHASH_MAKE_NAME(LHASH_NAME, done)
#define LHASH_LOOKUP    LHASH_MAKE_NAME(LHASH_NAME, lookup)
#define LHASH_REHASH    LHASH_MAKE_NAME(LHASH_NAME, rehash)
#define LHASH_INSERT    LHASH_MAKE_NAME(LHASH_NAME, insert)
#define LHASH_ROLLBACK  LHASH_MAKE_NAME(LHASH_NAME, rollback)

// stev: this hash table implementation is due to my work
// on Path-Set project: http://www.nongnu.org/path-set/
// (see Path-Set's source file src/lhash-impl.h)

struct LHASH_NODE_TYPE
{
    LHASH_KEY_TYPE key;
#ifdef LHASH_VAL_TYPE
    LHASH_VAL_TYPE val;
#endif
#ifdef LHASH_NEED_NODE_HASH
    LHASH_HASH_TYPE hash;
#endif
};

struct LHASH_TYPE
{
    struct LHASH_NODE_TYPE* table;
#ifdef LHASH_NEED_DEBUG
    bits_t debug: 1;
#endif
    bits_t frozen: 1;
#ifndef LHASH_NEED_FIXED_SIZE
    size_t max_size;
#endif
    size_t max_load;
    size_t size;
    size_t used;
};

#define LHASH_MUL_FRAC_(v, n, d)    \
    (                               \
        STATIC(TYPEOF_IS_SIZET(v)), \
        STATIC(TYPEOF_IS_SIZET(n)), \
        STATIC(TYPEOF_IS_SIZET(d)), \
        STATIC(IS_CONSTANT(n)),     \
        STATIC(IS_CONSTANT(d)),     \
        STATIC(n > 0),              \
        STATIC(d > 0),              \
        SIZE_MUL_NO_OVERFLOW(v, n)  \
        ? v = (v * n) / d,          \
          true                      \
        : false                     \
    )
#define LHASH_MUL_FRAC(v, ...) \
        LHASH_MUL_FRAC_(v, __VA_ARGS__)

#define LHASH_FRAC(n, d) SZ(n), SZ(d)

// stev: Knuth, TAOCP, vol 3, 2nd edition,
// 6.4 Hashing, p. 528
#define LHASH_REHASH_LOAD \
        LHASH_FRAC(3, 4) // 0.75

// stev: double the size of the table
// each time decided to enlarge it; a
// variant would be to double its size
// every second time enlarging it --
// which amounts to the factor below
// be sqrt(2) ~= 1.4142 (~= 71 / 50)
#define LHASH_REHASH_SIZE \
        LHASH_FRAC(2, 1) // 2.0

#define LHASH_MAX_LOAD()               \
    ({                                 \
        size_t __r = hash->size;       \
        VERIFY(LHASH_MUL_FRAC(         \
            __r, LHASH_REHASH_LOAD));  \
        VERIFY(__r < hash->size);      \
        /* => __r <= hash->size - 1 */ \
        VERIFY(__r > 0);               \
        __r;                           \
    })

#ifdef  LHASH_NEED_DEBUG
#undef  LHASH_PRINT_DEBUG
#ifndef LHASH_NEED_FIXED_SIZE
#define LHASH_PRINT_DEBUG()     \
    do {                        \
        if (hash->debug) {      \
            PRINT_DEBUG_UNCOND( \
                "max_size=%zu " \
                "max_load=%zu " \
                "size=%zu "     \
                "used=%zu",     \
                hash->max_size, \
                hash->max_load, \
                hash->size,     \
                hash->used);    \
        }                       \
    } while (0)
#else // LHASH_NEED_FIXED_SIZE
    do {                        \
        if (hash->debug) {      \
            PRINT_DEBUG_UNCOND( \
                "max_load=%zu " \
                "size=%zu "     \
                "used=%zu",     \
                hash->max_load, \
                hash->size,     \
                hash->used);    \
        }                       \
    } while (0)
#endif // LHASH_NEED_FIXED_SIZE
#else // LHASH_NEED_DEBUG
#undef  LHASH_PRINT_DEBUG
#define LHASH_PRINT_DEBUG() \
    do {} while (0)
#endif // LHASH_NEED_DEBUG

static void LHASH_INIT(
    struct LHASH_TYPE* hash,
#ifdef LHASH_NEED_DEBUG
    bool debug,
#endif
#ifndef LHASH_NEED_FIXED_SIZE
    size_t max_size,
#endif
    size_t init_size)
{
#ifndef LHASH_NEED_FIXED_SIZE
    if (max_size > lhash_max_prime)
        INVALID_ARG("%zu", max_size);

    if (max_size == 0)
        max_size = 128;

    if (init_size == 0 ||
        init_size > max_size)
        init_size = max_size;
#else // LHASH_NEED_FIXED_SIZE
    if (init_size > lhash_max_prime)
        INVALID_ARG("%zu", init_size);

    if (init_size == 0)
        init_size = 128;
#endif // LHASH_NEED_FIXED_SIZE

    memset(hash, 0,
        sizeof(struct LHASH_TYPE));

    hash->size =
          lhash_next_prime2(init_size);
#ifndef LHASH_NEED_FIXED_SIZE
    hash->max_size =
          max_size > init_size
        ? lhash_next_prime2(max_size)
        : hash->size;
    VERIFY(hash->size <= hash->max_size);
#endif

    hash->max_load = LHASH_MAX_LOAD();
#ifdef LHASH_NEED_DEBUG
    hash->debug = debug;
#endif

    hash->table = calloc(hash->size,
        sizeof(struct LHASH_NODE_TYPE));
    VERIFY(hash->table != NULL);

    LHASH_PRINT_DEBUG();
}

static void LHASH_DONE(
    struct LHASH_TYPE* hash)
{
    free(hash->table);
}

#define LHASH_ASSERT_INVARIANTS_(hash)       \
    do {                                     \
        ASSERT((hash)->size > 0);            \
        ASSERT((hash)->used < (hash)->size); \
    } while (0)
#define LHASH_ASSERT_INVARIANTS(hash)   \
    do {                                \
        STATIC(                         \
            TYPEOF_IS(hash,             \
                struct LHASH_TYPE*) ||  \
            TYPEOF_IS(hash, const       \
                struct LHASH_TYPE*));   \
        LHASH_ASSERT_INVARIANTS_(hash); \
    } while (0)

// stev: Knuth, TAOCP, vol 3, 2nd edition,
// 6.4 Hashing, Algorithm L, p. 526

#ifdef LHASH_NEED_LOOKUP

static bool LHASH_LOOKUP(
    const struct LHASH_TYPE* hash,
    LHASH_KEY_TYPE key,
#ifdef LHASH_NEED_HSUM_ARG
    LHASH_HASH_TYPE hsum,
#endif
    struct LHASH_NODE_TYPE** result)
{
    struct LHASH_NODE_TYPE* p;

#ifdef LHASH_NEED_HSUM_ARG
    STATIC(TYPE_IS_UINT_MAX_SIZET(
        LHASH_HASH_TYPE));
#endif

    ASSERT(!LHASH_KEY_IS_NULL(key));

    LHASH_ASSERT_INVARIANTS_(hash);

#ifndef LHASH_NEED_HSUM_ARG
    p = hash->table + LHASH_KEY_HASH(key) %
        hash->size;
#else
    p = hash->table + hsum % hash->size;
#endif

    while (!LHASH_KEY_IS_NULL(p->key)) {
        if (LHASH_KEY_EQ(p->key, key)) {
            *result = p;
            return true;
        }
        if (p == hash->table)
            p += hash->size - 1;
        else
            p --;
    }

    *result = NULL;
    return false;
}

#endif // LHASH_NEED_LOOKUP

#ifndef LHASH_NEED_FIXED_SIZE

static bool LHASH_REHASH(
    struct LHASH_TYPE* hash)
{
    struct LHASH_NODE_TYPE *t, *p, *e, *q;
    size_t s;

    LHASH_ASSERT_INVARIANTS_(hash);

    ASSERT(!hash->frozen);

    s = hash->size;
    VERIFY(LHASH_MUL_FRAC(
        s, LHASH_REHASH_SIZE));
    VERIFY(s > hash->size);

    s = lhash_next_prime(s);
    if (s > hash->max_size) {
        s = hash->max_size;

        hash->frozen = true;
        if (hash->size >= s)
            return false;
    }
    // => hash->size < s <= hash->max_size

    t = calloc(s,
            sizeof(struct LHASH_NODE_TYPE));
    VERIFY(t != NULL);

    for (p = hash->table,
         e = p + hash->size;
         p < e;
         p ++) {
        if (LHASH_KEY_IS_NULL(p->key))
            continue;

#ifndef LHASH_NEED_NODE_HASH
        q = t + LHASH_KEY_HASH(p->key) % s;
#else
        q = t + p->hash % s;
#endif

        while (!LHASH_KEY_IS_NULL(q->key)) {
            if (q == t)
                q += s - 1;
            else
                q --;
        }

        *q = *p;
    }

    free(hash->table);

    hash->table = t;
    hash->size = s;
    hash->max_load = LHASH_MAX_LOAD();
    // the new size > the old size =>
    // the invariants are preserved

    LHASH_PRINT_DEBUG();

    return true;
}

#endif // LHASH_NEED_FIXED_SIZE

static bool LHASH_INSERT(
    struct LHASH_TYPE* hash,
    LHASH_KEY_TYPE key,
#ifdef LHASH_NEED_HSUM_ARG
    LHASH_HASH_TYPE hsum,
#endif
    struct LHASH_NODE_TYPE** result)
{
    struct LHASH_NODE_TYPE* p;
#ifndef LHASH_NEED_HSUM_ARG
    size_t hsum;
#endif

#ifdef LHASH_NEED_HSUM_ARG
    STATIC(TYPE_IS_UINT_MAX_SIZET(
        LHASH_HASH_TYPE));
#endif

    ASSERT(!LHASH_KEY_IS_NULL(key));

    LHASH_ASSERT_INVARIANTS_(hash);

#ifndef LHASH_NEED_HSUM_ARG
    hsum = LHASH_KEY_HASH(key);
#endif
    p = hash->table + hsum % hash->size;

    while (!LHASH_KEY_IS_NULL(p->key)) {
        if (LHASH_KEY_EQ(p->key, key)) {
            *result = p;
            return false;
        }
        if (p == hash->table)
            p += hash->size - 1;
        else
            p --;
    }

    ASSERT(hash->max_load <= hash->size - 1);
    if (hash->used < hash->max_load) {
        // => hash->used < hash->size - 1
        goto new_node;
    }

    // stev: let 'S' be the size of the hash
    // table before 'LHASH_REHASH' increases
    // it strictly; therefore: hash->used < S

#ifndef LHASH_NEED_FIXED_SIZE
    if (hash->frozen || !LHASH_REHASH(hash)) {
#endif
        ENSURE(hash->used < hash->size - 1,
            "linear hash table overflow");
#ifndef LHASH_NEED_FIXED_SIZE
        goto new_node;
    }

    // stev: we have that:
    //   hash->used < hash->size - 1
    // indeed:
    //   hash->size increased strictly =>
    //   S < hash->size <=>
    //   S <= hash->size - 1 =>
    //   hash->used < S <= hash->size - 1

    p = hash->table + hsum % hash->size;

    while (!LHASH_KEY_IS_NULL(p->key)) {
        if (p == hash->table)
            p += hash->size - 1;
        else
            p --;
    }
#endif // LHASH_NEED_FIXED_SIZE

new_node:
    // stev: hash->used < hash->size - 1
    hash->used ++;
    p->key = key;
#ifdef LHASH_NEED_NODE_HASH
    p->hash = hsum;
#endif

    *result = p;
    return true;
}

#ifdef LHASH_NEED_ROLLBACK

static void LHASH_ROLLBACK(
    struct LHASH_TYPE* hash,
    struct LHASH_NODE_TYPE* node)
{
    ASSERT(!LHASH_KEY_IS_NULL(node->key));

    LHASH_ASSERT_INVARIANTS_(hash);

    node->key = LHASH_NULL_KEY;

    ASSERT(hash->used > 0);
    hash->used --;
}

#endif // LHASH_NEED_ROLLBACK


