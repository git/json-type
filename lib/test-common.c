// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#define _GNU_SOURCE
#include <getopt.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "common.h"
#include "char-traits.h"
#include "ptr-traits.h"
#include "su-size.h"

#include "test-common.h"

#define ISASCII CHAR_IS_ASCII

const char stdin_name[] = "<stdin>";

void error(const char* fmt, ...)
{
    va_list arg;

    char b[256];

    va_start(arg, fmt);
    vsnprintf(b, sizeof b - 1, fmt, arg);
    va_end(arg);

    b[sizeof b - 1] = 0;

    fprintf(
        stderr, "%s: error: %s\n",
        program, b);

    exit(1);
}

static void version(void)
{
    fprintf(stdout, "%s: version %s\n", program, verdate);
}

static void usage(void)
{
    fprintf(stdout, help, program);
}

void options_invalid_opt_arg(const char* opt_name, const char* opt_arg)
{
    error("invalid argument for '%s' option: '%s'", opt_name, opt_arg);
}

void options_illegal_opt_arg(const char* opt_name, const char* opt_arg)
{
    error("illegal argument for '%s' option: '%s'", opt_name, opt_arg);
}

static void missing_opt_arg_str(const char* opt_name)
{
    error("argument for option '%s' not found", opt_name);
}

static void missing_opt_arg_ch(char opt_name)
{
    error("argument for option '-%c' not found", opt_name);
}

static void not_allowed_opt_arg(const char* opt_name)
{
    error("option '%s' does not allow an argument", opt_name);
}

static void invalid_opt_str(const char* opt_name)
{
    error("invalid command line option '%s'", opt_name);
}

static void invalid_opt_ch(char opt_name)
{
    error("invalid command line option '-%c'", opt_name);
}

#define SU_SIZE_OPT_NAME options
#define SU_SIZE_OPT_NEED_PARSE_SIZE
#define SU_SIZE_OPT_NEED_PARSE_SIZE_SU
#include "su-size-opt-impl.h"

void options_parse_args(void* opts,
    const struct options_funcs_t* funcs,
    const char* shorts, size_t n_shorts,
    const struct option* longs, size_t n_longs,
    int argc, char* argv[])
{
    struct bits_opts_t
    {
        bits_t dump: 1;
        bits_t usage: 1;
        bits_t version: 1;
    };
    struct bits_opts_t bits = {
        .dump    = false,
        .usage   = false,
        .version = false
    };
    enum {
        help_opt      = '?',
        dump_opts_opt = 128,
        version_opt,
    };
    static struct option AL[] = {
        { "dump-options", 0,       0, dump_opts_opt },
        { "version",      0,       0, version_opt },
        { "help",         0, &optopt, help_opt },
        { 0,              0,       0, 0 }
    };
    static const char AS[] = ":";

    const size_t L = ARRAY_SIZE(AL);
    const size_t S = ARRAY_SIZE(AS);
    const size_t n = sizeof(struct option);
    struct option OL[n_longs + L];
    char OS[n_shorts + S];
    struct option* l;
    char* s;
    int opt;

    ASSERT_SIZE_MUL_NO_OVERFLOW(n, n_longs);
    memcpy(l = OL, longs, n * n_longs);

    ASSERT_SIZE_MUL_NO_OVERFLOW(n, L);
    memcpy(l += n_longs, AL, n * L);

    ASSERT_SIZE_DEC_NO_OVERFLOW(S);
    memcpy(s = OS, AS, S - 1);
    memcpy(s += S - 1, shorts, n_shorts);
    s[n_shorts] = '\0';

#define call_func(f, ...)               \
    ({                                  \
        ASSERT(opts);                   \
        ASSERT(funcs->f);               \
        funcs->f(opts, ## __VA_ARGS__); \
    })

#define argv_optind()                      \
    ({                                     \
        size_t i = INT_AS_SIZE(optind);    \
        ASSERT_SIZE_DEC_NO_OVERFLOW(i);    \
        ASSERT(i - 1 < INT_AS_SIZE(argc)); \
        argv[i - 1];                       \
    })

#define optopt_char()                   \
    ({                                  \
        ASSERT(ISASCII((char) optopt)); \
        (char) optopt;                  \
    })

    opterr = 0;
    optind = 1;
    while ((opt = getopt_long(argc, argv, OS, OL, 0)) != EOF) {
        switch (opt) {
        case dump_opts_opt:
            bits.dump = true;
            break;
        case version_opt:
            bits.version = true;
            break;
        case 0:
            bits.usage = true;
            break;
        case ':': {
            const char* opt = argv_optind();
            if (opt[0] == '-' && opt[1] == '-')
                missing_opt_arg_str(opt);
            else
                missing_opt_arg_ch(optopt_char());
            break;
        }
        case '?':
        inv_opt:
            if (optopt == 0)
                invalid_opt_str(argv_optind());
            else {
                char* opt = argv_optind();
                if (opt[0] != '-' || opt[1] != '-')
                    invalid_opt_ch(optopt_char());
                else {
                    char* end = strchr(opt, '=');
                    if (end) *end = '\0';
                    if (end || optopt != '?')
                        not_allowed_opt_arg(opt);
                    else
                        bits.usage = true;
                }
            }
            break;
        default:
            if (!call_func(parse, opt, optarg))
                goto inv_opt;
            break;
        }
    }

    call_func(done, INT_AS_SIZE(optind));

    if (bits.version)
        version();
    if (bits.dump)
        call_func(dump);
    if (bits.usage)
        usage();

    if (bits.dump ||
        bits.version ||
        bits.usage)
        exit(0);
}


