// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __LHASH_H
#define __LHASH_H

#include <stddef.h>

// stev: the max value accepted by
// the next-prime functions below:
// that is the next prime of 2^16
enum { lhash_max_prime = SZ(65537) };

size_t lhash_next_prime(size_t n);
size_t lhash_next_prime2(size_t n);

#endif // __LHASH_H

