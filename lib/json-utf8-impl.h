// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifdef JSON_UTF8_NEED_VALIDATE_ALGO

#ifndef JSON_UTF8_VALIDATE_CHR
#error  JSON_UTF8_VALIDATE_CHR is not defined
#endif

#ifndef JSON_UTF8_VALIDATE_SEQ
#error  JSON_UTF8_VALIDATE_SEQ is not defined
#endif

#ifndef JSON_UTF8_VALIDATE_ERROR
#error  JSON_UTF8_VALIDATE_ERROR is not defined
#endif

#ifndef JSON_UTF8_VALIDATE_ERROR_END
#error  JSON_UTF8_VALIDATE_ERROR_END is not defined
#endif

#define CHR(n) JSON_UTF8_VALIDATE_CHR(ch)
#define SEQ(n) JSON_UTF8_VALIDATE_SEQ(n, ptr)
#define ERR(n) JSON_UTF8_VALIDATE_ERROR(n, buf, ptr)
#define INC(p) ({ if (++ (p) >= end) JSON_UTF8_VALIDATE_ERROR_END(len); (p); })

// >>> JSON_UTF8_VALIDATE_ALGO

    const uchar_t *ptr, *end;
    uchar_t ch;

    // stev: see Table 3-7, Well-Formed UTF-8 Byte Sequences
    // The Unicode Standard Version 8.0 - Core Specification, Chapter 3, p. 125
    // http://www.unicode.org/versions/Unicode8.0.0/ch03.pdf

    for (ptr = buf, end = buf + len; ptr < end; ptr ++) {
#ifdef JSON_UTF8_NEED_VALIDATE_ITER_LABELS
    begin_iter:
#endif
        ch = *ptr;

        if (ch < 0x80u)
            CHR();
        else
        if (ch < 0xc2u)
            ERR(0);
        else
        if (ch < 0xe0u) {
            if ((*INC(ptr) ^ 0x80u) >= 0x40u)
                ERR(1);
            else
                SEQ(1);
        }
        else
        if (ch < 0xf0u) {
            if (((*INC(ptr) ^ 0x80u) >= 0x40u) ||
                (ch == 0xe0u && *ptr < 0xa0u) ||
                (ch == 0xedu && *ptr > 0x9fu) ||
                ((*INC(ptr) ^ 0x80u) >= 0x40u))
                ERR(2);
            else
                SEQ(2);
        }
        else
        if (ch < 0xf5u) {
            if (((*INC(ptr) ^ 0x80u) >= 0x40u) ||
                (ch < 0xf1u && *ptr < 0x90u) ||
                (ch > 0xf3u && *ptr >= 0x90u) ||
                ((*INC(ptr) ^ 0x80u) >= 0x40u) ||
                ((*INC(ptr) ^ 0x80u) >= 0x40u))
                ERR(3);
            else
                SEQ(3);
        }
        else
            ERR(4);

#ifdef JSON_UTF8_NEED_VALIDATE_ITER_LABELS
    next_iter:;
#endif
    }

// <<< JSON_UTF8_VALIDATE_ALGO

#undef INC
#undef ERR
#undef SEQ
#undef CHR

#endif // JSON_UTF8_NEED_VALIDATE_ALGO


