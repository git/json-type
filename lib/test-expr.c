// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#define _GNU_SOURCE
#include <getopt.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>

#include "common.h"
#include "ptr-traits.h"
#include "su-size.h"

#include "test-common.h"

#define JSON_LITEX_TEST_EXPR
#include "json-litex.c"

const char program[] = STRINGIFY(PROGRAM);
const char verdate[] = "0.1 -- 2018-08-09 11:26"; // $ date +'%F %R'

const char help[] =
"usage: %s [ACTION|OPTION]... [EXPR [LITERAL]]\n"
"where the actions are:\n"
"  -C|--compile-expr          compile given literal expression\n"
"  -E|--execute-expr          execute given literal expression on given\n"
"                               JSON literal value (default)\n"
"  -B|--dump-bytecodes        print out the PCRE2 bytecodes of given\n"
"                               literal expression\n"
"and the options are:\n"
"  -e|--expression=EXPR       input literal expression\n"
"  -l|--[json-]literal=VALUE  input JSON literal value against which\n"
"                               the input literal expression is evaluated\n"
"  -a|--alpha-values=LIST     when executing literal expressions containing\n"
"                               references to alpha builtins (i.e. if using\n"
"                               options `-b|--builtins-space=alpha'), assign\n"
"                               the given values to the one-letter function\n"
"                               builtins; LIST is a comma-separated list of\n"
"                               decimal non-negative integers; LIST defines\n"
"                               the value to be returned by the corresponding\n"
"                               one-letter function: 'a' and 'A' return the\n"
"                               value at position 0, 'b' and 'B' return the\n"
"                               value at position 1, and so on; note that the\n"
"                               two-character functions return 0, N1 is 123,\n"
"                               N2 is 456, S1 is `foo` and S2 is `bar`\n"
"  -b|--builtins-space=NAME   when compiling literal expressions containing\n"
"                               references to builtins, use the specified\n"
"                               namespace: either default or alpha; the alpha\n"
"                               builtins are the following: \n"
"                                 [a-z]   function: num()\n"
"                                 [A-Z]   function: bool()\n"
"                                 [ABC]1  function: bool(num)\n"
"                                 [AB]2   function: bool(num, num)\n"
"                                 A3      function: bool(num, num, num)\n"
"                                 N[12]   constant: num\n"
"                                 S[12]   constant: str\n"
"  -c|--[expr-]compiler=NAME  when compiling literal expressions to internal\n"
"                               executable code, use the named expression\n"
"                               compiler: flat or ast; the flat compiler is\n"
"                               faster compared to the other one, while the\n"
"                               ast compiler produces better executable code;\n"
"                               flat is the default compiler used\n"
"  -n|--null-char=CHAR        replace all occurrences of CHAR with NUL chars in\n"
"     --no-null-char            the input JSON literal if that is a string\n"
"  -r|--[no-]relative-offs    when printing out literal expression code, in case\n"
"                               of jump instructions let offsets be relative, or\n"
"                               otherwise do not (default)\n"
"     --sizes-NAME=VALUE      assign the named sizes parameter the given value;\n"
"                               use `--help-sizes' option to obtain the list of\n"
"                               all the sizes parameters along with the minimum,\n"
"                               the maximum and the default values and a short\n"
"                               description attached\n"
"  -v|--[no-]verbose          be verbose or not (default not)\n"
"     --dump-options          print options and exit\n"
"     --help-sizes            print info about the sizes parameters and exit\n"
"     --version               print version numbers and exit\n"
"  -?|--help                  display this help info and exit\n";

enum options_action_t
{
    options_compile_expr_action,
    options_execute_expr_action,
    options_dump_bytecodes_action
};

enum options_builtins_space_t
{
    options_builtins_space_default,
    options_builtins_space_alpha,
};

enum options_expr_compiler_t
{
    options_expr_compiler_flat,
    options_expr_compiler_ast
};

struct options_t
{
    enum options_action_t
                 action;
    enum options_builtins_space_t
                 builtins_space;
    enum options_expr_compiler_t
                 expr_compiler;
    const char*  expr;
    struct json_litex_value_t
                 literal;
    struct json_litex_expr_vm_alpha_t
                 alpha;
    size_t       n_alpha;
    char         null_char;
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
    size_t       sizes_ ## id;
#include "test-expr-sizes.def"

    bits_t       rel_offs: 1;
    bits_t       verbose: 1;
    bits_t       usage_sizes: 1;
    size_t       argc;
    char* const *argv;
};

static struct json_litex_value_t
    options_parse_literal_optarg(
        const char* opt_name, char* opt_arg);

static void options_done(struct options_t* opts, size_t n_arg)
{
    ASSERT_SIZE_SUB_NO_OVERFLOW(opts->argc, n_arg);
    opts->argc -= n_arg;
    opts->argv += n_arg;

    if (opts->argc > 0) {
        opts->expr = *opts->argv ++;
        opts->argc --;
    }
    if (opts->argc > 0) {
        opts->literal =
            options_parse_literal_optarg(
                "literal", *opts->argv ++);
        opts->argc --;
    }
}

#undef  CASE
#define CASE(n) \
    case json_litex_value_ ## n ## _type

static char* options_literal_to_str(
    const struct json_litex_value_t* val)
{
    char* b = NULL;
    size_t n = 0;
    FILE* f;

    f = open_memstream(&b, &n);
    VERIFY(f != NULL);

    switch (val->type) {
    CASE(null):
        fputs("null", f);
        break;
    CASE(boolean):
        fputs(val->u.boolean
            ? "true" : "false", f);
        break;
    CASE(number):
        fputs(PTR_CHAR_CAST_CONST(
            val->u.number.ptr),
            f);
        break;
    CASE(string):
        pretty_print_string(
            f, val->u.string.ptr,
            strulen(val->u.string.ptr),
            pretty_print_string_quotes);
        break;
    default:
        UNEXPECT_VAR("%d", val->type);
    }

    fclose(f);
    ASSERT(n > 0);

    return b;
}

#define PARSE_SIZE_OPTARG_(n)   options_parse_ ## n ## _optarg

#define PARSE_BUF_SIZE_OPTARG   PARSE_SIZE_OPTARG_(su_size)
#define PARSE_STACK_SIZE_OPTARG PARSE_SIZE_OPTARG_(su_size)
#define PARSE_POOL_SIZE_OPTARG  PARSE_SIZE_OPTARG_(su_size)
#define PARSE_PLAIN_SIZE_OPTARG PARSE_SIZE_OPTARG_(size)

#define PARSE_SIZE_OPTARG(name, type, min, max) \
    PARSE_ ## type ## _SIZE_OPTARG(             \
        "sizes-" name, optarg, min, max)

#define SU_SIZE_(n) \
    struct su_size_t n ## _su = su_size(opts->n);

#define SU_BUF_SIZE      SU_SIZE_
#define SU_STACK_SIZE    SU_SIZE_
#define SU_POOL_SIZE     SU_SIZE_
#define SU_PLAIN_SIZE(n)

#define SU_SIZE(id, type) SU_ ## type ## _SIZE(sizes_ ## id)

#define FMT_SU_SIZE    "%zu%s"
#define FMT_BUF_SIZE   FMT_SU_SIZE
#define FMT_STACK_SIZE FMT_SU_SIZE
#define FMT_POOL_SIZE  FMT_SU_SIZE
#define FMT_PLAIN_SIZE "%zu"

#define FMT_SIZE(type) FMT_ ## type ## _SIZE

#define ARG_SU_SIZE(n)    n ## _su.sz, n ## _su.su
#define ARG_BUF_SIZE      ARG_SU_SIZE
#define ARG_STACK_SIZE    ARG_SU_SIZE
#define ARG_POOL_SIZE     ARG_SU_SIZE
#define ARG_PLAIN_SIZE(n) opts->n

#define ARG_SIZE(id, type) ARG_ ## type ## _SIZE(sizes_ ## id),

static void options_dump(const struct options_t* opts)
{
    static const char* const noyes[] = {
        [0] "no", [1] "yes"
    };
#define CASE2(n0, n1) \
    [options_ ## n0 ## _ ## n1 ## _action] = #n0 "-" #n1
    static const char* const actions[] = {
        CASE2(compile, expr),
        CASE2(execute, expr),
        CASE2(dump, bytecodes),
    };
#undef  CASE
#define CASE(n) [options_builtins_space_ ## n] = #n
    static const char* builtins_spaces[] = {
        CASE(default),
        CASE(alpha),
    };
#undef  CASE
#define CASE(n) [options_expr_compiler_ ## n] = #n
    static const char* expr_compilers[] = {
        CASE(flat),
        CASE(ast)
    };
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
    SU_SIZE(id, type)
#include "test-expr-sizes.def"
    char* b = options_literal_to_str(&opts->literal);
    char c[2] = { opts->null_char ? opts->null_char : 0, 0 };
    char d[128];

#define NAME(x)  OPTIONS_NAME(x)
#define NNUL(x)  OPTIONS_NNUL(x)
#define NOYES(x) OPTIONS_NOYES(x)

    su_size_list_to_string(
        d, ARRAY_SIZE(d), opts->alpha.vals, opts->n_alpha,
        ",", true);

    fprintf(stdout,
        "action:                     %s\n"
        "builtins-space:             %s\n"
        "expr-compiler:              %s\n"
        "expression:                 %s\n"
        "json-literal:               %s\n"
        "alpha-values:               %s\n"
        "null-char:                  %s\n"
        "relative-offs:              %s\n"
        "verbose:                    %s\n"
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
        "sizes-" name ":" pad " " FMT_SIZE(type) "\n"
#include "test-expr-sizes.def"
        "argc:                       %zu\n",
        NAME(action),
        NAME(builtins_space),
        NAME(expr_compiler),
        opts->expr, b, d, c,
        NOYES(rel_offs),
        NOYES(verbose),
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
        ARG_SIZE(id, type)
#include "test-expr-sizes.def"
        opts->argc);

    free(b);

    pretty_print_strings(stdout,
        PTR_CONST_PTR_CAST(opts->argv, char),
        opts->argc, "argv", 28, 0);
}

static void options_usage_sizes(void)
{
    static const struct su_size_param_t params[] = {
#define BUF   su_size_param_su
#define STACK su_size_param_su
#define POOL  su_size_param_su
#define PLAIN su_size_param_plain

#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
        { type, name, desc, min, max, def },
#include "test-expr-sizes.def"

#undef PLAIN
#undef POOL
#undef STACK
#undef BUF
    };

    su_size_print_sizes(params, ARRAY_SIZE(params), stdout);
}

enum {
    // stev: actions:
    options_compile_expr_act   = 'C',
    options_execute_expr_act   = 'E',
    options_dump_bytecodes_act = 'B',

    // stev: options:
    options_alpha_values_opt   = 'a',
    options_builtins_space_opt = 'b',
    options_expr_compiler_opt  = 'c',
    options_expression_opt     = 'e',
    options_literal_opt        = 'l',
    options_null_char_opt      = 'n',
    options_rel_offs_opt       = 'r',
    options_verbose_opt        = 'v',

    options_no_null_char_opt   = 256,
    options_no_rel_offs_opt,
    options_no_verbose_opt,
    options_help_sizes_opt,
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
    options_sizes_ ## id ## _opt,
#include "test-expr-sizes.def"
};

// $ print() { printf '%s\n' "$@"; }
// $ print 'default =options_builtins_space_default' 'alpha =options_builtins_space_alpha'|gen-func -f options_lookup_builtins_space_name -Pf -q \!strcmp|ssed -R '1s/^/static /;1s/(?<=\()/\n\t/;1s/\bresult_t\&/enum options_builtins_space_t*/;s/(?=t =)/*/;s/result_t:://'

static bool options_lookup_builtins_space_name(
    const char* n, enum options_builtins_space_t* t)
{
    // pattern: alpha|default
    switch (*n ++) {
    case 'a':
        if (!strcmp(n, "lpha")) {
            *t = options_builtins_space_alpha;
            return true;
        }
        return false;
    case 'd':
        if (!strcmp(n, "efault")) {
            *t = options_builtins_space_default;
            return true;
        }
    }
    return false;
}

static enum options_builtins_space_t
    options_parse_builtins_space_optarg(
        const char* opt_name, const char* opt_arg)
{
    enum options_builtins_space_t r;

    if (!options_lookup_builtins_space_name(opt_arg, &r))
        options_invalid_opt_arg(opt_name, opt_arg);

    return r;
}

// $ print 'flat =options_expr_compiler_flat' 'ast =options_expr_compiler_ast'|gen-func -f options_lookup_expr_compiler_name -Pf -q \!strcmp|ssed -R '1s/^/static /;1s/(?<=\()/\n\t/;1s/\bresult_t\&/enum options_expr_compiler_t*/;s/(?=t =)/*/;s/result_t:://'

static bool options_lookup_expr_compiler_name(
    const char* n, enum options_expr_compiler_t* t)
{
    // pattern: ast|flat
    switch (*n ++) {
    case 'a':
        if (!strcmp(n, "st")) {
            *t = options_expr_compiler_ast;
            return true;
        }
        return false;
    case 'f':
        if (!strcmp(n, "lat")) {
            *t = options_expr_compiler_flat;
            return true;
        }
    }
    return false;
}

static enum options_expr_compiler_t
    options_parse_expr_compiler_optarg(
        const char* opt_name, const char* opt_arg)
{
    enum options_expr_compiler_t r;

    if (!options_lookup_expr_compiler_name(opt_arg, &r))
        options_invalid_opt_arg(opt_name, opt_arg);

    return r;
}

static struct json_litex_value_t
    options_parse_literal_optarg(
        const char* opt_name, char* opt_arg)
{
    struct json_litex_value_t r;

    if (!strcmp(opt_arg, "null"))
        r.type = json_litex_value_null_type;
    else
    if (!strcmp(opt_arg, "false")) {
        r.type = json_litex_value_boolean_type;
        r.u.boolean = false;
    }
    else
    if (!strcmp(opt_arg, "true")) {
        r.type = json_litex_value_boolean_type;
        r.u.boolean = true;
    }
    else
    if (ISDIGIT(*opt_arg) || *opt_arg == '-') {
        const char* p = opt_arg;

        // -?(0|[1-9][0-9]*)(\.[0-9]+)?([eE][-+]?[0-9]+)?
        if (*p == '-')
             p ++;
        if (*p == '0') {
             p ++;
            goto end_of_num;
        }

        if (!ISDIGIT(*p))
            goto error;
        do
            p ++;
        while (ISDIGIT(*p));

        if (*p == '.') {
             p ++;
            if (!ISDIGIT(*p))
                goto error;
            do
                p ++;
            while (ISDIGIT(*p));
        }

        if (*p == 'e' ||
            *p == 'E') {
             p ++;
            if (*p == '-' ||
                *p == '+')
                 p ++;
            if (!ISDIGIT(*p))
                goto error;
            do
                p ++;
            while (ISDIGIT(*p));
        }

    end_of_num:
        if (*p) goto error;

        r.type = json_litex_value_number_type;
        r.u.number.ptr = PTR_UCHAR_CAST(opt_arg);
        r.u.number.len = strlen(opt_arg);
    }
    else
    if (*opt_arg == '"') {
        size_t n = strlen(opt_arg) - 1;

        // stev: this isn't a JSON string
        // parser; nevertheless currently
        // is sufficient for the purposes
        // of this test program
        if (n == 0 || opt_arg[n] != '"')
            goto error;

        opt_arg[n] = 0;
        r.type = json_litex_value_string_type;
        r.u.string.ptr = PTR_UCHAR_CAST(opt_arg + 1);
        r.u.string.len = n - 1;
    }
    else {
    error:
        options_invalid_opt_arg(opt_name, opt_arg);
    }

    return r;
}

static size_t options_parse_list_optarg(
    const char* opt_name, const char* opt_arg,
    size_t* list, size_t n_list)
{
    size_t r;

    if (!su_size_parse_list(
            opt_arg, 0, 0, ',', list, n_list, NULL, NULL, &r))
        options_invalid_opt_arg(opt_name, opt_arg);
    ASSERT(r <= n_list);

    return r;
}

static char options_parse_char_optarg(
    const char* opt_name, const char* opt_arg)
{
    if (strlen(opt_arg) != 1)
        options_invalid_opt_arg(opt_name, opt_arg);
    return *opt_arg;
}

static bool options_parse(struct options_t* opts,
    int opt, const char* opt_arg)
{
    switch (opt) {
    case options_compile_expr_act:
        opts->action = options_compile_expr_action;
        break;
    case options_execute_expr_act:
        opts->action = options_execute_expr_action;
        break;
    case options_dump_bytecodes_act:
        opts->action = options_dump_bytecodes_action;
        break;
    case options_alpha_values_opt:
        opts->n_alpha =
            options_parse_list_optarg(
                "alpha_values", opt_arg, opts->alpha.vals,
                ARRAY_SIZE(opts->alpha.vals));
        break;
    case options_builtins_space_opt:
        opts->builtins_space =
            options_parse_builtins_space_optarg(
                "builtins-space", opt_arg);
        break;
    case options_expr_compiler_opt:
        opts->expr_compiler =
            options_parse_expr_compiler_optarg(
                "expr-compiler", opt_arg);
        break;
    case options_expression_opt:
        opts->expr = opt_arg;
        break;
    case options_literal_opt:
        opts->literal = options_parse_literal_optarg(
            "literal", CONST_CAST(opt_arg, char));
        break;
    case options_null_char_opt:
        opts->null_char = options_parse_char_optarg(
            "null-char", opt_arg);
        break;
    case options_no_null_char_opt:
        opts->null_char = 0;
        break;
    case options_no_rel_offs_opt:
        opts->rel_offs = false;
        break;
    case options_rel_offs_opt:
        opts->rel_offs = true;
        break;
    case options_no_verbose_opt:
        opts->verbose = false;
        break;
    case options_verbose_opt:
        opts->verbose = true;
        break;
    case options_help_sizes_opt:
        opts->usage_sizes = true;
        break;
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
    case options_sizes_ ## id ## _opt:                 \
        opts->sizes_ ## id = PARSE_SIZE_OPTARG(        \
            name, type, min, max);                     \
        break;
#include "test-expr-sizes.def"
    default:
        return false;
    }
    return true;
}

static const struct options_t* options(
    int argc, char* argv[])
{
    static struct options_t opts = {
        .action         = options_execute_expr_action,
        .builtins_space = options_builtins_space_default,
        .expr_compiler  = options_expr_compiler_flat,
        .expr           = NULL,
        .n_alpha        = 0,
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
        .sizes_ ## id   = def,
#include "test-expr-sizes.def"
        .null_char      = 0,
        .rel_offs       = false
    };

    static const struct option longs[] = {
        { "compile-expr",     0, 0, options_compile_expr_act },
        { "execute-expr",     0, 0, options_execute_expr_act },
        { "dump-bytecodes",   0, 0, options_dump_bytecodes_act },
        { "alpha-values",     1, 0, options_alpha_values_opt },
        { "builtins-space",   1, 0, options_builtins_space_opt },
        { "expr-compiler",    1, 0, options_expr_compiler_opt },
        { "compiler",         1, 0, options_expr_compiler_opt },
        { "expression",       1, 0, options_expression_opt },
        { "json-literal",     1, 0, options_literal_opt },
        { "literal",          1, 0, options_literal_opt },
        { "no-null-char",     0, 0, options_no_null_char_opt },
        { "null-char",        1, 0, options_null_char_opt },
        { "no-relative-offs", 0, 0, options_no_rel_offs_opt },
        { "relative-offs",    0, 0, options_rel_offs_opt },
        { "no-verbose",       0, 0, options_no_verbose_opt },
        { "verbose",          0, 0, options_verbose_opt },
#undef  CASE
#define CASE(type, id, name, pad, desc, min, max, def) \
        { "sizes-" name,      1, 0, options_sizes_ ## id ## _opt },
#include "test-expr-sizes.def"
        { "help-sizes",       0, 0, options_help_sizes_opt },
    };
    static const char shorts[] = "BCE" "a:b:c:e:l:n:rv";

    static const struct options_funcs_t funcs = {
        .done  = (options_done_func_t)  options_done,
        .parse = (options_parse_func_t) options_parse,
        .dump  = (options_dump_func_t)  options_dump,
    };

    opts.argc = INT_AS_SIZE(argc);
    opts.argv = argv;

    options_parse_args(
        &opts, &funcs,
        shorts, ARRAY_SIZE(shorts) - 1,
        longs, ARRAY_SIZE(longs),
        argc, argv);

    if (opts.usage_sizes) {
        options_usage_sizes();
        exit(0);
    }

    if (opts.expr == NULL)
        error("input literal expression not given");
    if (strlen(opts.expr) == 0)
        error("empty input literal expression given");

    if (opts.action == options_execute_expr_action &&
        JSON_LITEX_VALUE_IS(&opts.literal, string) &&
        opts.null_char > 0) {
        const char* p;
        char *q;

        p = PTR_CHAR_CAST_CONST(
                opts.literal.u.string.ptr);
        q = CONST_CAST(p, char);
        while ((q = strchr(q, opts.null_char)))
            *q ++ = 0;
    }

    if (opts.n_alpha) {
        size_t n = ARRAY_SIZE(opts.alpha.vals);

        SIZE_SUB_EQ(n, opts.n_alpha);
        SIZE_MUL_EQ(n, sizeof(*opts.alpha.vals));
        memset(opts.alpha.vals + opts.n_alpha, 0, n);
    }

    return &opts;
}

static void expr_error(
    const struct json_error_pos_t*,
    const char*, ...)
    PRINTF(2);

static void expr_error(
    const struct json_error_pos_t* pos,
    const char* msg, ...)
{
    fprintf(
        stderr, "%s: error:", program);

    if (pos && pos->line && pos->col)
        fprintf(stderr, "%zu:%zu: ",
            pos->line, pos->col);
    else
        fputc(' ', stderr);

    if (msg) {
        va_list a;

        va_start(a, msg);
        vfprintf(stderr, msg, a);
        va_end(a);
    }
}

struct expr_printer_t
{
    const struct json_litex_expr_rex_def_t*
           rexes;
    bits_t rel_offs: 1;
    FILE*  file;
    int    prec;
    size_t eos;
    size_t ip;
};

static void expr_printer_init(
    struct expr_printer_t* printer,
    const struct json_litex_expr_rex_def_t* rexes,
    bool rel_offs, FILE* file)
{
    memset(printer, 0, sizeof(*printer));

    printer->rexes = rexes;
    printer->rel_offs = rel_offs;
    printer->file = file;
}

static void expr_printer_print_rex(
    struct expr_printer_t* printer,
    json_litex_expr_rex_index_t rex)
{
    const struct json_litex_expr_rex_t* r;

    ASSERT(printer->rexes != NULL);

    ASSERT(rex < printer->rexes->size);
    r = printer->rexes->elems + rex;

    pretty_print_repr(printer->file,
        r->text, strulen(r->text),
        PRETTY_PRINT_REPR_FLAGS(
            quote_non_print,
            print_quotes));

    if (!r->opts) return;

    fputs(",\"", printer->file);

#define rex r
#define file printer->file
    JSON_LITEX_EXPR_REX_PRINT_OPT(ignore_case);
    JSON_LITEX_EXPR_REX_PRINT_OPT(dot_match_all);
    JSON_LITEX_EXPR_REX_PRINT_OPT(no_utf8_check);
    JSON_LITEX_EXPR_REX_PRINT_OPT(extended_pat);
#undef file
#undef rex

    fputc('"', printer->file);
}

static void expr_printer_print_node(
    struct expr_printer_t* printer,
    const struct json_litex_expr_node_t* node)
{
    fprintf(printer->file, "%*zu %s%s(",
        printer->prec, printer->ip,
        json_litex_expr_node_type_get_name(
            node->type),
        json_litex_expr_node_get_name_suffix(
            node));

#define NOP(n)
#define CMP(n)                             \
    fputs(json_litex_expr_cmp_op_get_name( \
        node->node.n), printer->file)
#define FMT(n, f) \
    fprintf(printer->file, "%" #f, node->node.n)
#define JMP(n)                           \
    do {                                 \
        size_t __a = node->node.n.offs;  \
        if (!printer->rel_offs) {        \
            __a += printer->ip;          \
            ASSERT(__a <= printer->eos); \
        }                                \
        if (printer->rel_offs ||         \
            __a < printer->eos)          \
            fprintf(printer->file,       \
                "%zu", __a);             \
        else                             \
            fputs("eos", printer->file); \
    } while (0)
#define STR(n)                               \
    pretty_print_repr(                       \
        printer->file,                       \
        node->node.n, strulen(node->node.n), \
        PRETTY_PRINT_REPR_FLAGS(             \
            quote_non_print,                 \
            print_quotes))
#define REX(n) \
    expr_printer_print_rex(printer, node->node.n)
#define BUILTIN(n) \
    FMT(n->name, s)
#undef  CASE
#define CASE(n, m, ...)              \
    case json_litex_expr_node_ ## n: \
        m(n, ## __VA_ARGS__);        \
        break

    switch (node->type) {
    CASE(const_id, BUILTIN);
    CASE(const_num, FMT, zu);
    CASE(const_str, STR);
    CASE(count_str, STR);
    CASE(count_rex, REX);
    CASE(match_str, STR);
    CASE(match_rex, REX);
    CASE(call_builtin, BUILTIN);
    CASE(jump_false, JMP);
    CASE(jump_true, JMP);
    CASE(make_bool, NOP);
    CASE(cmp_op, CMP);
    CASE(not_op, NOP);
    default:
        UNEXPECT_VAR("%d", node->type);
    }
    fputs(")\n", printer->file);

#undef BUILTIN
#undef REX
#undef STR
#undef JMP
#undef FMT
#undef CMP
#undef NOP
}

static inline size_t
    digits10(size_t n)
{
    size_t r = 0;
    do {
        n /= 10;
        r ++;
    }
    while (n > 0);
    return r;
}

static void expr_printer_print_def(
    struct expr_printer_t* printer,
    const struct json_litex_expr_def_t* def)
{
    const struct json_litex_expr_node_t *p, *e;
    size_t n;

    ASSERT(def != NULL);

    n = def->size;
    ASSERT(n > 0);

    printer->prec = digits10(n - 1);
    printer->eos = n;

    for (p = def->nodes,
         e = p + n;
         p < e;
         p ++) {
        expr_printer_print_node(printer, p);
        printer->ip ++;
    }
}

static void expr_printer_print_codes(
    struct expr_printer_t* printer,
    const struct json_litex_expr_rex_def_t* def)
{
    struct json_litex_expr_rex_bytes_t b;
    const uchar_t *p, *e;
    size_t k;

    if (def == NULL ||
        def->size == 0) return;

    json_litex_expr_rex_bytes_init(
        &b, &def->codes);

    for (k = 0,
         p = b.buf,
         e = p + b.size;
         p < e;
         p ++,
         k ++) {
        if ((k % 16) == 0)
            fprintf(printer->file, "%s%04zx",
                k ? "\n" : "", k);
        fprintf(printer->file, " %02x", *p);
    }
    fputc('\n', printer->file);

    json_litex_expr_rex_bytes_done(&b);
}

typedef void* (*expr_allocator_func_t)(
    void*, size_t, size_t);
typedef bool (*expr_deallocator_func_t)(
    void*, void*);

struct expr_allocator_t
{
    expr_allocator_func_t   allocate;
    expr_deallocator_func_t deallocate;
    void* obj;
};

struct expr_compiler_t
{
    const struct options_t*   opt;
    struct expr_allocator_t   alloc;
    struct json_litex_expr_t* expr;
};

#define EXPR_COMPILER_FLAT_SIZES()                    \
    {                                                 \
        .stack_max = opt->sizes_comp_flat_stack_max,  \
        .stack_init = opt->sizes_comp_flat_stack_init \
    }
#define EXPR_COMPILER_AST_SIZES()                    \
    {                                                \
        .pool_size = opt->sizes_comp_ast_pool_size,  \
        .stack_max = opt->sizes_comp_ast_stack_max,  \
        .stack_init = opt->sizes_comp_ast_stack_init \
    }
#define EXPR_COMPILER_SIZES()                     \
    {                                             \
        .flat = EXPR_COMPILER_FLAT_SIZES(),       \
        .ast = EXPR_COMPILER_AST_SIZES(),         \
        .nodes_max = opt->sizes_comp_nodes_max,   \
        .nodes_init = opt->sizes_comp_nodes_init, \
        .rexes_max = opt->sizes_comp_rexes_max,   \
        .rexes_init = opt->sizes_comp_rexes_init  \
    }

static struct expr_compiler_t*
    expr_compiler_create(
        const struct options_t* opt,
        struct expr_allocator_t alloc)
{
    enum json_litex_expr_opts_t o =
         json_litex_expr_opts_keep_regex_texts;
    enum json_litex_expr_impl_type_t t = 0;
    struct json_litex_expr_sizes_t s =
        EXPR_COMPILER_SIZES();
    struct expr_compiler_t* compiler;

    compiler = malloc(sizeof(*compiler));
    VERIFY(compiler != NULL);
    memset(compiler, 0, sizeof(*compiler));

    compiler->opt = opt;
    compiler->alloc = alloc;

    switch (opt->builtins_space) {
    case options_builtins_space_default:
        break; // stev: nop
    case options_builtins_space_alpha:
        o |= json_litex_expr_opts_alpha_builtins;
        break;
    default:
        UNEXPECT_VAR("%d", opt->builtins_space);
    }

    switch (opt->expr_compiler) {
    case options_expr_compiler_flat:
        t = json_litex_expr_flat_impl_type;
        break;
    case options_expr_compiler_ast:
        t = json_litex_expr_ast_impl_type;
        break;
    default:
        UNEXPECT_VAR("%d", opt->expr_compiler);
    }

    compiler->expr =
        json_litex_expr_create(
            o, t, &s,
            alloc.allocate,
            alloc.deallocate,
            alloc.obj);

    return compiler;
}

static void expr_compiler_destroy(
    struct expr_compiler_t* compiler)
{
    json_litex_expr_destroy(compiler->expr);
    free(compiler);
}

static void expr_compiler_print_error_header(
    struct expr_compiler_t* compiler)
{
    expr_error(
        &compiler->expr->error.pos,
        "compiler error: ");
}

static bool text_pos_get_offset(
    struct json_text_pos_t* pos,
    const char* buf, size_t len,
    size_t* off)
{
    const char *b, *p;
    size_t l, n, d;

    ASSERT(pos->line > 0);
    ASSERT(pos->col > 0);

    if (pos->line == 1) {
        d = pos->col - 1;

        if (d > len)
            return false;
        else {
            *off = d;
            return true;
        }
    }
    // => pos->line > 1

    for (b = buf,
         l = len,
         n = pos->line - 1;
         n && (p = memchr(b, '\n', l)) != NULL;
         n --) {
        d = PTR_DIFF(p + 1, b);
        SIZE_SUB_EQ(l, d);
        b = p + 1;
    }
    // => n == 0 || p == NULL

    if (n) return false;

    d = PTR_DIFF(b, buf);
    SIZE_ADD_EQ(d, pos->col - 1);

    *off = d;
    return true;
}

static void expr_compiler_print_error_desc(
    struct expr_compiler_t* compiler)
{
    const char *p, *e;
    size_t l, n, o;

    expr_compiler_print_error_header(
        compiler);
    json_litex_expr_print_error_desc(
        &compiler->expr->error,
        stderr);
    fputc('\n', stderr);

    if (!compiler->opt->verbose ||
        compiler->expr->error.pos.line == 0 ||
        compiler->expr->error.pos.col == 0)
        return;

    p = compiler->opt->expr;
    l = strlen(p);

    if (!text_pos_get_offset(
            &compiler->expr->error.pos,
            p, l, &o))
        ASSERT(false);

    expr_compiler_print_error_header(
        compiler);
    for (n = o + 1,
         e = p + l;
         p < e;
         p ++) {
        const char* f =
            pretty_print_fmt(*p, 0);
        size_t l =
            pretty_print_len(*p, 0);

        fprintf(stderr, f, (uchar_t) *p);
        if (p < compiler->opt->expr + o) {
            ASSERT_SIZE_DEC_NO_OVERFLOW(l);
            ASSERT_SIZE_ADD_NO_OVERFLOW(n, l - 1);
            n += l - 1;
        }
    }
    fputc('\n', stderr);

    expr_compiler_print_error_header(
        compiler);
    fprintf(
        stderr, "%*c\n", SIZE_AS_INT(n), '^');
}

static const struct json_litex_expr_def_t*
    expr_compiler_compile(
        struct expr_compiler_t* compiler)
{
    const struct json_litex_expr_def_t* d;
    struct json_text_pos_t p = {1, 0};

    if (!json_litex_expr_compile(
            compiler->expr,
            PTR_UCHAR_CAST_CONST(
                compiler->opt->expr),
            &p, 1, &d)) {
        expr_compiler_print_error_desc(
            compiler);
        return NULL;
    }

    return d;
}

static inline const struct json_litex_expr_rex_def_t*
    expr_compiler_get_rexes(
        struct expr_compiler_t* compiler)
{
    return json_litex_expr_get_rexes(
        compiler->expr);
}

static bool expr_compiler_run(
    struct expr_compiler_t* compiler)
{
    const struct json_litex_expr_rex_def_t* r;
    const struct json_litex_expr_def_t* d;
    struct expr_printer_t p;

    if (!(d = expr_compiler_compile(compiler)))
        return false;

    expr_printer_init(&p,
        r = expr_compiler_get_rexes(compiler),
        compiler->opt->rel_offs,
        stdout);

    if (compiler->opt->action ==
            options_dump_bytecodes_action)
        expr_printer_print_codes(&p, r);
    else
        expr_printer_print_def(&p, d);

    return true;
}

struct expr_executor_t
{
    const struct options_t*     opt;
    struct expr_allocator_t     alloc;
    struct json_litex_expr_vm_t expr;
};

#define EXPR_EXECUTOR_SIZES()                       \
    {                                               \
        .ovector_max = opt->sizes_exec_ovector_max, \
        .match_limit = opt->sizes_exec_match_limit, \
        .depth_limit = opt->sizes_exec_depth_limit, \
        .heap_limit = opt->sizes_exec_heap_limit,   \
        .stack_max = opt->sizes_exec_stack_max,     \
        .stack_init = opt->sizes_exec_stack_init    \
    }

static struct expr_executor_t*
    expr_executor_create(
        const struct options_t* opt,
        struct expr_allocator_t alloc)
{
    struct json_litex_expr_vm_sizes_t s =
        EXPR_EXECUTOR_SIZES();
    struct expr_executor_t* executor;

    executor = malloc(sizeof(*executor));
    VERIFY(executor != NULL);
    memset(executor, 0, sizeof(*executor));

    executor->opt = opt;
    executor->alloc = alloc;

    json_litex_expr_vm_init(
        &executor->expr,
        &s);

    return executor;
}

static void expr_executor_destroy(
    struct expr_executor_t* executor)
{
    json_litex_expr_vm_done(&executor->expr);
    free(executor);
}

static void expr_executor_print_error_desc(
    struct expr_executor_t* executor)
{
    expr_error(NULL, "executor error: ");
    json_litex_expr_vm_error_print_desc(
        &executor->expr.error,
        stderr);
    fputc('\n', stderr);
}

static bool expr_executor_execute(
    struct expr_executor_t* executor,
    json_litex_expr_value_t* val)
{
    const struct json_litex_expr_rex_def_t* r;
    const struct json_litex_expr_def_t* d;
    struct expr_compiler_t* c;

    c = expr_compiler_create(
            executor->opt, executor->alloc);
    d = expr_compiler_compile(c);
    r = expr_compiler_get_rexes(c);
    expr_compiler_destroy(c);

    if (d == NULL) return false;

    if (!json_litex_expr_vm_set_rexes(
            &executor->expr, r) ||
        !json_litex_expr_vm_execute(
            &executor->expr,
            &executor->opt->alpha, d,
            &executor->opt->literal,
            val)) {
        expr_executor_print_error_desc(
            executor);
        return false;
    }

    return true;
}

static bool expr_executor_run(
    struct expr_executor_t* executor)
{
    json_litex_expr_value_t v;

    if (!expr_executor_execute(executor, &v))
        return false;

    fprintf(stdout, "%zu\n", v);
    return true;
}

struct expr_alloc_info_t
{
    size_t size;
    void* ptr;
};

struct expr_t
{
    void  *impl;
    void (*destroy)(void*);
    bool (*run)(void*);

    struct pool_alloc_t      pool;
    struct expr_alloc_info_t alloc;
};

static void* expr_allocate(
    struct expr_t* expr, size_t size, size_t align)
{
    void* n;

    if (!(n = pool_alloc_allocate(
            &expr->pool, size, align)))
        fatal_error("expr pool alloc failed");

    expr->alloc.size = size;
    expr->alloc.ptr = n;

    memset(n, 0, size);

    return n;
}

static bool expr_deallocate(
    struct expr_t* expr, void* ptr)
{
    // stev: deallocate the last 'ptr' only
    if (ptr != NULL && expr->alloc.ptr == ptr) {
        expr->alloc.ptr = NULL;
        return pool_alloc_deallocate(
            &expr->pool, expr->alloc.size);
    }
    return false;
}

#define EXPR_INIT(n)                       \
    do {                                   \
        expr->impl =                       \
            expr_ ## n ## _create(opt, a); \
        expr->destroy = (void (*)(void*))  \
            expr_ ## n ## _destroy;        \
        expr->run = (bool (*)(void*))      \
            expr_ ## n ## _run;            \
    } while (0)

static void expr_init(
    struct expr_t* expr, const struct options_t* opt)
{
    struct expr_allocator_t a = {
        .allocate = (expr_allocator_func_t)
                     expr_allocate,
        .deallocate = (expr_deallocator_func_t)
                       expr_deallocate,
        .obj = expr
    };

    memset(expr, 0, sizeof(*expr));

    pool_alloc_init(
        &expr->pool, opt->sizes_pool_size);

    switch (opt->action) {
    case options_compile_expr_action:
    case options_dump_bytecodes_action:
        EXPR_INIT(compiler);
        break;
    case options_execute_expr_action:
        EXPR_INIT(executor);
        break;
    default:
        UNEXPECT_VAR("%d", opt->action);
    }
}

static void expr_done(struct expr_t* expr)
{
    expr->destroy(expr->impl);
    pool_alloc_done(&expr->pool);
}

static bool expr_run(struct expr_t* expr)
{
    return expr->run(expr->impl);
}

int main(int argc, char* argv[])
{
    const struct options_t* opt =
        options(argc, argv);
    struct expr_t expr;
    bool r;

    expr_init(&expr, opt);
    r = expr_run(&expr);
    expr_done(&expr);

    return !r;
}


