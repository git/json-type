// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#include <getopt.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>

#include "common.h"
#include "ptr-traits.h"
#include "pretty-print.h"
#include "su-size.h"

#include "test-common.h"

const char program[] = STRINGIFY(PROGRAM);
const char verdate[] = "0.1 -- 2018-09-28 21:06"; // $ date +'%F %R'

const char help[] =
"usage: %s [ACTION|OPTION]...\n"
"where the actions are:\n"
"  -T|--gen-trees=NUM   generate all binary trees of NUM operators\n"
"  -E|--gen-expr=NUM    generate all fully-parenthesized expressions\n"
"                         of NUM operators\n"
"  -P|--gen-part=PRIO   generate all fully-parenthesized expressions\n"
"                         along with all equivalent expressions that\n"
"                         are partially-parenthesized; PRIO is a list of\n"
"                         comma-separated decimal non-negative integers;\n"
"                         the number of elements of PRIO define the number\n"
"                         of operators occuring in each generated expression;\n"
"                         PRIO is specifying, in the given order, a priority\n"
"                         for each operator of each partially-parenthesized\n"
"                         generated expression; by convention, the lower the\n"
"                         priority of an operator is, the tighter it binds\n"
"  -M|--gen-perm=NUM    generate all permutations of `{ 0, 1, ..., NUM - 1 }'\n"
"and the options are:\n"
"  -r|--tree-repr=TYPE  when generating binary trees, print them represented\n"
"                         as specified by TYPE: 'links' -- for the arrays of\n"
"                         left and right links; 'graph' -- for textual tree\n"
"                         graph (default)\n"
"  -d|--[no-]dots       when generating binary trees that are represented as\n"
"                         graphs, fill empty grid cells with dot characters\n"
"                         instead of spaces\n"
"  -p|--[no-]all-part   when generating partially-parenthesized expressions,\n"
"                         produce all such expressions, marking with '+' those\n"
"                         that are and with '-' those that are not equivalent\n"
"                         with the correlated fully-parenthesized expression\n"
"  -l|--[no-]one-line   when generating partially-parenthesized expressions,\n"
"                         print all correlated expressions on the same line\n"
"     --dump-options    print options and exit\n"
"     --version         print version numbers and exit\n"
"  -?|--help            display this help info and exit\n";

enum options_action_t
{
    options_gen_trees_action,
    options_gen_expr_action,
    options_gen_part_action,
    options_gen_perm_action,
};

enum options_tree_repr_t
{
    options_tree_repr_links,
    options_tree_repr_graph,
};

struct options_t
{
    enum options_action_t
                 action;
    enum options_tree_repr_t
                 tree_repr;
    size_t       prio[128];
    size_t       n_prio;
    size_t       n_oper;
    size_t       n_perm;
    bits_t       dots: 1;
    bits_t       all_part: 1;
    bits_t       one_line: 1;
    size_t       argc;
    char* const *argv;
};

static void options_done(
    struct options_t* opts, size_t n_arg)
{
    ASSERT_SIZE_SUB_NO_OVERFLOW(
        opts->argc, n_arg);
    opts->argc -= n_arg;
    opts->argv += n_arg;
}

static void options_dump(const struct options_t* opts)
{
    static const char* const noyes[] = {
        [0] "no", [1] "yes"
    };
#define CASE2(n0, n1) \
    [options_ ## n0 ## _ ## n1 ## _action] = #n0 "-" #n1
    static const char* const actions[] = {
        CASE2(gen, trees),
        CASE2(gen, expr),
        CASE2(gen, part),
        CASE2(gen, perm),
    };
#define CASE(n) \
    [options_tree_repr_ ## n] = #n
    static const char* const tree_reprs[] = {
        CASE(links),
        CASE(graph),
    };
    const size_t n = TYPE_DIGITS10(size_t) + 1;
    char b[128], c[n], d[n];

#define NAME(x)  OPTIONS_NAME(x)
#define NNUL(x)  OPTIONS_NNUL(x)
#define NOYES(x) OPTIONS_NOYES(x)

    su_size_list_to_string(
        b, ARRAY_SIZE(b), opts->prio, opts->n_prio,
        ",", true);
    su_size_to_string(
        c, ARRAY_SIZE(c), opts->n_oper, true);
    su_size_to_string(
        d, ARRAY_SIZE(d), opts->n_perm, true);

    fprintf(stdout,
        "action:    %s\n"
        "tree-repr: %s\n"
        "prio:      %s\n"
        "n-oper:    %s\n"
        "n-perm:    %s\n"
        "dots:      %s\n"
        "all-part:  %s\n"
        "one-line:  %s\n"
        "argc:      %zu\n",
        NAME(action),
        NAME(tree_repr),
        b, c, d,
        NOYES(dots),
        NOYES(all_part),
        NOYES(one_line),
        opts->argc);

    pretty_print_strings(stdout,
        PTR_CONST_PTR_CAST(opts->argv, char),
        opts->argc, "argv", 11, 0);
}

enum {
    // stev: actions:
    options_gen_trees_act = 'T',
    options_gen_expr_act  = 'E',
    options_gen_part_act  = 'P',
    options_gen_perm_act  = 'M',

    // stev: options:
    options_tree_repr_opt = 'r',
    options_dots_opt      = 'd',
    options_all_part_opt  = 'p',
    options_one_line_opt  = 'l',

    options_no_dots_opt   = 256,
    options_no_all_part_opt,
    options_no_one_line_opt,
};

// $ . ~/lookup-gen/commands.sh
// $ print() { printf '%s\n' "$@"; }
// $ adjust-func() { ssed -R '1s/^/static /;1s/\&/*/;1s/(,\s+)/\1enum /;s/(?=t =)/*/;1s/(?<=\()/\n\t/;s/_t::/_/'; }

// $ print links graph|gen-func -f options_lookup_tree_repr -r options_tree_repr_t -Pf -q \!strcmp|adjust-func

static bool options_lookup_tree_repr(
    const char* n, enum options_tree_repr_t* t)
{
    // pattern: graph|links
    switch (*n ++) {
    case 'g':
        if (!strcmp(n, "raph")) {
            *t = options_tree_repr_graph;
            return true;
        }
        return false;
    case 'l':
        if (!strcmp(n, "inks")) {
            *t = options_tree_repr_links;
            return true;
        }
    }
    return false;
}

static enum options_tree_repr_t
    options_parse_tree_repr_optarg(
        const char* opt_name, const char* opt_arg)
{
    enum options_tree_repr_t r;

    ASSERT(opt_arg != NULL);
    if (!options_lookup_tree_repr(opt_arg, &r))
        options_invalid_opt_arg(opt_name, opt_arg);

    return r;
}

static size_t options_parse_list_optarg(
    const char* opt_name, const char* opt_arg,
    size_t* list, size_t n_list)
{
    size_t r;

    if (!su_size_parse_list(
            opt_arg, 0, 0, ',', list, n_list, NULL, NULL, &r))
        options_invalid_opt_arg(opt_name, opt_arg);
    ASSERT(r <= n_list);
    if (r == 1)
        options_illegal_opt_arg(opt_name, opt_arg);

    return r;
}

static bool options_parse(struct options_t* opts,
    int opt, const char* opt_arg)
{
    switch (opt) {
    case options_gen_trees_act:
        opts->action = options_gen_trees_action;
        opts->n_oper = options_parse_size_optarg(
            "gen-trees", opt_arg, 1, 0);
        opts->n_prio = 0;
        opts->n_perm = 0;
        break;
    case options_gen_expr_act:
        opts->action = options_gen_expr_action;
        opts->n_oper = options_parse_size_optarg(
            "gen-expr", opt_arg, 1, 0);
        opts->n_prio = 0;
        opts->n_perm = 0;
        break;
    case options_gen_part_act:
        opts->action = options_gen_part_action;
        opts->n_prio = options_parse_list_optarg(
            "gen-part", opt_arg, opts->prio,
            ARRAY_SIZE(opts->prio));
        opts->n_oper = 0;
        opts->n_perm = 0;
        break;
    case options_gen_perm_act:
        opts->action = options_gen_perm_action;
        opts->n_perm = options_parse_size_optarg(
            "gen-perm", opt_arg, 2, 0);
        opts->n_oper = 0;
        opts->n_prio = 0;
        break;
    case options_tree_repr_opt:
        opts->tree_repr = options_parse_tree_repr_optarg(
            "tree-repr", opt_arg);
        break;
    case options_dots_opt:
        opts->dots = true;
        break;
    case options_no_dots_opt:
        opts->dots = false;
        break;
    case options_all_part_opt:
        opts->all_part = true;
        break;
    case options_no_all_part_opt:
        opts->all_part = false;
        break;
    case options_one_line_opt:
        opts->one_line = true;
        break;
    case options_no_one_line_opt:
        opts->one_line = false;
        break;
    default:
        return false;
    }
    return true;
}

static const struct options_t* options(
    int argc, char* argv[])
{
    static struct options_t opts = {
        .action    = options_gen_expr_action,
        .tree_repr = options_tree_repr_graph,
        .n_prio    = 0,
        .n_oper    = 3,
        .n_perm    = 0,
        .dots      = false,
        .all_part  = false,
        .one_line  = false,
    };

    static const struct option longs[] = {
        { "gen-trees",   0, 0, options_gen_trees_act },
        { "gen-expr",    0, 0, options_gen_expr_act },
        { "gen-part",    1, 0, options_gen_part_act },
        { "gen-perm",    1, 0, options_gen_perm_act },
        { "tree-repr",   1, 0, options_tree_repr_opt },
        { "dots",        0, 0, options_dots_opt },
        { "no-dots",     0, 0, options_no_dots_opt },
        { "all-part",    0, 0, options_all_part_opt },
        { "no-all-part", 0, 0, options_no_all_part_opt },
        { "one-line",    0, 0, options_one_line_opt },
        { "no-one-line", 0, 0, options_no_one_line_opt },
    };
    static const char shorts[] = "E:M:P:T:" "dlpr:";

    static const struct options_funcs_t funcs = {
        .done  = (options_done_func_t)  options_done,
        .parse = (options_parse_func_t) options_parse,
        .dump  = (options_dump_func_t)  options_dump,
    };

    opts.argc = INT_AS_SIZE(argc);
    opts.argv = argv;

    options_parse_args(
        &opts, &funcs,
        shorts, ARRAY_SIZE(shorts) - 1,
        longs, ARRAY_SIZE(longs),
        argc, argv);

    return &opts;
}

typedef bool (*gen_visitor_func_t)(void*);

struct gen_visitor_t
{
    gen_visitor_func_t visit;
    void* this;
};

struct gen_expr_t
{
    struct gen_visitor_t
               visitor;
    size_t     n_oper;
    size_t*    lefts;
    size_t*    rights;
};

static void gen_expr_init(
    struct gen_expr_t* expr, size_t n_oper,
    struct gen_visitor_t visitor)
{
    size_t n = 2;

    ASSERT(n_oper > 0);

    expr->visitor = visitor;
    expr->n_oper = n_oper;

    SIZE_MUL_EQ(n, n_oper);
    SIZE_PRE_INC(n);
    SIZE_MUL_EQ(n, sizeof(size_t));

    expr->lefts = malloc(n);
    VERIFY(expr->lefts != NULL);

    memset(expr->lefts, 0, n);
    expr->rights = expr->lefts + n_oper + 1;
}

static void gen_expr_done(
    struct gen_expr_t* expr)
{
    free(expr->lefts);
}

static bool gen_expr_run(
    struct gen_expr_t* expr)
{
    size_t n, *l, *r, k, j, y;

    n = expr->n_oper;
    l = expr->lefts;
    r = expr->rights;

    // stev: Knuth, TAOCP, vol. 4A
    // Combinatorial algorithms, part 1
    // 7.2.1.6 Generating all trees
    // Algorithm B, p. 444

    // step B1: Initialize
    for (k = 1; k < n; k ++)
        l[k - 1] = k + 1;
    l[n] = 1;

    while (true) {
        // step B2: Visit
        if (!expr->visitor.visit(
                expr->visitor.this))
            return false;

        // step B3: Find 'j'
        for (j = 1; l[j - 1] == 0; j ++) {
            l[j - 1] = j + 1;
            r[j - 1] = 0;
        }
        if (j > n)
            break;

        // step B4: Find 'k' and 'y'
        k = 0;
        y = l[j - 1];
        while (r[y - 1] > 0) {
            k = y;
            y = r[y - 1];
        }

        // step B5: Promote 'y'
        if (k > 0)
            r[k - 1] = 0;
        else
            l[j - 1] = 0;
        r[y - 1] = r[j - 1];
        r[j - 1] = y;
    }

    return true;
}

static void gen_expr_print_links(
    const struct gen_expr_t* expr)
{
    size_t *p, *e;

    for (p = expr->lefts,
         e = p + expr->n_oper;
         p < e;
         p ++)
         printf("%zu ", *p);

    for (p = expr->rights,
         e = p + expr->n_oper;
         p < e;
         p ++)
         printf(" %zu", *p);
}

static void gen_expr_print_node(
    const struct gen_expr_t* expr, size_t k)
{
    ASSERT(k < expr->n_oper);

    if (!expr->lefts[k])
        putchar('x');
    else {
        putchar('(');
        gen_expr_print_node(
            expr, expr->lefts[k] - 1);
        putchar(')');
    }

    putchar('o');

    if (!expr->rights[k])
        putchar('x');
    else {
        putchar('(');
        gen_expr_print_node(
            expr, expr->rights[k] - 1);
        putchar(')');
    }
}

struct gen_impl_expr_t
{
    struct gen_expr_t expr;
};

static bool gen_impl_expr_print_tree(
    struct gen_impl_expr_t* impl)
{
    gen_expr_print_links(&impl->expr);
    putchar('\n');
    return true;
}

static bool gen_impl_expr_print_expr(
    struct gen_impl_expr_t* impl)
{
    gen_expr_print_node(&impl->expr, 0);
    putchar('\n');
    return true;
}

static void gen_impl_expr_init(
    struct gen_impl_expr_t* impl,
    size_t n_oper, bool trees)
{
    const struct gen_visitor_t v = {
        .visit = (gen_visitor_func_t)
            (trees ? gen_impl_expr_print_tree
                   : gen_impl_expr_print_expr),
        .this = impl
    };

    gen_expr_init(&impl->expr, n_oper, v);
}

static void gen_impl_expr_done(
    struct gen_impl_expr_t* impl)
{
    gen_expr_done(&impl->expr);
}

static bool gen_impl_expr_run(
    struct gen_impl_expr_t* impl)
{
    return gen_expr_run(&impl->expr);
}

struct gen_impl_tree_t
{
    struct gen_expr_t expr;
    char cell;
    char* grid;
    size_t width;
    size_t size;
    size_t x;
    size_t y;
    size_t n;
};

#define GRID_SET(i, j, c)              \
    do {                               \
        size_t __k = i;                \
        ASSERT(i < tree->width);       \
        ASSERT(j < tree->width);       \
        SIZE_MUL_EQ(__k, tree->width); \
        SIZE_ADD_EQ(__k, j);           \
        ASSERT(__k < tree->size);      \
        tree->grid[__k] = c;           \
    } while (0)
#define PUT_CHAR(c, i)           \
    do {                         \
        GRID_SET(i, tree->x, c); \
        tree->x ++;              \
    } while (0)

static void gen_impl_tree_gen_graph(
    struct gen_impl_tree_t* tree,
    size_t k, size_t l)
{
    const struct gen_expr_t* e =
        &tree->expr;

    ASSERT(k < e->n_oper);
    ASSERT(l < tree->width);

    if (!e->lefts[k])
        PUT_CHAR('x', l + 1);
    else
        gen_impl_tree_gen_graph(
            tree, e->lefts[k] - 1, l + 1);

    PUT_CHAR('o', l);

    if (!e->rights[k])
        PUT_CHAR('x', l + 1);
    else
        gen_impl_tree_gen_graph(
            tree, e->rights[k] - 1, l + 1);

    if (tree->y < l + 1)
        tree->y = l + 1;
}

static void gen_impl_tree_print_graph(
    struct gen_impl_tree_t* tree)
{
    size_t w = tree->width;
    char *p, *e;

    ASSERT(tree->x == w);
    ASSERT(tree->y < w);
    ASSERT(tree->y > 0);

    for (p = tree->grid,
         e = p + SIZE_MUL(tree->y + 1, w);
         p < e;
         p += w)
         printf("%.*s\n", SIZE_AS_INT(w), p);
}

static bool gen_impl_tree_gen(
    struct gen_impl_tree_t* tree)
{
    tree->x = tree->y = 0;
    memset(tree->grid, tree->cell, tree->size);
    gen_impl_tree_gen_graph(tree, 0, 0);

    if (tree->n ++) putchar('\n');
    gen_impl_tree_print_graph(tree);

    return true;
}

static void gen_impl_tree_init(
    struct gen_impl_tree_t* tree,
    size_t n_oper, bool dots)
{
    const struct gen_visitor_t v = {
        .visit = (gen_visitor_func_t)
                  gen_impl_tree_gen,
        .this = tree
    };
    size_t n = 2;

    ASSERT(n_oper > 0);

    SIZE_MUL_EQ(n, n_oper);
    SIZE_PRE_INC(n);
    tree->width = n;

    tree->size = SIZE_MUL(n, n);
    tree->grid = malloc(tree->size);
    VERIFY(tree->grid != NULL);

    tree->cell = dots ? '.' : ' ';

    gen_expr_init(&tree->expr, n_oper, v);
}

static void gen_impl_tree_done(
    struct gen_impl_tree_t* tree)
{
    gen_expr_done(&tree->expr);
    free(tree->grid);
}

static bool gen_impl_tree_run(
    struct gen_impl_tree_t* tree)
{
    return gen_expr_run(&tree->expr);
}

struct gen_impl_part_t
{
    struct gen_expr_t expr;
    bits_t all_part: 1;
    bits_t one_line: 1;
    const char* sep;
    const size_t* prio;
    size_t size;
    char* repr;
    char* repr2;
    size_t* lefts;
    size_t* rights;
    size_t* oper1;
    size_t* oper2;
    size_t i;
    size_t j;
    size_t k;
    size_t n;
    size_t p;
};

#undef  PUT_CHAR
#define PUT_CHAR(c)                   \
    ({                                \
        ASSERT(part->k < part->size); \
        part->repr[part->k ++] = c;   \
    })

#define PUT_LEFT()                         \
    ({                                     \
        size_t __i = part->i;              \
        ASSERT(part->i < e->n_oper - 1);   \
        part->lefts[part->i ++] = part->k; \
        PUT_CHAR('(');                     \
        __i;                               \
    })
#define PUT_RIGHT(i)               \
    ({                             \
        ASSERT(i < e->n_oper - 1); \
        part->rights[i] = part->k; \
        PUT_CHAR(')');             \
    })
#define PUT_OPER(o)              \
    ({                           \
        size_t __j = part->j ++; \
        ASSERT(o < e->n_oper);   \
        part->oper1[o] = __j;    \
        PUT_CHAR('o');           \
        __j;                     \
    })

static void gen_impl_part_gen_repr(
    struct gen_impl_part_t* part,
    size_t k, size_t o)
{
    const struct gen_expr_t* e =
        &part->expr;
    size_t i, j, n;

    ASSERT(k < e->n_oper);

    if (!e->lefts[k])
        PUT_CHAR('x');
    else {
        i = PUT_LEFT();
        gen_impl_part_gen_repr(
            part, e->lefts[k] - 1, i);
        PUT_RIGHT(i);
    }

    n = PUT_OPER(o);

    if (!e->rights[k])
        PUT_CHAR('x');
    else {
        j = PUT_LEFT();
        gen_impl_part_gen_repr(
            part, e->rights[k] - 1, j);
        PUT_RIGHT(j);
    }

    if (e->lefts[k]) {
        ASSERT(i < e->n_oper - 1);
        part->oper2[i] = n;
    }
    if (e->rights[k]) {
        ASSERT(j < e->n_oper - 1);
        part->oper2[j] = n;
    }
}

static void gen_impl_part_print_part(
    struct gen_impl_part_t* part,
    bool equiv)
{
    const char* s =
        part->p ++ ? part->sep : "";

    if (part->all_part)
        printf("%s%c%s",
            s, equiv ? '+' : '-',
            part->repr2);
    else
    if (equiv)
        printf("%s%s",
            s, part->repr2);
}

static void gen_impl_part_gen_sub_parts(
    struct gen_impl_part_t* part,
    size_t l, bool e)
{
    const size_t n =
        part->expr.n_oper - 1;
    char* s = part->repr2;
    size_t i, j, k;
    bool e2;

    ASSERT(l < n);

    for (k = l; k < n; k ++) {
        i = part->oper1[k];
        ASSERT(i <= n);

        j = part->oper2[k];
        ASSERT(j <= n);

        if ((e2 = e))
             e2 = part->prio[i] <
                  part->prio[j];
        if (!part->all_part && !e2)
            continue;

        i = part->lefts[k];
        ASSERT(i < part->size);
        ASSERT(s[i] == '(');
        s[i] = '.';

        j = part->rights[k];
        ASSERT(j < part->size);
        ASSERT(s[j] == ')');
        s[j] = '.';

        gen_impl_part_print_part(
            part, e2);

        if (k + 1 < n)
            gen_impl_part_gen_sub_parts(
                part, k + 1, e2);

        s[j] = ')';
        s[i] = '(';
    }
}

static void gen_impl_part_gen_parts(
    struct gen_impl_part_t* part)
{
    part->p = 0;
    strcpy(
        part->repr2,
        part->repr);
    gen_impl_part_print_part(
        part, true);
    gen_impl_part_gen_sub_parts(
        part, 0, true);
}

static bool gen_impl_part_gen(
    struct gen_impl_part_t* part)
{
    const size_t n = part->expr.n_oper;

    part->i = part->j = part->k = 0;
    memset(part->repr, 0, part->size + 1);

    gen_impl_part_gen_repr(part, 0, n - 1);
    ASSERT(part->k == part->size);
    ASSERT(part->i == n - 1);
    ASSERT(part->j == n);

    if (part->n ++) putchar('\n');
    gen_impl_part_gen_parts(part);
    if (!part->one_line) putchar('\n');

    return true;
}

static void gen_impl_part_init(
    struct gen_impl_part_t* part,
    const size_t* prio, size_t n_prio,
    bool all_part, bool one_line)
{
    const struct gen_visitor_t v = {
        .visit = (gen_visitor_func_t)
                  gen_impl_part_gen,
        .this = part
    };
    size_t n = 4, m = 4;

    ASSERT(n_prio > 0);

    part->prio = prio;
    part->all_part = all_part;
    part->one_line = one_line;
    part->sep = one_line ? " " : "\n";

    SIZE_MUL_EQ(n, n_prio);
    SIZE_PRE_DEC(n);
    part->size = n;

    part->repr = malloc(
        SIZE_MUL(n + 1, SZ(2)));
    VERIFY(part->repr != NULL);

    part->repr2 = part->repr + n + 1;

    // stev: 4 * (n_prio - 1) + 1
    SIZE_MUL_EQ(m, n_prio - 1);
    SIZE_PRE_INC(m);
    SIZE_MUL_EQ(m, sizeof(size_t));

    part->lefts = malloc(m);
    VERIFY(part->lefts != NULL);

    part->rights = part->lefts + n_prio - 1;
    part->oper1 = part->rights + n_prio - 1;
    part->oper2 = part->oper1 + n_prio;

    gen_expr_init(&part->expr, n_prio, v);
}

static void gen_impl_part_done(
    struct gen_impl_part_t* part)
{
    gen_expr_done(&part->expr);
    free(part->repr);
    free(part->lefts);
}

static bool gen_impl_part_run(
    struct gen_impl_part_t* part)
{
    bool r;

    r = gen_expr_run(&part->expr);
    if (part->one_line) putchar('\n');

    return r;
}

struct gen_perm_t
{
    struct gen_visitor_t
               visitor;
    size_t     n_elems;
    size_t*    elems;
};

static void gen_perm_init(
    struct gen_perm_t* perm, size_t n_elems,
    struct gen_visitor_t visitor)
{
    size_t *p, *e, i = 0;

    ASSERT(n_elems > 1);

    perm->visitor = visitor;
    perm->n_elems = n_elems;

    perm->elems = malloc(SIZE_MUL(
        n_elems + 1, sizeof(size_t)));
    VERIFY(perm->elems != NULL);

    p = perm->elems;
    e = p + n_elems + 1;

    *p ++ = 0;
    while (p < e)
        *p ++ = i ++;
}

static void gen_perm_done(
    struct gen_perm_t* perm)
{
    free(perm->elems);
}

static bool gen_perm_run(
    struct gen_perm_t* perm)
{
    size_t n, *a, j, k, l, t;

    n = perm->n_elems;
    a = perm->elems;

    // stev: Knuth, TAOCP, vol. 4A
    // Combinatorial algorithms, part 1
    // 7.2.1.2 Generating all permutations
    // Algorithm L, p. 319

    while (true) {
        // step L1: Visit
        if (!perm->visitor.visit(
                perm->visitor.this))
            return false;

        // step L2: Find 'j'
        j = n - 1;
        while (a[j] >= a[j + 1])
            j --;
        if (j == 0)
            break;

        // step L3: Increase 'a[j]'
        l = n;
        while (a[j] >= a[l])
            l --;
        t = a[j];
        a[j] = a[l];
        a[l] = t;

        // step L4: Reverse 'a[j+1]', ..., 'a[n]'
        for (k = j + 1,
             l = n;
             k < l;
             k ++,
             l --) {
            t = a[k];
            a[k] = a[l];
            a[l] = t;
        }
    }

    return true;
}

struct gen_impl_perm_t
{
    struct gen_perm_t perm;
};

static bool gen_impl_perm_print_perm(
    struct gen_impl_perm_t* impl)
{
    size_t *p, *e;

    for (p = impl->perm.elems + 1,
         e = p + impl->perm.n_elems;
         p < e;
         p ++)
         printf("%zu%c",
            *p, p < e - 1 ? ' ' : '\n');

    return true;
}

static void gen_impl_perm_init(
    struct gen_impl_perm_t* impl,
    size_t n_elems)
{
    const struct gen_visitor_t v = {
        .visit = (gen_visitor_func_t)
                  gen_impl_perm_print_perm,
        .this = impl
    };

    gen_perm_init(&impl->perm, n_elems, v);
}

static void gen_impl_perm_done(
    struct gen_impl_perm_t* impl)
{
    gen_perm_done(&impl->perm);
}

static bool gen_impl_perm_run(
    struct gen_impl_perm_t* impl)
{
    return gen_perm_run(&impl->perm);
}

struct gen_t
{
    union {
        struct gen_impl_expr_t expr;
        struct gen_impl_tree_t tree;
        struct gen_impl_part_t part;
        struct gen_impl_perm_t perm;
    };

    void  *impl;
    void (*done)(void*);
    bool (*run)(void*);
};

#define GEN_IMPL_INIT(n, ...)         \
    do {                              \
        gen_impl_ ## n ## _init(      \
            gen->impl = &gen->n,      \
            ## __VA_ARGS__);          \
        gen->done = (void (*)(void*)) \
            gen_impl_ ## n ## _done;  \
        gen->run = (bool (*)(void*))  \
            gen_impl_ ## n ## _run;   \
    } while (0)

static void gen_init(
    struct gen_t* gen,
    const struct options_t* opt)
{
    memset(gen, 0, sizeof(*gen));

    switch (opt->action) {

    case options_gen_trees_action:
        if (opt->tree_repr == options_tree_repr_graph)
            GEN_IMPL_INIT(tree, opt->n_oper, opt->dots);
        else
        if (opt->tree_repr == options_tree_repr_links)
            GEN_IMPL_INIT(expr, opt->n_oper, true);
        else
            UNEXPECT_VAR("%d", opt->tree_repr);
        break;

    case options_gen_expr_action:
        GEN_IMPL_INIT(expr, opt->n_oper, false);
        break;

    case options_gen_part_action:
        GEN_IMPL_INIT(part, opt->prio, opt->n_prio,
            opt->all_part, opt->one_line);
        break;

    case options_gen_perm_action:
        GEN_IMPL_INIT(perm, opt->n_perm);
        break;

    default:
        UNEXPECT_VAR("%d", opt->action);
    }
}

static void gen_done(struct gen_t* gen)
{
    gen->done(gen->impl);
}

static bool gen_run(struct gen_t* gen)
{
    return gen->run(gen->impl);
}

int main(int argc, char* argv[])
{
    const struct options_t* opt =
        options(argc, argv);
    struct gen_t gen;
    bool r;

    gen_init(&gen, opt);
    r = gen_run(&gen);
    gen_done(&gen);

    return !r;
}


