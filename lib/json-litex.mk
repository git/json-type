# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

PROGRAM := json-litex.so

PCRE2_HOME := /usr/local
PCRE2_INCLUDE := ${PCRE2_HOME}/include
PCRE2_LIB := ${PCRE2_HOME}/lib

PCRE2_VERSION := 1031

ifneq (${MAKECMDGOALS},clean)
ifneq (${MAKECMDGOALS},allclean)
PCRE2_VERSION_CHECK := $(shell \
	bash -c '. ../libpcre2-version.sh && \
		libpcre2-version-check "${PCRE2_LIB}" ${PCRE2_VERSION}' 2>&1)

ifneq (${PCRE2_VERSION_CHECK},)
$(error ${PCRE2_VERSION_CHECK})
endif
endif
endif

GCC_STD := gnu99
CFLAGS := -Wall -Wextra \
          -std=${GCC_STD} -g -I. -I.. -I${PCRE2_INCLUDE} \
          -fPIC -fvisibility=hidden -shared -fdata-sections -ffunction-sections \
          -Wl,--gc-sections -Wl,--entry=json_litex_filter_main \
          -L${PCRE2_LIB} -lpcre2-8 \
          -Wl,-soname=${PROGRAM} \
          -DPROGRAM=${PROGRAM} \
          -DJSON_NO_LIB_MAIN

JSON_DEBUG = $(shell \
	sed -nr '/^\s*\#\s*define\s+JSON_DEBUG\s*$$/{s//yes/;p;q}' json.h)

ifeq (${JSON_DEBUG},yes)
CFLAGS += -DJSON_DEBUG
endif

SRCS := lhash.c \
        common.c \
        su-size.c \
        file-buf.c \
        pool-alloc.c \
        json-litex.c

ifeq (${JSON_DEBUG},yes)
SRCS += debug.c
endif

DEP  := json-litex
BIN  := ${PROGRAM}

include common.mk


