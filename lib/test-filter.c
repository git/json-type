// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#define _GNU_SOURCE
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "int-traits.h"
#include "ptr-traits.h"
#include "char-traits.h"
#include "pretty-print.h"
#include "common.h"

#define   JSON_FILTER_NAME test
#include "json-filter.h"

#define ISASCII CHAR_IS_ASCII

static const char program[] = STRINGIFY(PROGRAM);
static const char verdate[] = "0.1 -- 2018-06-28 13:44"; // $ date +'%F %R'

enum test_options_error_type_t
{
    test_options_error_type_invalid_opt_arg,
    test_options_error_type_illegal_opt_arg,
    test_options_error_type_missing_opt_arg_str,
    test_options_error_type_missing_opt_arg_ch,
    test_options_error_type_not_allowed_opt_arg,
    test_options_error_type_invalid_opt_str,
    test_options_error_type_invalid_opt_ch,
};

struct test_options_error_opt_arg_t
{
    const char* name;
    const char* arg;
};

struct test_options_error_opt_str_t
{
    const char* name;
};

struct test_options_error_opt_ch_t
{
    char name;
};

struct test_options_error_t
{
    enum test_options_error_type_t type;
    union {
        struct test_options_error_opt_arg_t
               invalid_opt_arg;
        struct test_options_error_opt_arg_t
               illegal_opt_arg;
        struct test_options_error_opt_str_t
               missing_opt_arg_str;
        struct test_options_error_opt_ch_t
               missing_opt_arg_ch;
        struct test_options_error_opt_str_t
               not_allowed_opt_arg;
        struct test_options_error_opt_str_t
               invalid_opt_str;
        struct test_options_error_opt_ch_t
               invalid_opt_ch;
    };
};

#define TEST_OPTIONS_ERROR(t, n)           \
    ({                                     \
        err->type =                        \
            test_options_error_type_ ## t; \
        err->t.name = n;                   \
        false;                             \
    })
#define TEST_OPTIONS_ERROR_ARG(t, n, v)    \
    ({                                     \
        err->type =                        \
            test_options_error_type_ ## t; \
        err->t.name = n;                   \
        err->t.arg = v;                    \
        false;                             \
    })

enum test_options_action_t
{
    test_options_action_generic,
};

enum test_failure_type_t
{
    test_failure_type_none,
    test_failure_type_init,
    test_failure_type_handler,
    test_failure_type_exec
};

struct test_options_t
{
    enum test_options_action_t
                      action;
    enum test_failure_type_t
                      fail;
    bits_t            dump: 1;
    bits_t            usage: 1;
    bits_t            version: 1;
    size_t            argc;
    char* const      *argv;
};

#define FILTER_VERSION VERSION_ID(JSON_FILTER)

static void test_version(void)
{
    fprintf(stdout,
        "%s: version %s\n"
        "%s: json filter version: %s\n",
        program, verdate, program,
        STRINGIFY(FILTER_VERSION));
}

static void test_usage(void)
{
    fprintf(stdout,
        "usage: %s [ACTION|OPTION]...\n"
        "where actions are specified as:\n"
        "  -G|--generic         print out the text 'generic action'\n"
        "the options are:\n"
        "  -f|--fail=WHERE      set where the filter to fail or make\n"
        "     --no-fail           it to not fail at all; WHERE can be\n"
        "                         any of: 'init', 'handler' or 'exec'\n"
        "  -d|--dump-options    print parsed options and exit\n"
        "  -v|--version         print version numbers and exit\n"
        "  -?|--help            display this help info and exit\n",
        program);
}

#define NAME_(x, t)                  \
    ({                               \
        size_t __v = opts->x;        \
        ARRAY_NON_NULL_ELEM(t, __v); \
    })
#define NAME(x)  NAME_(x, x ## s)
#define NOYES(x) NAME_(x, noyes)

static void test_dump_opts(
    const struct test_options_t* opts)
{
#if 0
    static const char* const noyes[] = {
        [0] = "no",
        [1] = "yes"
    };
#endif
#undef  CASE
#define CASE(n) [test_options_action_ ## n] = #n
    static const char* actions[] = {
        CASE(generic)
    };
#undef  CASE
#define CASE(n) [test_failure_type_ ## n] = #n
    static const char* fails[] = {
        CASE(none),
        CASE(init),
        CASE(handler),
        CASE(exec)
    };

    fprintf(stdout,
        "action:    %s\n"
        "fail:      %s\n"
        "argc:      %zu\n",
        NAME(action),
        NAME(fail),
        opts->argc);

    pretty_print_strings(stdout,
        PTR_CONST_PTR_CAST(opts->argv, char),
        opts->argc, "argv", 11, 0);
}

// $ . ~/lookup-gen/commands.sh
// $ print() { printf '%s\n' "$@"; }
// $ print 'init =test_failure_type_init' 'handler =test_failure_type_handler' 'exec =test_failure_type_exec'|gen-func -f test_options_lookup_fail_name -Pf -q \!strcmp|ssed -R '1s/^/static /;1s/\s*result_t\&/ enum test_failure_type_t*/;1s/(?<=\()/\n\t/;s/(?=t =)/*/;s/result_t:://'

static bool test_options_lookup_fail_name(
    const char* n, enum test_failure_type_t* t)
{
    // pattern: exec|handler|init
    switch (*n ++) {
    case 'e':
        if (!strcmp(n, "xec")) {
            *t = test_failure_type_exec;
            return true;
        }
        return false;
    case 'h':
        if (!strcmp(n, "andler")) {
            *t = test_failure_type_handler;
            return true;
        }
        return false;
    case 'i':
        if (!strcmp(n, "nit")) {
            *t = test_failure_type_init;
            return true;
        }
    }
    return false;
}

static bool test_options_parse_fail_optarg(
    struct test_options_t* opts, const char* opt_arg,
    struct test_options_error_t* err)
{
    if (!test_options_lookup_fail_name(
            opt_arg, &opts->fail))
        return TEST_OPTIONS_ERROR_ARG(
            invalid_opt_arg, "fail", opt_arg);
    return true;
}

static bool test_options_parse(
    struct test_options_t* opts,
    const struct json_filter_options_t* input,
    struct test_options_error_t* err)
{
    enum {
        generic_act = 'G',
        fail_opt    = 'f',
        dump_opt    = 'd',
        help_opt    = '?',
        version_opt = 'v',
        no_fail_opt = 128,
    };
    static struct option longs[] = {
        { "generic",      0,       0, generic_act },
        { "fail",         1,       0, fail_opt },
        { "no-fail",      0,       0, no_fail_opt },
        { "dump-options", 0,       0, dump_opt },
        { "version",      0,       0, version_opt },
        { "help",         0, &optopt, help_opt },
        { 0,              0,       0, 0 }
    };
    static const char shorts[] = ":df:Gv";
    int opt;

    ASSERT(input->argc > 0);

    memset(opts, 0, sizeof(*opts));
    opts->argc = input->argc;
    opts->argv = input->argv;

#define argv_optind()                   \
    ({                                  \
        size_t i = INT_AS_SIZE(optind); \
        ASSERT_SIZE_DEC_NO_OVERFLOW(i); \
        ASSERT(i - 1 < opts->argc);     \
        opts->argv[i - 1];              \
    })

#define optopt_char()                   \
    ({                                  \
        ASSERT(ISASCII((char) optopt)); \
        (char) optopt;                  \
    })

#define opt_error TEST_OPTIONS_ERROR

#define missing_opt_arg_str(o) opt_error(missing_opt_arg_str, o)
#define missing_opt_arg_ch(o)  opt_error(missing_opt_arg_ch, o)
#define not_allowed_opt_arg(o) opt_error(not_allowed_opt_arg, o)
#define invalid_opt_str(o)     opt_error(invalid_opt_str, o)
#define invalid_opt_ch(o)      opt_error(invalid_opt_ch, o)

    opterr = 0;
    optind = 1;
    while ((opt = getopt_long(
            SIZE_AS_INT(opts->argc), opts->argv,
            shorts, longs, 0)) != EOF) {
        switch (opt) {
        case generic_act:
            opts->action = test_options_action_generic;
            break;
        case fail_opt:
            if (!test_options_parse_fail_optarg(
                    opts, optarg, err))
                return false;
            break;
        case no_fail_opt:
            opts->fail = test_failure_type_none;
            break;
        case dump_opt:
            opts->dump = true;
            break;
        case version_opt:
            opts->version = true;
            break;
        case 0:
            opts->usage = true;
            break;
        case ':': {
            const char* opt = argv_optind();
            if (opt[0] == '-' && opt[1] == '-')
                return missing_opt_arg_str(opt);
            else
                return missing_opt_arg_ch(optopt_char());
        }
        case '?':
        default:
            if (optopt == 0)
                return invalid_opt_str(argv_optind());
            else {
                char* opt = argv_optind();
                if (opt[0] != '-' || opt[1] != '-')
                    return invalid_opt_ch(optopt_char());
                else {
                    char* end = strchr(opt, '=');
                    if (end) *end = '\0';
                    if (end || optopt != '?')
                        return not_allowed_opt_arg(opt);
                    else
                        opts->usage = true;
                }
            }
            break;
        }
    }

#undef invalid_opt_ch
#undef invalid_opt_str
#undef not_allowed_opt_arg
#undef missing_opt_arg_ch
#undef missing_opt_arg_str

#undef opt_error
#undef optopt_char
#undef argv_optind

    ASSERT(INT_AS_SIZE(optind) <= opts->argc);

    opts->argc -= optind;
    opts->argv += optind;

    return true;
}

#define PRINT_ERROR(t, n, m, ...)             \
    do {                                      \
        STATIC(TYPEOF_IS(error->n, struct     \
            test_options_error_ ## t ## _t)); \
        fprintf(file, m, ## __VA_ARGS__);     \
    } while (0)

#undef  CASE
#define CASE(t, n, m, ...)                    \
    case test_options_error_type_ ## n:       \
        PRINT_ERROR(t, n, m, ## __VA_ARGS__); \
        break;

#define CASE_OPT_ARG(n, m) \
        CASE(opt_arg, n, m, error->n.name, error->n.arg)
#define CASE_OPT_STR(n, m) \
        CASE(opt_str, n, m, error->n.name)
#define CASE_OPT_CH(n, m) \
        CASE(opt_ch, n, m, error->n.name)

static void test_options_error_print_desc(
    const struct test_options_error_t* error,
    FILE* file)
{
    switch (error->type) {

    CASE_OPT_ARG(invalid_opt_arg,
        "invalid argument for '%s' option: '%s'");
    CASE_OPT_ARG(illegal_opt_arg,
        "illegal argument for '%s' option: '%s'");
    CASE_OPT_STR(missing_opt_arg_str,
        "argument for option '%s' not found");
    CASE_OPT_CH(missing_opt_arg_ch,
        "argument for option '-%c' not found");
    CASE_OPT_STR(not_allowed_opt_arg,
        "option '%s' does not allow an argument");
    CASE_OPT_STR(invalid_opt_str,
        "invalid command line option '%s'");
    CASE_OPT_CH(invalid_opt_ch,
        "invalid command line option '-%c'");

    default:
        UNEXPECT_VAR("%d", error->type);
    }
}

#undef CASE_OPT_CH
#undef CASE_OPT_STR
#undef CASE_OPT_ARG
#undef CASE

#undef PRINT_ERROR

enum test_filter_mode_t
{
    test_filter_mode_filter,
    test_filter_mode_exec,
};

enum test_filter_error_type_t
{
    test_filter_error_type_none,
    test_filter_error_type_opts,
    test_filter_error_type_exec,
    test_filter_error_type_fail,
};

struct test_filter_error_t
{
    enum test_filter_error_type_t   type;
    union {
        struct test_options_error_t opts;
        enum test_failure_type_t    fail;
    };
};

#define TEST_FILTER_ERROR(t)              \
    ({                                    \
        test->error.type =                \
            test_filter_error_type_ ## t; \
        TEST_FILTER_ERROR_RESULT;         \
    })
#define TEST_FILTER_ERROR_ARG(t, v)       \
    ({                                    \
        test->error.type =                \
            test_filter_error_type_ ## t; \
        test->error.t = v;                \
        TEST_FILTER_ERROR_RESULT;         \
    })
#define TEST_FILTER_ERROR_FAIL(w) \
    TEST_FILTER_ERROR_ARG(fail,   \
        test_failure_type_ ## w)

#define TEST_FILTER_ERROR_RESULT false

struct test_filter_t
{
    enum test_filter_mode_t    mode;
    struct test_options_t      opts;
    struct test_filter_error_t error;
    struct json_handler_obj_t  obj;
};

#define CALL_HANDLER(n, ...)                    \
    ({                                          \
          test->opts.fail ==                    \
          test_failure_type_handler             \
        ? TEST_FILTER_ERROR_FAIL(handler)       \
        : test->obj.handler != NULL &&          \
          test->obj.handler->n ## _func != NULL \
        ? test->obj.handler->n ## _func(        \
                test->obj.ptr, ## __VA_ARGS__)  \
        : true;                                 \
    })

static bool test_filter_null(struct test_filter_t* test)
{ return CALL_HANDLER(null); }

static bool test_filter_boolean(struct test_filter_t* test, bool val)
{ return CALL_HANDLER(boolean, val); }

static bool test_filter_number(struct test_filter_t* test,
    const uchar_t* val, size_t len)
{ return CALL_HANDLER(number, val, len); }

static bool test_filter_string(struct test_filter_t* test,
    const uchar_t* val, size_t len)
{ return CALL_HANDLER(string, val, len); }

static bool test_filter_object_start(struct test_filter_t* test)
{ return CALL_HANDLER(object_start); }

static bool test_filter_object_key(struct test_filter_t* test,
    const uchar_t* key, size_t len)
{ return CALL_HANDLER(object_key, key, len); }

static bool test_filter_object_sep(struct test_filter_t* test)
{ return CALL_HANDLER(object_sep); }

static bool test_filter_object_end(struct test_filter_t* test)
{ return CALL_HANDLER(object_end); }

static bool test_filter_array_start(struct test_filter_t* test)
{ return CALL_HANDLER(array_start); }

static bool test_filter_array_sep(struct test_filter_t* test)
{ return CALL_HANDLER(array_sep); }

static bool test_filter_array_end(struct test_filter_t* test)
{ return CALL_HANDLER(array_end); }

static bool test_filter_value_sep(struct test_filter_t* test)
{ return CALL_HANDLER(value_sep); }

static const struct json_handler_t handler = {
    .null_func         = (json_null_func_t)         test_filter_null,
    .boolean_func      = (json_boolean_func_t)      test_filter_boolean,
    .number_func       = (json_number_func_t)       test_filter_number,
    .string_func       = (json_string_func_t)       test_filter_string,
    .object_start_func = (json_object_start_func_t) test_filter_object_start,
    .object_key_func   = (json_object_key_func_t)   test_filter_object_key,
    .object_sep_func   = (json_object_sep_func_t)   test_filter_object_sep,
    .object_end_func   = (json_object_end_func_t)   test_filter_object_end,
    .array_start_func  = (json_array_start_func_t)  test_filter_array_start,
    .array_sep_func    = (json_array_sep_func_t)    test_filter_array_sep,
    .array_end_func    = (json_array_end_func_t)    test_filter_array_end,
    .value_sep_func    = (json_value_sep_func_t)    test_filter_value_sep,
};

size_t test_filter_get_version(void)
{ return JSON_FILTER_VERSION; }

#undef  TEST_FILTER_ERROR_RESULT
#define TEST_FILTER_ERROR_RESULT test

static struct test_filter_t* test_filter_create(
    enum test_filter_mode_t mode,
    const struct json_filter_options_t* opts,
    const struct json_handler_obj_t* obj,
    const struct json_handler_t** filter)
{
    struct test_options_error_t e;
    struct test_filter_t* test;

    test = malloc(sizeof(struct test_filter_t));
    VERIFY(test != NULL);

    test->mode = mode;
    test->obj = obj == NULL
        ? (struct json_handler_obj_t) {NULL, NULL}
        : *obj;
    test->error.type = test_filter_error_type_none;

    if (!test_options_parse(&test->opts, opts, &e))
        return TEST_FILTER_ERROR_ARG(opts, e);

    if (test->opts.fail == test_failure_type_init)
        return TEST_FILTER_ERROR_FAIL(init);

    if (filter != NULL) *filter = &handler;

    return test;
}

#undef  TEST_FILTER_ERROR_RESULT
#define TEST_FILTER_ERROR_RESULT false

struct test_filter_t* test_filter_create_filter(
    const struct json_filter_options_t* opts,
    const struct json_handler_obj_t* obj,
    const struct json_handler_t** filter)
{
    ASSERT(filter != NULL);
    return test_filter_create(
        test_filter_mode_filter, opts, obj, filter);
}

struct test_filter_t* test_filter_create_exec(
    const struct json_filter_options_t* opts)
{
    return test_filter_create(
        test_filter_mode_exec, opts, NULL, NULL);
}

void test_filter_destroy(struct test_filter_t* test)
{
    free(test);
}

bool test_filter_get_is_error(struct test_filter_t* test)
{
    return test->error.type != test_filter_error_type_none;
}

struct json_error_pos_t test_filter_get_error_pos(
    struct test_filter_t* test UNUSED)
{
    return (struct json_error_pos_t) {0, 0};
}

const struct json_file_info_t* test_filter_get_error_file(
    struct test_filter_t* test UNUSED)
{
    return NULL;
}

void test_filter_print_error_desc(
    struct test_filter_t* test,
    FILE* file)
{
#undef  CASE
#define CASE(n) [test_failure_type_ ## n] = #n
    static const char* const failures[] = {
        CASE(none),
        CASE(init),
        CASE(handler),
        CASE(exec),
    };

    const struct test_filter_error_t* e =
        &test->error;

#undef  CASE
#define CASE(n) case test_filter_error_type_ ## n

    switch (e->type) {

    CASE(none):
        fputs("no error", file);
        break;
    CASE(opts):
        test_options_error_print_desc(
            &e->opts, file);
        break;
    CASE(exec):
        fputs("filter not in exec mode", file);
        break;
    CASE(fail): {
        const char* f = ARRAY_NULL_ELEM(
            failures, e->fail);

        ASSERT(f != NULL);
        fprintf(file, "%s failure", f);
        break;
    }

    default:
        UNEXPECT_VAR("%d", e->type);
    }
}

static void test_filter_generic(
    struct test_filter_t* test UNUSED)
{
    fprintf(stdout, "%s: generic action\n", program);
}

#undef  CASE
#define CASE(n) \
    [test_options_action_ ## n] = test_filter_ ## n

bool test_filter_exec(struct test_filter_t* test)
{
    typedef void (*act_func_t)(struct test_filter_t*);
    static const act_func_t acts[] = {
        CASE(generic)
    };
    act_func_t act;

    if (test->opts.version)
        test_version();
    if (test->opts.dump)
        test_dump_opts(&test->opts);
    if (test->opts.usage)
        test_usage();

    if (test->opts.dump ||
        test->opts.version ||
        test->opts.usage)
        return true;

    if (test->opts.fail == test_failure_type_exec)
        return TEST_FILTER_ERROR_FAIL(exec);
    if (test->mode != test_filter_mode_exec)
        return TEST_FILTER_ERROR(exec);

    act = ARRAY_NULL_ELEM(acts, test->opts.action);
    ASSERT(act != NULL);

    act(test);

    return true;
}

#ifdef CONFIG_DYNAMIC_LINKER

JSON_API const char test_interp[]
    __attribute__((section(".interp"))) =
    CONFIG_DYNAMIC_LINKER;

void test_filter_main(void)
    NORETURN;

void test_filter_main(void)
{
    test_version();
    fflush(stdout);

    _exit(0);
}

#endif


