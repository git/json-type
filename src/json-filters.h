// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __JSON_FILTERS_H
#define __JSON_FILTERS_H

#include <stdio.h>

#include "lib/json.h"
#include "lib/json-filter.h"

#include "common.h"

enum json_filter_config_param_t
{
    json_filter_verbose_error_config,
};

struct json_filter_lib_t;

struct json_filter_lib_t* json_filter_lib_create(
    const struct options_filter_t* opts);

void json_filter_lib_destroy(struct json_filter_lib_t* filter);

void json_filter_lib_config_set_param(
    struct json_filter_lib_t* filter, enum json_filter_config_param_t param,
    bool val);

bool json_filter_lib_exec(struct json_filter_lib_t* filter);

bool json_filter_lib_get_is_error(struct json_filter_lib_t* filter);

struct json_error_pos_t json_filter_lib_get_error_pos(
    struct json_filter_lib_t* filter);

const struct json_file_info_t* json_filter_lib_get_error_file(
    struct json_filter_lib_t* filter);

void json_filter_lib_print_error_desc(
    struct json_filter_lib_t* filter, FILE* file);

struct json_filter_chain_t;

struct json_filter_chain_t* json_filter_chain_create(
    const struct options_filter_t* optv, size_t n_optv,
    struct json_handler_obj_t* obj);

void json_filter_chain_destroy(
    struct json_filter_chain_t* chain);

void json_filter_chain_config_set_param(
    struct json_filter_chain_t* chain,
    enum json_filter_config_param_t param,
    bool val);

bool json_filter_chain_get_is_error(
    struct json_filter_chain_t* chain);

struct json_error_pos_t json_filter_chain_get_error_pos(
    struct json_filter_chain_t* chain);

const struct json_file_info_t* json_filter_chain_get_error_file(
    struct json_filter_chain_t* chain);

void json_filter_chain_print_error_desc(
    struct json_filter_chain_t* chain, FILE* file);

#endif /*__JSON_FILTERS_H*/

