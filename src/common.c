// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#define _GNU_SOURCE
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <alloca.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <ctype.h>
#include <locale.h>
#include <time.h>
#include <link.h>

#include "lib/json.h"
#include "lib/json-filter.h"
#include "lib/pretty-print.h"
#include "lib/su-size.h"

#include "common.h"
#include "char-traits.h"

#ifdef DEBUG
#define PRINT_DEBUG_COND true
#include "debug.h"
#endif

#define STRINGIFY_(s) #s
#define STRINGIFY(s)  STRINGIFY_(s)

#define ISASCII CHAR_IS_ASCII
#define ISDIGIT CHAR_IS_DIGIT

static const char program[] = STRINGIFY(PROGRAM);
static const char library[] = STRINGIFY(LIBRARY);
static const char verdate[] = "0.9.6 -- 2021-07-22 11:22"; // $ date +'%F %R'

static const char license[] =
"Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas.\n"
"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n"
"This is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n";

const char stdin_name[] = "<stdin>";

typedef char fmt_buf_t[512];

enum error_type_t { FATAL, WARNING, ERROR };

static void verror(
    enum error_type_t err, const char* fmt, bool nl,
    va_list arg)
{
    static const char* const errors[] = {
        [FATAL]   = "fatal error",
        [WARNING] = "warning",
        [ERROR]   = "error",
    };
    static const char* const fmts[] = {
        "%s: %s: %s\n",
        "%s: %s: %s"
    };
    fmt_buf_t b;

    vsnprintf(b, sizeof b - 1, fmt, arg);
    b[sizeof b - 1] = 0;

    fprintf(stderr, fmts[!nl], program, errors[err], b);
}

void fatal_error(const char* fmt, ...)
{
    va_list arg;

    va_start(arg, fmt);
    verror(FATAL, fmt, true, arg);
    va_end(arg);

    exit(127);
}

void assert_failed(
    const char* file, int line, const char* func,
    const char* expr)
{
    fatal_error(
        "assertion failed: %s:%d:%s: %s",
        file, line, func, expr);
}

void unexpect_error(
    const char* file, int line, const char* func,
    const char* msg, ...)
{
    va_list arg;
    fmt_buf_t b;

    va_start(arg, msg);
    vsnprintf(b, sizeof b - 1, msg, arg);
    va_end(arg);

    b[sizeof b - 1] = 0;

    fatal_error(
        "unexpected error: %s:%d:%s: %s",
        file, line, func, b);
}

void warning(const char* fmt, ...)
{
    va_list arg;

    va_start(arg, fmt);
    verror(WARNING, fmt, true, arg);
    va_end(arg);
}

void error(const char* fmt, ...)
{
    va_list arg;

    va_start(arg, fmt);
    verror(ERROR, fmt, true, arg);
    va_end(arg);

    exit(1);
}

static void error_fmt(bool nl, const char* fmt, ...)
    PRINTF(2);

static void error_fmt(bool nl, const char* fmt, ...)
{
    va_list arg;

    va_start(arg, fmt);
    verror(ERROR, fmt, nl, arg);
    va_end(arg);
}

void oom_error(const char* fmt, ...)
{
    va_list arg;
    fmt_buf_t b;

    va_start(arg, fmt);
    vsnprintf(b, sizeof b - 1, fmt, arg);
    va_end(arg);

    b[sizeof b - 1] = 0;

    fatal_error("out of memory: %s", b);
}

void sys_error(const char* fmt, ...)
{
    int sys = errno;
    va_list arg;
    fmt_buf_t b;

    va_start(arg, fmt);
    vsnprintf(b, sizeof b - 1, fmt, arg);
    va_end(arg);

    b[sizeof b - 1] = 0;

    error("%s: %s", b, strerror(sys));
}

void pos_error_header(
    const char* file, size_t line, size_t col)
{
    error_fmt(
        false,
        line && col ? "%s:%zu:%zu: " : "%s: ",
        file ? file : stdin_name,
        line, col);
}

void pos_error(
    const char* file, size_t line, size_t col,
    const char* fmt, ...)
{
    va_list arg;

    pos_error_header(file, line, col);

    va_start(arg, fmt);
    vfprintf(stderr, fmt, arg);
    va_end(arg);

    fputc('\n', stderr);
}

bool mem_find(
    const uchar_t* buf, size_t len, char ch,
    size_t* pos)
{
    const uchar_t* p;

    if ((p = memchr(buf, ch, len)) != NULL) {
        *pos = PTR_OFFSET(p, buf, len);
        return true;
    }
    return false;
}

bool mem_rfind(
    const uchar_t* buf, size_t len, char ch,
    size_t* pos)
{
    const uchar_t* p;

    if ((p = memrchr(buf, ch, len)) != NULL) {
        *pos = PTR_OFFSET(p, buf, len);
        return true;
    }
    return false;
}

size_t mem_count(
    const uchar_t* buf, size_t len, char ch)
{
    const uchar_t *b = buf, *p;
    size_t l = len, r = 0;

    while ((p = memchr(b, ch, l)) != NULL) {
        size_t d = PTR_OFFSET(p, b, l);

        b += ++ d;
        l -= d;

        ASSERT(PTR_IS_IN_BUF(b, l, buf, len));

        r ++;
    }

    return r;
}

bool mem_find_nth(
    const uchar_t* buf, size_t len, char ch,
    size_t n, size_t* pos)
{
    const uchar_t *b = buf, *p;
    size_t l = len, d;

    ASSERT(n > 0);
    while ((p = memchr(b, ch, l)) != NULL) {
        if (-- n == 0) {
            *pos = PTR_OFFSET(p, buf, len);
            return true;
        }
        d = PTR_OFFSET(p, b, l);

        b += ++ d;
        l -= d;

        ASSERT(PTR_IS_IN_BUF(b, l, buf, len));
    }

    return false;
}

static void sigpipe_no_error(
    int signal UNUSED)
{
    _exit(0);
}

static void init_sigpipe_action(
    enum options_sigpipe_act_t act)
{
    static void (* const acts[])(int) = {
        [options_sigpipe_act_error]    = SIG_DFL,
        [options_sigpipe_act_no_error] = sigpipe_no_error,
        [options_sigpipe_act_ignore]   = SIG_IGN,
    };
    void (* const* f)(int);
    struct sigaction a;

    f = ARRAY_NULL_ELEM_REF(acts, act);
    VERIFY(f != NULL);

    memset(&a, 0, sizeof a);
    a.sa_handler = *f;

    sigaction(SIGPIPE, &a, NULL);
}

static int dl_callback(
    struct dl_phdr_info* info,
    size_t size UNUSED, void* result)
{
    const char **r = result, *p;

    if (info->dlpi_name &&
        (p = strrchr(info->dlpi_name, '/')) &&
        !strcmp(p + 1, library)) {
        *r = info->dlpi_name;
        return 1;
    }
    return 0;
}

static char* lib_runtime_path(void)
{
    const char* r = NULL;
    char *p, *q;

    if (!dl_iterate_phdr(dl_callback, &r))
        return NULL;
    ASSERT(r != NULL);

    if (!(p = realpath(r, NULL)))
        return NULL;

    if (!(q = strrchr(p, '/'))) {
        free(p);
        return NULL;
    }

    if (q == p) q ++;
    *q = 0;

    return p;
}

struct lib_version_t
{
    unsigned short major;
    unsigned short minor;
    unsigned short patch;
    char*          build;
    char*          rpath;
};

static struct lib_version_t lib_version(
    bool extra_info)
{
    enum { M = 10000, m = 100, d = 20 };
    struct lib_version_t r;
    const char *b, *c;
    struct tm t;
    size_t v;

    memset(&r, 0, sizeof(r));

    v = json_version();
    ASSERT(v > 0 && v < M * 10);

    r.major = v / M;
    v %= M;
    r.minor = v / m;
    r.patch = v % m;

    if (!extra_info)
        return r;

    b = json_build_datetime();
    ASSERT(b != NULL);

    c = setlocale(LC_TIME, "en_US");
    ASSERT(c != NULL);

    memset(&t, 0, sizeof(t));
    if (!strptime(b, "%b %d %Y %H:%M:%S", &t))
        fatal_error("failed parsing library build date");

    c = setlocale(LC_TIME, c);
    ASSERT(c != NULL);

    r.build = malloc(d);
    if (r.build == NULL)
        OOM_ERROR("malloc failed creating "
            "lib_version_t::build");

    v = strftime(r.build, d, "%Y-%m-%d %H:%M:%S", &t);
    ASSERT(v);

    r.rpath = lib_runtime_path();

    return r;
}

static void lib_version_done(struct lib_version_t* ver)
{
    free(ver->rpath);
    free(ver->build);
}

static void lib_version_check(void)
{
    struct lib_version_t lib;

    lib = lib_version(false);
    if (lib.major == JSON_VERSION_MAJOR &&
        lib.minor == JSON_VERSION_MINOR)
        return;

    fatal_error(
        "json library is not of expected "
        "version %d.%d but %d.%d.%d",
        JSON_VERSION_MAJOR,
        JSON_VERSION_MINOR,
        lib.major, lib.minor,
        lib.patch);
}

#if JSON_VERSION <= 0 || JSON_VERSION >= 100000
#error "JSON_VERSION has an invalid value"
#endif

#ifndef JSON_DEBUG
#define JSON_DEBUG_STATE "disabled"
#else
#define JSON_DEBUG_STATE "enabled"
#endif

#define FILTER_VERSION VERSION_ID(JSON_FILTER)

static void version(void)
{
    struct lib_version_t v;

    v = lib_version(true);

    fprintf(stdout, 
        "%s: version %s\n"
        "%s: %s: version: %u.%u.%u\n"
        "%s: %s: build: %s\n"
        "%s: %s: path: %s\n"
        "%s: %s: JSON_DEBUG is " JSON_DEBUG_STATE "\n"
        "%s: json filter version: %s\n\n%s",
        program, verdate,
        program, library, v.major, v.minor, v.patch,
        program, library, v.build,
        program, library, v.rpath
            ? v.rpath : "???",
        program, library, program,
        STRINGIFY(FILTER_VERSION),
        license
    );

    lib_version_done(&v);
}

#define PARSE_BUF_SIZE_OPTARG   parse_su_size_optarg
#define PARSE_STACK_SIZE_OPTARG parse_su_size_optarg
#define PARSE_POOL_SIZE_OPTARG  parse_su_size_optarg
#define PARSE_WIDTH_SIZE_OPTARG parse_size_optarg

#define PARSE_SIZE_OPTARG(name, type, min, max) \
    PARSE_ ## type ## _SIZE_OPTARG(             \
        "sizes-" name, optarg, min, max)

#define SU_SIZE_(n) \
    struct su_size_t n ## _su = su_size(opts->n);

#define SU_BUF_SIZE      SU_SIZE_
#define SU_STACK_SIZE    SU_SIZE_
#define SU_POOL_SIZE     SU_SIZE_
#define SU_WIDTH_SIZE(n)

#define SU_SIZE(id, type) SU_ ## type ## _SIZE(sizes_ ## id)

#define FMT_SU_SIZE    "%zu%s"
#define FMT_BUF_SIZE   FMT_SU_SIZE
#define FMT_STACK_SIZE FMT_SU_SIZE
#define FMT_POOL_SIZE  FMT_SU_SIZE
#define FMT_WIDTH_SIZE "%zu"

#define FMT_SIZE(type) FMT_ ## type ## _SIZE

#define ARG_SU_SIZE(n)    n ## _su.sz, n ## _su.su
#define ARG_BUF_SIZE      ARG_SU_SIZE
#define ARG_STACK_SIZE    ARG_SU_SIZE
#define ARG_POOL_SIZE     ARG_SU_SIZE
#define ARG_WIDTH_SIZE(n) opts->n

#define ARG_SIZE(id, type) ARG_ ## type ## _SIZE(sizes_ ## id),

#define HELP_(h)
#define HELP__(h) h "\n"

#define HELP_b HELP__
#define HELP_e HELP__

#define HELP_LETTER(help, letter) \
    HELP_ ## letter(help)

#define STRUCT_(o, n)
#define STRUCT__(o, n) { o, 1, 0, sizes_ ## n ## _opt },

#define STRUCT_b STRUCT__
#define STRUCT_e STRUCT__

#define STRUCT_LETTER(name, id, letter) \
    STRUCT_ ## letter(name, id)

#define ARG_(l)
#define ARG__(l) #l ":"

#define ARG_b ARG__
#define ARG_e ARG__

#define ARG_LETTER(letter) ARG_ ## letter(letter)

#define CH_b 'b'
#define CH_e 'e'
#define CH(l) CH_ ## l

#define LETTER_(n, l)
#define LETTER__(n, l) n = CH(l),

#define LETTER_b LETTER__
#define LETTER_e LETTER__

#define LETTER(n, l) LETTER_ ## l(sizes_ ## n, l)

#define OPT_LETTER(id, letter) \
    LETTER(id ## _opt, letter)

#define NON_LETTER_(n) n,
#define NON_LETTER_b(n)
#define NON_LETTER_e(n)

#define NON_LETTER(n, l) NON_LETTER_ ## l(sizes_ ## n)

#define OPT_NON_LETTER(id, letter) \
    NON_LETTER(id ## _opt, letter)

static void usage(void)
{
    fprintf(stdout,
        "usage: %s [ACTION|OPTION]... [FILE] [-- FILTER_LIB [FILTER_ARG]...]...\n"
        "where actions are specified as:\n"
        "  -O|--parse-only              run 'json-parser': only parse the input or\n"
        "  -E|--echo-parse                parse and echo out verbatim the input\n"
        "  -P|--pretty-print            run 'json-printer': reprint prettily or\n"
        "  -R|--terse-print               tersely the input JSON objects; otherwise\n"
        "  -Y|--type-print                build the AST of the JSON input and print\n"
        "  -A|--ast-print                 out either its type object or the AST or\n"
        "  -M|--from-ast-print            reprint prettily from AST the JSON input\n"
        "  -T|--typelib-func [NAME]     run 'json-typelib' on the input typelib for\n"
        "                                 the named function:\n"
        "                                   v|validate    check the validity of input;\n"
        "                                   p|print       print out the constructed type;\n"
        "                                   a|attr        print out the constructed type\n"
        "                                                 along with generated attributes;\n"
        "                                   c|check-attr  check the constructed type for to\n"
        "                                                 validate the generated attributes\n"
        "                                   A|print-check-attr\n"
        "                                                 print out the constructed type\n"
        "                                                 along with generated attributes,\n"
        "                                                 then validate these attributes\n"
        "                                   d|gen-def     generate the type def code for\n"
        "                                                 the constructed type\n"
        "                                 the default is validate\n"
        "  -F|--filterlib-exec          run 'json-filterlib': execute the filter library\n"
        "                                 specified by the first command line argument\n"
        "                                 group of form `-- FILTER_LIB [FILTER_ARG]...';\n"
        "                                 if the invoking command line specifies an input\n"
        "                                 file name (prior to the first `--'), then take\n"
        "                                 that to be library name and load the library by\n"
        "                                 passing to it the argument `--help' only\n"
        "  -S|--escape-json             run 'json-escape': produce the JSON-escaped text\n"
        "                                 of the input UTF8 text given\n"
        "  -J|--json2                   run 'json2': flatten the hierarchical structure\n"
        "                                 of the JSON input to a series of text lines of\n"
        "                                 form /PATH/NAME=VALUE (default)\n"
//!!!2JSON		"  -2|--2json                 run '2json'\n"
        "the options are:\n"
        "  -d|--type-def TYPE           use the specified type definition or library for\n"
        "  -t|--type-lib FILE[:NAME]      type checking the JSON input or, otherwise, do\n"
        "  -n|--type-name [NAME]          not check the input for types at all (default);\n"
        "     --no-type-check             the optional name specifies which type to use\n"
        "                                 in case the def/library is made of an array\n"
        "                                 of named types instead of just a sole type;\n"
        "                                 type checking doesn't apply yet to the AST\n"
        "                                 options `-Y|--type-print', `-A|--ast-print'\n"
        "                                 and `-M|--from-ast-print'\n"
        "  -f|--[no-]filter-libs        build a JSON input filtering chain using the\n"
        "                                 specified filter libraries and arguments or,\n"
        "                                 otherwise, do no filter the input at all;\n"
        "                                 the filtering chain is specified as a series\n"
        "                                 of one or more command line arguments groups\n"
        "                                 of form `-- FILTER_LIB [FILTER_ARG]...'; the\n"
        "                                 filtering series is placed immediately after\n"
        "                                 the main program's command line arguments\n"
        "  -i|--input-file FILE         take input from the named file (default: '-')\n"
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        HELP_LETTER(help, letter)
#include "opt-sizes.def"
        "  -l|--[no-]liter[al]-value    allow literal values on input or otherwise\n"
        "                                 do not (default not)\n"
        "  -s|--[no-]surrogate-pairs    allow surrogate pairs in JSON strings (default do not)\n"
        "                                 when action is anything but `-S|--escape-json';\n"
        "                                 when action is `-S|--escape-json', produce\n"
        "                                 surrogate pairs on output (default do not)\n"
        "                                 when given `-u|--validate-utf8' too; given along\n"
        "                                 with `-c|--escape-utf8', generates surrogate pairs\n"
        "                                 in the strings produced by `-P|--pretty-print',\n"
        "                                 `-R|--terse-print', `Y|--type-print',\n"
        "                                 `-A|--ast-print' or `M|--from-ast-print'\n"
        "  -u|--[no-]valid[ate]-utf8    validate for UTF8 encoding the non-ascii chars\n"
        "                                 in JSON strings (when the action given is any but\n"
        "                                 `-S|--escape-json') or in whole text (when given\n"
        "                                 `-S|--escape-json'); default: do not validate\n"
        "  -c|--[no-]escape-utf8        escape UTF8 byte sequences or do not (default not)\n"
        "                                 in the strings produced by `-P|--pretty-print',\n"
        "                                 `-R|--terse-print', `Y|--type-print',\n"
        "                                 `-A|--ast-print' or `M|--from-ast-print'\n"
        "  -a|--[no-]ascii-only         enforce JSON strings to contain ASCII chars\n"
        "                                 only or otherwise do not (default not)\n"
        "  -m|--[no-]multi-object       allow multiple JSON objects on input or\n"
        "                                 otherwise do not (default not)\n"
        "  -o|--[no-]print-dots         put indenting dots in structure print outs\n"
        "                                 or otherwise do not (default not)\n"
        "  -p|--sigpipe-action NAME     specify program's behavior when receiving SIGPIPE;\n"
        "                                 NAME can be any of: 'error' or 'no-error' for\n"
        "                                 terminating the execution with or without error,\n"
        "                                 or 'ignore' for ignoring the signal altogether\n"
        "                                 (the default is 'error'); option `-p' cuts short\n"
        "                                 the long option `--sigpipe-action=no-error'\n"
        "  -q|--[no-]quote-text         print out double quote chars surrounding the\n"
        "                                 text produced by action `-S|--escape-json'\n"
        "                                 or otherwise do not (default not)\n"
        "  -r|--[no-]trim-spaces        trim out heading and trailing space chars in the\n"
        "                                 input text when action is `-S|--escape-json'\n"
        "                                 or otherwise do not (default not)\n"
        "  -w|--[no-]raw-strings        allow C++11-like raw string literals when JSON\n"
        "                                 text expected or otherwise do not (default not);\n"
        "                                 also produce raw string literals if action is\n"
        "                                 `-P|--pretty-print' or `-M|--from-ast-print';\n"
        "                                 note that in case of the other 'json-printer'\n"
        "                                 actions no such literals get produced at all\n"
        "  -y|--[no-]empty-input        allow empty input when JSON text expected or\n"
        "                                 otherwise do not (default not)\n"
        "     --[no-]newline-sep        separate the top values on terse JSON print outs\n"
        "                                 by newline or otherwise do not (default not)\n"
        "     --[no-]sort-keys          sort the key names of each JSON sub-object in the\n"
        "                                 JSON input when action is `-M|--from-ast-print'\n"
        "                                 or otherwise do not (default not)\n"
        "     --[no-]warning            print out warning reports on stderr or otherwise\n"
        "                                 do not (default do)\n"
        "     --[no-]error              print out error reports on stderr or otherwise\n"
        "                                 do not (default do); note that reporting the\n"
        "                                 fatal errors on stderr is never suppressed\n"
        "     --sizes-NAME VALUE        assign the named sizes parameter the given value;\n"
        "                                 use `--help-sizes' option to obtain the list of\n"
        "                                 all the sizes parameters along with the minimum,\n"
        "                                 the maximum and the default values and a short\n"
        "                                 description attached\n"
#ifdef DEBUG
        "     --debug-CLASS [LEVEL]     do print some debugging output for the named\n"
        "                                 class, which can be any of: base, printer,\n"
        "                                 obj, ast, type, type-lib, type-ruler or escape;\n"
        "                                 the level can be [0-9], 1 being the default\n"
        "     --no-debug                do not print debugging output at all (default)\n"
#endif
        "     --dump-options            print options and exit\n"
        "  -V|--[no-]verbose            be verbose or otherwise do not (default not)\n"
        "     --help-sizes              print info about the sizes parameters and exit\n"
        "  -v|--version                 print version numbers and exit\n"
        "  -?|--help                    display this help info and exit\n",
        program);
}

static void usage_sizes(void)
{
    static const struct su_size_param_t params[] = {
#define BUF   su_size_param_su
#define STACK su_size_param_su
#define POOL  su_size_param_su
#define WIDTH su_size_param_plain

#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        { type, name, desc, min, max, def },
#include "opt-sizes.def"

#undef WIDTH
#undef POOL
#undef STACK
#undef BUF
    };

    su_size_print_sizes(params, ARRAY_SIZE(params), stdout);
}

static char* dump_filters(
    const struct options_filter_t* filters,
    size_t n_filters, const char* name)
{
    const size_t m = TYPE_DIGITS10(size_t) + 1;
    const struct options_filter_t* p;
    size_t i, n = 0, l, s = 2;
    const size_t w = 31;
    char *b = NULL, *d;
    FILE* f;

    l = strlen(name);

    f = open_memstream(&b, &n);
    VERIFY(f != NULL);

    if (n_filters == 0) {
        fprintf(f, "%s:%*s-\n", name, w - 1  > l
            ? SIZE_AS_INT((w - 1) - l) : 0, "");

        goto done;
    }

    SIZE_ADD_EQ(s, l);
    SIZE_ADD_EQ(s, m);
    d = alloca(s);

    for (i = 0,
         p = filters;
         i < n_filters;
         i ++,
         p ++) {
        int c;

        c = snprintf(d, s, "%s[%zu]", name, i);
        n = VERIFY_INT_AS_SIZE(c);
        ASSERT(n < s);

        pretty_print_strings(
            f, PTR_PTR_CAST(p->argv, char),
            p->argc, d, w, 0);
    }

done:
    fclose(f);
    return b;
}

static void dump_options(const struct options_t* opts)
{
    static const char* const objects[] = {
        [OBJ_JSON_PARSER]    = "json-parser",
        [OBJ_JSON_PRINTER]   = "json-printer",
        [OBJ_JSON_TYPELIB]   = "json-typelib",
        [OBJ_JSON_FILTERLIB] = "json-filterlib",
        [OBJ_JSON_ESCAPE]    = "json-escape",
        [OBJ_JSON2]          = "json2"
//!!!2JSON		[OBJ_2JSON]        = "2json"
    };
#undef  CASE
#define CASE(n) [options_ ## n ## _parse_type] = #n "-parse"
    static const char* const parse_types[] = {
        CASE(echo),
        CASE(quiet),
    };
#undef  CASE
#define CASE2(n, s) [options_ ## n ## _print_type] = s "-print"
#define CASE(n)     CASE2(n, #n)
    static const char* const print_types[] = {
        CASE2(from_ast, "from-ast"),
        CASE(pretty),
        CASE(terse),
        CASE(type),
        CASE(ast)
    };
#undef  CASE
#undef  CASE2
#define CASE2(n, s) [options_ ## n ## _typelib_func] = s
#define CASE(n)     CASE2(n, #n)
    static const char* const typelib_funcs[] = {
        CASE(validate),
        CASE(print),
        CASE(attr),
        CASE(check),
        CASE2(print_check, "print-check"),
        CASE(def),
    };
#undef  CASE
#define CASE(n) [options_type_check_ ## n ## _type] = #n
    static const char* const type_checks[] = {
        CASE(none),
        CASE(def),
        CASE(lib)
    };
#undef  CASE
#undef  CASE2
#define CASE2(n, s) [options_sigpipe_act_ ## n] = s
#define CASE(n)     CASE2(n, #n)
    static const char* const sigpipe_acts[] = {
        CASE(error),
        CASE2(no_error, "no-error"),
        CASE(ignore)
    };
    static const char* const noyes[] = {
        [0] = "no",
        [1] = "yes"
    };
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
    SU_SIZE(id, type)
#include "opt-sizes.def"
    char* b;
    bool f;

    f = opts->filter_libs ||
        opts->object == OBJ_JSON_FILTERLIB;
    b = dump_filters(
            f ? opts->filters : NULL,
            f ? opts->n_filters : 0,
            "filter-libs");

    fprintf(stdout,
        "input:                         %s\n"
        "object:                        %s\n"
        "parse-type:                    %s\n"
        "print-type:                    %s\n"
        "typelib-func:                  %s\n"
        "type-check:                    %s\n"
        "type-def:                      %s\n"
        "type-lib:                      %s\n"
        "type-name:                     %s\n%s"
        "sigpipe-act:                   %s\n"
        "empty-input:                   %s\n"
        "raw-strings:                   %s\n"
        "liter-value:                   %s\n"
        "surrogate-pairs:               %s\n"
        "valid-utf8:                    %s\n"
        "escape-utf8:                   %s\n"
        "ascii-only:                    %s\n"
        "multi-obj:                     %s\n"
        "print-dots:                    %s\n"
        "quote-text:                    %s\n"
        "trim-spaces:                   %s\n"
        "newline-sep:                   %s\n"
        "sort-keys:                     %s\n"
        "no-warning:                    %s\n"
        "no-error:                      %s\n"
        "verbose:                       %s\n"
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        "sizes-" name ":" pad " " FMT_SIZE(type) "\n"
#include "opt-sizes.def"
#ifdef DEBUG
        "debug-base:                    %d\n"
        "debug-printer:                 %d\n"
        "debug-obj:                     %d\n"
        "debug-ast:                     %d\n"
        "debug-type:                    %d\n"
        "debug-type-lib:                %d\n"
        "debug-type-ruler:              %d\n"
        "debug-escape:                  %d\n"
#endif
        "argc:                          %zu\n",
        opts->input ? opts->input : stdin_name,
#define NAME0(X, T)                  \
    ({                               \
        size_t __v = opts->X;        \
        ARRAY_NON_NULL_ELEM(T, __v); \
    })
#define NAME(X)  (NAME0(X, X ## s))
#define NNUL(X)  (opts->X ? opts->X : "-")
#define NOYES(X) (NAME0(X, noyes))
        NAME(object),
        NAME(parse_type),
        NAME(print_type),
        NAME(typelib_func),
        NAME(type_check),
        NNUL(type_def),
        NNUL(type_lib),
        NNUL(type_name),
        b != NULL ? b : "",
        NAME(sigpipe_act),
        NOYES(empty_input),
        NOYES(raw_strings),
        NOYES(liter_value),
        NOYES(surrogate_pairs),
        NOYES(valid_utf8),
        NOYES(escape_utf8),
        NOYES(ascii_only),
        NOYES(multi_obj),
        NOYES(print_dots),
        NOYES(quote_text),
        NOYES(trim_spaces),
        NOYES(newline_sep),
        NOYES(sort_keys),
        NOYES(no_warning),
        NOYES(no_error),
        NOYES(verbose),
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        ARG_SIZE(id, type)
#include "opt-sizes.def"
#ifdef DEBUG
        opts->debug_base,
        opts->debug_printer,
        opts->debug_obj,
        opts->debug_ast,
        opts->debug_type,
        opts->debug_type_lib,
        opts->debug_type_ruler,
        opts->debug_escape,
#endif
#undef  NOYES
#undef  NNUL
#undef  NAME
#undef  NAME0
        opts->argc);

    free(b);

    pretty_print_strings(stdout,
        PTR_CONST_PTR_CAST(opts->argv, char),
        opts->argc, "argv", 31, 0);
}

static void invalid_opt_arg(const char* opt_name, const char* opt_arg)
{
    error("invalid argument for '%s' option: '%s'", opt_name, opt_arg);
}

static void illegal_opt_arg(const char* opt_name, const char* opt_arg)
{
    error("illegal argument for '%s' option: '%s'", opt_name, opt_arg);
}

static void missing_opt_arg_str(const char* opt_name)
{
    error("argument for option '%s' not found", opt_name);
}

static void missing_opt_arg_ch(char opt_name)
{
    error("argument for option '-%c' not found", opt_name);
}

static void not_allowed_opt_arg(const char* opt_name)
{
    error("option '%s' does not allow an argument", opt_name);
}

static void invalid_opt_str(const char* opt_name)
{
    error("invalid command line option '%s'", opt_name);
}

static void invalid_opt_ch(char opt_name)
{
    error("invalid command line option '-%c'", opt_name);
}

static const char* parse_typelib_optarg(
    const char* opt_name, char* opt_arg,
    const char** type_name)
{
    char* p = opt_arg;
    if ((*p == '\0') || (*p ++ == '-' && *p == '\0'))
        illegal_opt_arg(opt_name, opt_arg);

    if ((p = strrchr(opt_arg, ':'))) {
        if (p[1] == '\0')
            illegal_opt_arg(opt_name, opt_arg);
        *p ++ = 0;
        *type_name = p;
    }

    return opt_arg;
}

static bool find_filter_sep(
    const struct options_filter_t* base,
    size_t* pos)
{
    char *const *p, *const* e;

    for (p = base->argv,
         e = p + base->argc;
         p < e;
         p ++) {
        if (!strcmp(*p, "--")) {
            *pos = PTR_DIFF(p, base->argv);
            return true;
        }
    }
    return false;
}

static bool update_filter_opt(
    struct options_filter_t* base,
    struct options_filter_t* prev)
{
    size_t k;

    if (!find_filter_sep(base, &k))
        return false;

    ASSERT(k < base->argc);

    prev->argv = base->argv;
    prev->argv[k] = NULL;
    prev->argc = k ++;

    base->argc -= k;
    base->argv += k;

    return true;
}


static void parse_filters(
    struct options_filter_t base,
    struct options_filter_t* libs,
    size_t* n_libs, size_t max)
{
    size_t n = 1;

    ASSERT(max > 0);

    while (true) {
        if (base.argc == 0)
        empty_args:
            error("empty filter arguments "
                "group #%zu", n);
        if (n > max)
            error("too many filter arguments "
                "groups (max is %zu)", max);

        if (!update_filter_opt(&base, libs)) {
            *libs = base;
            break;
        }
        if (libs->argc == 0)
            goto empty_args;

        libs ++;
        n ++;
    }
    *n_libs = n;
}

// $ . ~/lookup-gen/commands.sh
// $ print() { printf '%s\n' "$@"; }
// $ print 'v validate =options_validate_typelib_func' 'p print =options_print_typelib_func' 'a attr =options_attr_typelib_func' 'c check check-attr =options_check_typelib_func' 'A print-check print-check-attr =options_print_check_typelib_func' 'd gen-def =options_def_typelib_func'|gen-func -f lookup_typelib_func_name|ssed -R '1s/^/static /;1s/\s*result_t\&/\n\tenum options_typelib_func_t*/;s/(?=t =)/*/;s/result_t:://'

static bool lookup_typelib_func_name(const char* n,
    enum options_typelib_func_t* t)
{
    // pattern: A|a[ttr]|c[heck[-attr]]|d|gen-def|p[rint[-check[-attr]]]|v[alidate]
    switch (*n ++) {
    case 'A':
        if (*n == 0) {
            *t = options_print_check_typelib_func;
            return true;
        }
        return false;
    case 'a':
        if (*n == 0) {
            *t = options_attr_typelib_func;
            return true;
        }
        if (*n ++ == 't' &&
            *n ++ == 't' &&
            *n ++ == 'r' &&
            *n == 0) {
            *t = options_attr_typelib_func;
            return true;
        }
        return false;
    case 'c':
        if (*n == 0) {
            *t = options_check_typelib_func;
            return true;
        }
        if (*n ++ == 'h' &&
            *n ++ == 'e' &&
            *n ++ == 'c' &&
            *n ++ == 'k') {
            if (*n == 0) {
                *t = options_check_typelib_func;
                return true;
            }
            if (*n ++ == '-' &&
                *n ++ == 'a' &&
                *n ++ == 't' &&
                *n ++ == 't' &&
                *n ++ == 'r' &&
                *n == 0) {
                *t = options_check_typelib_func;
                return true;
            }
        }
        return false;
    case 'd':
        if (*n == 0) {
            *t = options_def_typelib_func;
            return true;
        }
        return false;
    case 'g':
        if (*n ++ == 'e' &&
            *n ++ == 'n' &&
            *n ++ == '-' &&
            *n ++ == 'd' &&
            *n ++ == 'e' &&
            *n ++ == 'f' &&
            *n == 0) {
            *t = options_def_typelib_func;
            return true;
        }
        return false;
    case 'p':
        if (*n == 0) {
            *t = options_print_typelib_func;
            return true;
        }
        if (*n ++ == 'r' &&
            *n ++ == 'i' &&
            *n ++ == 'n' &&
            *n ++ == 't') {
            if (*n == 0) {
                *t = options_print_typelib_func;
                return true;
            }
            if (*n ++ == '-' &&
                *n ++ == 'c' &&
                *n ++ == 'h' &&
                *n ++ == 'e' &&
                *n ++ == 'c' &&
                *n ++ == 'k') {
                if (*n == 0) {
                    *t = options_print_check_typelib_func;
                    return true;
                }
                if (*n ++ == '-' &&
                    *n ++ == 'a' &&
                    *n ++ == 't' &&
                    *n ++ == 't' &&
                    *n ++ == 'r' &&
                    *n == 0) {
                    *t = options_print_check_typelib_func;
                    return true;
                }
            }
        }
        return false;
    case 'v':
        if (*n == 0) {
            *t = options_validate_typelib_func;
            return true;
        }
        if (*n ++ == 'a' &&
            *n ++ == 'l' &&
            *n ++ == 'i' &&
            *n ++ == 'd' &&
            *n ++ == 'a' &&
            *n ++ == 't' &&
            *n ++ == 'e' &&
            *n == 0) {
            *t = options_validate_typelib_func;
            return true;
        }
    }
    return false;
}

static enum options_typelib_func_t parse_typelib_func_optarg(
    const char* opt_name, const char* opt_arg)
{
    enum options_typelib_func_t r;

    if (opt_arg == NULL)
        return options_validate_typelib_func;
    if (!lookup_typelib_func_name(opt_arg, &r))
        invalid_opt_arg(opt_name, opt_arg);

    return r;
}

// $ print 'error =options_sigpipe_act_error' 'no-error =options_sigpipe_act_no_error' 'ignore =options_sigpipe_act_ignore'|gen-func -f lookup_sigpipe_act_name|ssed -R '1s/^/static /;1s/\s*result_t\&/\n\tenum options_sigpipe_act_t*/;s/(?=t =)/*/;s/result_t:://'

static bool lookup_sigpipe_act_name(const char* n,
    enum options_sigpipe_act_t* t)
{
    // pattern: error|ignore|no-error
    switch (*n ++) {
    case 'e':
        if (*n ++ == 'r' &&
            *n ++ == 'r' &&
            *n ++ == 'o' &&
            *n ++ == 'r' &&
            *n == 0) {
            *t = options_sigpipe_act_error;
            return true;
        }
        return false;
    case 'i':
        if (*n ++ == 'g' &&
            *n ++ == 'n' &&
            *n ++ == 'o' &&
            *n ++ == 'r' &&
            *n ++ == 'e' &&
            *n == 0) {
            *t = options_sigpipe_act_ignore;
            return true;
        }
        return false;
    case 'n':
        if (*n ++ == 'o' &&
            *n ++ == '-' &&
            *n ++ == 'e' &&
            *n ++ == 'r' &&
            *n ++ == 'r' &&
            *n ++ == 'o' &&
            *n ++ == 'r' &&
            *n == 0) {
            *t = options_sigpipe_act_no_error;
            return true;
        }
    }
    return false;
}

static enum options_sigpipe_act_t parse_sigpipe_act_optarg(
    const char* opt_name, const char* opt_arg)
{
    enum options_sigpipe_act_t r;

    // stev: short option `-p' means 'no-error'
    if (opt_arg == NULL)
        return options_sigpipe_act_no_error;

    if (!lookup_sigpipe_act_name(opt_arg, &r))
        invalid_opt_arg(opt_name, opt_arg);

    return r;
}

#define SU_SIZE_OPT_NEED_PARSE_SIZE
#define SU_SIZE_OPT_NEED_PARSE_SIZE_SU
#define SU_SIZE_OPT_NEED_STATIC_FUNCS
#include "lib/su-size-opt-impl.h"

#ifdef DEBUG
static size_t parse_debug_optarg(
    const char* opt_name, const char* opt_arg)
{
    if (opt_arg == NULL)
        return 1;
    else {
        if (!ISDIGIT(opt_arg[0]) || opt_arg[1] != '\0')
            invalid_opt_arg(opt_name, opt_arg);
        return *opt_arg - '0';
    }
}
#endif

// stev: comment out the following define to disable
// processing of long command line options using the
// generated function 'lookup_long_opt' in header
// 'long-opts-impl.h'
#define CONFIG_GETOPT_LONG2

#ifdef CONFIG_GETOPT_LONG2
#include "long-opts-impl.h"
#include "lib/getopt2-impl.h"
#endif // CONFIG_GETOPT_LONG2

#if defined(DEBUG) && defined(DEBUG_GETOPT_LONG)

static void print_getopt_long_debug(int opt)
{
    fprintf(stderr, "optind=%d optarg=", optind);
    if (optarg)
        pretty_print_string(stderr,
            PTR_UCHAR_CAST_CONST(optarg),
            strlen(optarg), pretty_print_string_quotes);
    else
        fputs("(nil)", stderr);
    fprintf(stderr, " opt=%d opt=", opt);
    pretty_print_char(stderr, opt, pretty_print_char_quotes);
    fprintf(stderr, " optopt=%d optopt=", optopt);
    pretty_print_char(stderr, optopt, pretty_print_char_quotes);
}

#define PRINT_GETOPT_LONG_DEBUG()                  \
    do {                                           \
        PRINT_DEBUG_BEGIN_UNCOND("getopt_long: "); \
        print_getopt_long_debug(opt);              \
        PRINT_DEBUG_END_UNCOND();                  \
    } while (0)

#endif

const struct options_t* options(int argc, char* argv[])
{
    static struct options_t opts = {
        .input            = NULL,
        .object           = OBJ_JSON2,
        .parse_type       = options_quiet_parse_type,
        .print_type       = options_pretty_print_type,
        .typelib_func     = options_validate_typelib_func,
        .type_check       = options_type_check_none_type,
        .type_def         = NULL,
        .type_lib         = NULL,
        .type_name        = NULL,
        .sigpipe_act      = options_sigpipe_act_error,
        .filter_libs      = false,
        .empty_input      = false,
        .raw_strings      = false,
        .liter_value      = false,
        .surrogate_pairs  = false,
        .valid_utf8       = false,
        .escape_utf8      = false,
        .ascii_only       = false,
        .multi_obj        = false,
        .print_dots       = false,
        .quote_text       = false,
        .trim_spaces      = false,
        .newline_sep      = false,
        .sort_keys        = false,
        .no_warning       = false,
        .no_error         = false,
        .verbose          = false,
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        .sizes_ ## id = def,
#include "opt-sizes.def"
#ifdef DEBUG
        .debug_base       = 0,
        .debug_printer    = 0,
        .debug_obj        = 0,
        .debug_ast        = 0,
        .debug_type       = 0,
        .debug_type_lib   = 0,
        .debug_type_ruler = 0,
        .debug_escape     = 0,
#endif
        .argc             = 0,
        .argv             = NULL
    };

    enum {
        // stev: actions:
        quiet_parser_act     = 'O',
        echo_parser_act      = 'E',
        from_ast_printer_act = 'M',
        pretty_printer_act   = 'P',
        terse_printer_act    = 'R',
        type_printer_act     = 'Y',
        ast_printer_act      = 'A',
        escape_json_act      = 'S',
        json2_act            = 'J',
        //!!!2JON two_json_act = '2',
        typelib_func_act     = 'T',
        filterlib_exec_act   = 'F',

        // stev: options:
        type_def_opt         = 'd',
        type_lib_opt         = 't',
        type_name_opt        = 'n',
        input_file_opt       = 'i',
        sigpipe_act_opt      = 'p',
        filter_libs_opt      = 'f',
        empty_input_opt      = 'y',
        raw_strings_opt      = 'w',
        liter_value_opt      = 'l',
        surrogate_pairs_opt  = 's',
        valid_utf8_opt       = 'u',
        escape_utf8_opt      = 'c',
        ascii_only_opt       = 'a',
        multi_obj_opt        = 'm',
        print_dots_opt       = 'o',
        quote_text_opt       = 'q',
        trim_spaces_opt      = 'r',
        version_opt          = 'v',
        verbose_opt          = 'V',
        help_opt             = '?',

#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        OPT_LETTER(id, letter)
#include "opt-sizes.def"

        newline_sep_opt      = 128,
        sort_keys_opt,
        no_warning_opt,
        no_error_opt,
        dump_opts_opt,

#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        OPT_NON_LETTER(id, letter)
#include "opt-sizes.def"

        no_type_check_opt,
        no_filter_libs_opt,
        no_empty_input_opt,
        no_raw_strings_opt,
        no_liter_value_opt,
        no_surrogate_pairs_opt,
        no_valid_utf8_opt,
        no_escape_utf8_opt,
        no_ascii_only_opt,
        no_multi_obj_opt,
        no_print_dots_opt,
        no_quote_text_opt,
        no_trim_spaces_opt,
        no_newline_sep_opt,
        no_sort_keys_opt,
        no_no_warning_opt,
        no_no_error_opt,
        no_verbose_opt,

#ifdef DEBUG
        no_debug_opt,

        debug_base_opt,
        debug_printer_opt,
        debug_obj_opt,
        debug_ast_opt,
        debug_type_opt,
        debug_type_lib_opt,
        debug_type_ruler_opt,
        debug_escape_opt,
#endif
        help_sizes_opt,
    };

// stev: note that function 'lookup_long_opt' in header
// 'long-opts-impl.h' needs to be regenerated each and
// every time the variable 'long_opts' below changes,
// in the case CONFIG_GETOPT_LONG2 has been defined!

    static struct option long_opts[] = {
        { "parse-only",             0,       0, quiet_parser_act },
        { "echo-parse",             0,       0, echo_parser_act },
        { "from-ast-print",         0,       0, from_ast_printer_act },
        { "pretty-print",           0,       0, pretty_printer_act },
        { "terse-print",            0,       0, terse_printer_act },
        { "type-print",             0,       0, type_printer_act },
        { "ast-print",              0,       0, ast_printer_act },
        { "typelib-func",           2,       0, typelib_func_act },
        { "filterlib-exec",         0,       0, filterlib_exec_act },
        { "escape-json",            0,       0, escape_json_act },
        { "json2",                  0,       0, json2_act },
//!!!2JSON		{ "2json",                  0,       0, two_json_act },

        { "type-def",               1,       0, type_def_opt },
        { "type-lib",               1,       0, type_lib_opt },
        { "type-name",              2,       0, type_name_opt },
        { "no-type-check",          0,       0, no_type_check_opt },
        { "filter-libs",            0,       0, filter_libs_opt },
        { "no-filter-libs",         0,       0, no_filter_libs_opt },
        { "input-file",             1,       0, input_file_opt },
        { "sigpipe-action",         1,       0, sigpipe_act_opt },
        { "empty-input",            0,       0, empty_input_opt },
        { "no-empty-input",         0,       0, no_empty_input_opt },
        { "raw-strings",            0,       0, raw_strings_opt },
        { "no-raw-strings",         0,       0, no_raw_strings_opt },
        { "liter-value",            0,       0, liter_value_opt },
        { "no-liter-value",         0,       0, no_liter_value_opt },
        { "literal-value",          0,       0, liter_value_opt },
        { "no-literal-value",       0,       0, no_liter_value_opt },
        { "valid-utf8",             0,       0, valid_utf8_opt },
        { "no-valid-utf8",          0,       0, no_valid_utf8_opt },
        { "validate-utf8",          0,       0, valid_utf8_opt },
        { "no-validate-utf8",       0,       0, no_valid_utf8_opt },
        { "escape-utf8",            0,       0, escape_utf8_opt },
        { "no-escape-utf8",         0,       0, no_escape_utf8_opt },
        { "surrogate-pairs",        0,       0, surrogate_pairs_opt },
        { "no-surrogate-pairs",     0,       0, no_surrogate_pairs_opt },
        { "ascii-only",             0,       0, ascii_only_opt },
        { "no-ascii-only",          0,       0, no_ascii_only_opt },
        { "multi-object",           0,       0, multi_obj_opt },
        { "no-multi-object",        0,       0, no_multi_obj_opt },
        { "print-dots",             0,       0, print_dots_opt },
        { "no-print-dots",          0,       0, no_print_dots_opt },
        { "dots",                   0,       0, print_dots_opt },
        { "no-dots",                0,       0, no_print_dots_opt },
        { "quote-text",             0,       0, quote_text_opt },
        { "no-quote-text",          0,       0, no_quote_text_opt },
        { "trim-spaces",            0,       0, trim_spaces_opt },
        { "no-trim-spaces",         0,       0, no_trim_spaces_opt },
        { "newline-sep",            0,       0, newline_sep_opt },
        { "no-newline-sep",         0,       0, no_newline_sep_opt },
        { "sort-keys",              0,       0, sort_keys_opt },
        { "no-sort-keys",           0,       0, no_sort_keys_opt },
        { "dump-options",           0,       0, dump_opts_opt },
        { "version",                0,       0, version_opt },
        { "warning",                0,       0, no_no_warning_opt },
        { "no-warning",             0,       0, no_warning_opt },
        { "error",                  0,       0, no_no_error_opt },
        { "no-error",               0,       0, no_error_opt },
        { "verbose",                0,       0, verbose_opt },
        { "no-verbose",             0,       0, no_verbose_opt },
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def)    \
        { "sizes-" name,            1,       0, sizes_ ## id ## _opt }, \
        STRUCT_LETTER(name, id, letter)
#include "opt-sizes.def"
#ifdef DEBUG
        { "debug-base",             2,       0, debug_base_opt },
        { "debug-printer",          2,       0, debug_printer_opt },
        { "debug-obj",              2,       0, debug_obj_opt },
        { "debug-ast",              2,       0, debug_ast_opt },
        { "debug-type",             2,       0, debug_type_opt },
        { "debug-type-lib",         2,       0, debug_type_lib_opt },
        { "debug-type-ruler",       2,       0, debug_type_ruler_opt },
        { "debug-escape",           2,       0, debug_escape_opt },
        { "no-debug",               0,       0, no_debug_opt },
#endif
        { "help-sizes",             0,       0, help_sizes_opt },
        { "help",                   0, &optopt, help_opt },
        { 0,                        0,       0, 0 }
    };
    static const char short_opts[] =
        ":"
        "AEFJMOPRST::Y"
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        ARG_LETTER(letter)
#include "opt-sizes.def"
        "acd:fi:lmn::opqrst:uvVwy";

    struct bits_opts_t {
        bits_t dump: 1;
        bits_t usage: 1;
        bits_t usage_sizes: 1;
        bits_t version: 1;
        bits_t has_filters: 1;
    };
    struct bits_opts_t bits = {
        .dump        = false,
        .usage       = false,
        .usage_sizes = false,
        .version     = false,
        .has_filters = false,
    };

    struct options_filter_t fopt = {
        .argc = argc,
        .argv = argv
    };
    struct options_filter_t popt;
    int opt;

#define argv_optind()                      \
    ({                                     \
        size_t i = INT_AS_SIZE(optind);    \
        ASSERT_SIZE_DEC_NO_OVERFLOW(i);    \
        ASSERT(i - 1 < INT_AS_SIZE(argc)); \
        argv[i - 1];                       \
    })

#define optopt_char()                   \
    ({                                  \
        ASSERT(ISASCII((char) optopt)); \
        (char) optopt;                  \
    })

    if (update_filter_opt(&fopt, &popt)) {
        bits.has_filters = true;
        ASSERT(popt.argv == argv);
        argc = popt.argc;
    }

    opterr = 0;
    optind = 1;
    while ((opt = getopt_long(argc, argv, short_opts,
        long_opts, 0)) != EOF) {

#if defined(DEBUG) && defined(DEBUG_GETOPT_LONG)
        PRINT_GETOPT_LONG_DEBUG();
#endif

        switch (opt) {
        case no_type_check_opt:
            opts.type_check =
                options_type_check_none_type;
            break;
        case type_def_opt:
            opts.type_check =
                options_type_check_def_type;
            opts.type_def = optarg;
            break;
        case type_lib_opt:
            opts.type_check =
                options_type_check_lib_type;
            opts.type_lib =
                parse_typelib_optarg("type-lib", optarg,
                    &opts.type_name);
            break;
        case type_name_opt:
            opts.type_name =
                optarg && *optarg ? optarg : NULL;
            break;
        case filter_libs_opt:
            opts.filter_libs = true;
            break;
        case no_filter_libs_opt:
            opts.filter_libs = false;
            break;
        case sigpipe_act_opt:
            opts.sigpipe_act = parse_sigpipe_act_optarg(
                "sigpipe-action", optarg);
            break;
        case empty_input_opt:
            opts.empty_input = true;
            break;
        case no_empty_input_opt:
            opts.empty_input = false;
            break;
        case raw_strings_opt:
            opts.raw_strings = true;
            break;
        case no_raw_strings_opt:
            opts.raw_strings = false;
            break;
        case liter_value_opt:
            opts.liter_value = true;
            break;
        case no_liter_value_opt:
            opts.liter_value = false;
            break;
        case surrogate_pairs_opt:
            opts.surrogate_pairs = true;
            break;
        case no_surrogate_pairs_opt:
            opts.surrogate_pairs = false;
            break;
        case valid_utf8_opt:
            opts.valid_utf8 = true;
            break;
        case no_valid_utf8_opt:
            opts.valid_utf8 = false;
            break;
        case escape_utf8_opt:
            opts.escape_utf8 = true;
            break;
        case no_escape_utf8_opt:
            opts.escape_utf8 = false;
            break;
        case ascii_only_opt:
            opts.ascii_only = true;
            break;
        case no_ascii_only_opt:
            opts.ascii_only = false;
            break;
        case multi_obj_opt:
            opts.multi_obj = true;
            break;
        case no_multi_obj_opt:
            opts.multi_obj = false;
            break;
        case print_dots_opt:
            opts.print_dots = true;
            break;
        case no_print_dots_opt:
            opts.print_dots = false;
            break;
        case quote_text_opt:
            opts.quote_text = true;
            break;
        case no_quote_text_opt:
            opts.quote_text = false;
            break;
        case trim_spaces_opt:
            opts.trim_spaces = true;
            break;
        case no_trim_spaces_opt:
            opts.trim_spaces = false;
            break;
        case newline_sep_opt:
            opts.newline_sep = true;
            break;
        case no_newline_sep_opt:
            opts.newline_sep = false;
            break;
        case sort_keys_opt:
            opts.sort_keys = true;
            break;
        case no_sort_keys_opt:
            opts.sort_keys = false;
            break;
        case dump_opts_opt:
            bits.dump = true;
            break;
#undef  CASE
#define CASE(type, id, name, pad, letter, desc, help, min, max, def) \
        case sizes_ ## id ## _opt: opts.sizes_ ## id =               \
            PARSE_SIZE_OPTARG(name, type, min, max);                 \
            break; 
#include "opt-sizes.def"

#ifdef DEBUG
#define CASE_DEBUG__(t, n)                             \
        case debug_ ## t ## _opt: {                    \
                size_t __n;                            \
                __n = parse_debug_optarg(n, optarg);   \
                ASSERT(SIZE_IS_BITS(__n, debug_bits)); \
                opts.debug_ ## t = __n;                \
            break;                                     \
        }
#define CASE_DEBUG_(t, n) CASE_DEBUG__(t, "debug-" n)

#define CASE_DEBUG(t) CASE_DEBUG_(t, #t)
#define CASE_DEBUG2   CASE_DEBUG_

        CASE_DEBUG(base)
        CASE_DEBUG(printer)
        CASE_DEBUG(obj)
        CASE_DEBUG(ast)
        CASE_DEBUG(type)
        CASE_DEBUG2(type_lib, "type-lib")
        CASE_DEBUG2(type_ruler, "type-ruler")
        CASE_DEBUG(escape)

        case no_debug_opt:
            opts.debug_base = 0;
            opts.debug_printer = 0;
            opts.debug_obj = 0;
            opts.debug_ast = 0;
            opts.debug_type = 0;
            opts.debug_type_lib = 0;
            opts.debug_type_ruler = 0;
            opts.debug_escape = 0;
            break;
#endif
        case input_file_opt:
            opts.input = optarg;
            break;
        case version_opt:
            bits.version = true;
            break;
        case no_warning_opt:
            opts.no_warning = true;
            break;
        case no_no_warning_opt:
            opts.no_warning = false;
            break;
        case no_error_opt:
            opts.no_error = true;
            break;
        case no_no_error_opt:
            opts.no_error = false;
            break;
        case verbose_opt:
            opts.verbose = true;
            break;
        case no_verbose_opt:
            opts.verbose = false;
            break;

#undef  CASE
#define CASE(n)                                       \
        case n ## r_act:                              \
            opts.object = OBJ_JSON_PARSER;            \
            opts.parse_type = options_ ## n ## _type; \
            break;

        CASE(quiet_parse)
        CASE(echo_parse)

        case json2_act:
            opts.object = OBJ_JSON2;
            break;
        //!!!2JSONcase two_json_act:
        //!!!2JSON	opts.object = OBJ_2JSON;
        //!!!2JSON	break;
        case escape_json_act:
            opts.object = OBJ_JSON_ESCAPE;
            break;

#undef  CASE
#define CASE(n)                                       \
        case n ## er_act:                             \
            opts.object = OBJ_JSON_PRINTER;           \
            opts.print_type = options_ ## n ## _type; \
            break;

        CASE(from_ast_print)
        CASE(pretty_print)
        CASE(terse_print)
        CASE(type_print)
        CASE(ast_print)

        case typelib_func_act:
            opts.object = OBJ_JSON_TYPELIB;
            opts.typelib_func = parse_typelib_func_optarg(
                "typelib-func", optarg);
            break;
        case filterlib_exec_act:
            opts.object = OBJ_JSON_FILTERLIB;
            break;
        case help_sizes_opt:
            bits.usage_sizes = true;
            break;
        case 0:
            bits.usage = true;
            break;
        case ':': {
            const char* opt = argv_optind();
            if (opt[0] == '-' && opt[1] == '-')
                missing_opt_arg_str(opt);
            else
                missing_opt_arg_ch(optopt_char());
            break;
        }
        case '?':
        default:
            if (optopt == 0)
                invalid_opt_str(argv_optind());
            else {
                char* opt = argv_optind();
                if (opt[0] != '-' || opt[1] != '-')
                    invalid_opt_ch(optopt_char());
                else {
                    char* end = strchr(opt, '=');
                    if (end) *end = '\0';
                    if (end || optopt != '?')
                        not_allowed_opt_arg(opt);
                    else
                        bits.usage = true;
                }
            }
            break;
        }
    }

    ASSERT(optind > 0);
    ASSERT(optind <= argc);

    argc -= optind;
    argv += optind;

    if (argc > 0) {
        opts.input = *argv ++;
        argc --;
    }

    opts.argc = INT_AS_SIZE(argc);
    opts.argv = argv;

    if (bits.has_filters &&
        (opts.filter_libs ||
         opts.object == OBJ_JSON_FILTERLIB))
        parse_filters(fopt, opts.filters, &opts.n_filters,
            ARRAY_SIZE(opts.filters));

    if (opts.input && !strcmp(opts.input, "-")) {
        if (opts.object == OBJ_JSON_FILTERLIB)
            error("input filter library cannot be stdin");
        opts.input = NULL;
    }

    if (bits.version)
        version();
    if (bits.dump)
        dump_options(&opts);
    if (bits.usage_sizes)
        usage_sizes();
    if (bits.usage)
        usage();

    if (bits.dump ||
        bits.version ||
        bits.usage_sizes ||
        bits.usage)
        exit(0);

    ASSERT_SIZE_DEC_NO_OVERFLOW(
        opts.sizes_error_buf_max);
    ASSERT_SIZE_MUL_NO_OVERFLOW(
        opts.sizes_error_ctxt_size, SZ(2));
    if (2 * opts.sizes_error_ctxt_size >
            opts.sizes_error_buf_max - 1)
        error("invalid pair of parameters "
            "'error-context-size' and 'error-buf-max': "
            "2 * %zu + 1 > %zu",
            opts.sizes_error_ctxt_size,
            opts.sizes_error_buf_max);

    if (opts.object == OBJ_JSON_TYPELIB) {
        if (!opts.no_warning && opts.multi_obj)
            warning("`-m|--multi-object' is ignored when "
                "given along with `-T|--typelib-func'");
    }

    if (opts.object == OBJ_JSON_FILTERLIB) {
        if (!opts.no_warning &&
             opts.n_filters > 1 && opts.input == NULL)
            warning("action `-F|--filterlib-exec': "
                "multiple filter command line "
                "arguments groups given");

        if (!opts.no_warning &&
             opts.n_filters > 0 && opts.input != NULL)
            warning("action `-F|--filterlib-exec': "
                "ignoring the filter command line arguments "
                "since input file was given");

        if (opts.n_filters == 0 && opts.input == NULL)
            error("action `-F|--filterlib-exec' requires "
                "at least one filter command line "
                "arguments group");

        if (opts.input != NULL) {
            static char* argv[3] = { [1] = "--help" };

            argv[0] = CONST_CAST(opts.input, char);
            opts.filters->argv = argv;
            opts.filters->argc = 2;
            opts.n_filters = 1;
        }
    }

    if (!opts.no_warning &&
         bits.has_filters &&
        !opts.filter_libs &&
         opts.object != OBJ_JSON_FILTERLIB)
        warning("ignoring the filter command line arguments "
            "since neither `-F|--filterlib-exec' nor "
            "`-f|--filter-libs' were given");

    if (!opts.no_warning &&
         bits.has_filters &&
         opts.filter_libs &&
         opts.object != OBJ_JSON_FILTERLIB &&
         opts.object != OBJ_JSON_PARSER &&
         opts.object != OBJ_JSON_PRINTER &&
         opts.object != OBJ_JSON2)
        warning("ignoring the filter command line arguments "
            "since none of JSON parser, printer or flattener "
            "options were given");

    if (opts.filter_libs &&
        opts.n_filters == 0 &&
        opts.object != OBJ_JSON_ESCAPE &&
        opts.object != OBJ_JSON_TYPELIB &&
        opts.object != OBJ_JSON_FILTERLIB)
        error("option `-f|--filter-libs' requires at least "
            "one filter command line arguments group");

    init_sigpipe_action(opts.sigpipe_act);

    lib_version_check();

    return &opts;
}


