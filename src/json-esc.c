// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "lib/json.h"

#include "obj.h"
#include "common.h"
#include "json-esc.h"

static void obj_json_escape_init(
    struct obj_json_escape_t* this, const struct options_t* opts)
{
    obj_init(OBJ_SUPER(this));

    obj_json_base_init_esc(JSON_BASE(this), opts);
}

static void obj_json_escape_done(struct obj_json_escape_t* this)
{
    obj_json_base_done(JSON_BASE(this));

    obj_done(OBJ_SUPER(this));
}

static int obj_json_escape_run(struct obj_json_escape_t* this)
{
    return obj_json_base_run(JSON_BASE(this));
}

OBJ_IMPL(
    obj_json_escape,
    obj_json_escape_t,
    obj_json_escape_init,
    obj_json_escape_done,
    obj_json_escape_run
);


