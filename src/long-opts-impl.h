// Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
// 
// This file is part of Json-Type.
// 
// Json-Type is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Json-Type is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

static inline bool prefix(
    const char* p, const char* q)
{
    while (*p && *p == *q)
         ++ p, ++ q;
    return *p == 0;
}

// $ . ~/lookup-gen/commands.sh

// # long-opts [--debug] [-n]
// $ long-opts() { local d=''; [ "$1" == "--debug" ] && { d='-DDEBUG'; shift; }; ${GCC:-gcc} -Wall -Wextra -std=${GCC_STD:-gnu99} -g -I.. -I. -DPROGRAM=json -DLIBRARY=json.so -DCONFIG_GEN_LOOKUP_LONG_OPT $d -E common.c -o -|ssed -nR '/\slong_opts\s*\[\]\s*=\s*\{\s*$/,/^\s+\}\s*;\s*$/{:0;/^\s*(\{\s*".*?"\s*,\s*\d+\s*,\s*(?:\d+|\&\s*[a-z0-9_]+)\s*,\s*[a-z0-9_]+\s*\}\s*,)/!b;s//\1\n/;s/" "//;P;s/^.*?\n//;b0}'|if [ "$1" == '-n' ]; then ssed -nR 's/^.*?"(.*?)".*?$/\1/p'; else cat; fi; }

// $ long-opts|less
// $ long-opts -n|less
// $ long-opts --debug|less
// $ long-opts --debug -n|less

// # gen-long-opts-func0 [--debug]
// $ gen-long-opts-func0() { local d=''; [ "$1" == "--debug" ] && d="$1"; long-opts $d -n|gen-func -r- -f lookup_long_opt -e-1 -Pf -uz|ssed -R '1s/^/static /'; }

// $ gen-long-opts-func() { diff -DDEBUG <(gen-long-opts-func0) <(gen-long-opts-func0 --debug)|ssed -R 's/(?<=^#else|^#endif)\s+\/\*\s*DEBUG\s*\*\/\s*$//'; }

// $ gen-long-opts-func 

static int lookup_long_opt(const char* p)
{
    switch (*p ++) {
    case 'a':
        if (*p ++ == 's') {
            switch (*p ++) {
            case 'c':
                if (prefix(p, "ii-only"))
                    return 35;
                return -1;
            case 't':
                if (prefix(p, "-print"))
                    return 6;
            }
        }
        return -1;
    case 'd':
        switch (*p ++) {
#ifdef DEBUG
        case 'e':
            if (prefix("bug-", p)) {
                p += 4;
                switch (*p ++) {
                case 'a':
                    if (prefix(p, "st"))
                        return 103;
                    return -1;
                case 'b':
                    if (prefix(p, "ase"))
                        return 100;
                    return -1;
                case 'e':
                    if (prefix(p, "scape"))
                        return 107;
                    return -1;
                case 'o':
                    if (prefix(p, "bj"))
                        return 102;
                    return -1;
                case 'p':
                    if (prefix(p, "rinter"))
                        return 101;
                    return -1;
                case 't':
                    if (prefix("ype", p)) {
                        p += 3;
                        if (*p == 0)
                            return 104;
                        if (*p ++ == '-') {
                            switch (*p ++) {
                            case 'l':
                                if (prefix(p, "ib"))
                                    return 105;
                                return -1;
                            case 'r':
                                if (prefix(p, "uler"))
                                    return 106;
                            }
                        }
                    }
                }
            }
            return -1;
#endif
        case 'o':
            if (prefix(p, "ts"))
                return 41;
            return -1;
        case 'u':
            if (prefix(p, "mp-options"))
                return 51;
        }
        return -1;
    case 'e':
        switch (*p ++) {
        case 'c':
            if (prefix(p, "ho-parse"))
                return 1;
            return -1;
        case 'm':
            if (prefix(p, "pty-input"))
                return 19;
            return -1;
        case 'r':
            if (prefix("ror", p)) {
                p += 3;
                if (*p == 0)
                    return 55;
                if (prefix(p, "-context-size"))
                    return 66;
            }
            return -1;
        case 's':
            if (prefix("cape-", p)) {
                p += 5;
                switch (*p ++) {
                case 'j':
                    if (prefix(p, "son"))
                        return 9;
                    return -1;
                case 'u':
                    if (prefix(p, "tf8"))
                        return 31;
                }
            }
        }
        return -1;
    case 'f':
        switch (*p ++) {
        case 'i':
            if (prefix("lter", p)) {
                p += 4;
                switch (*p ++) {
                case '-':
                    if (prefix(p, "libs"))
                        return 15;
                    return -1;
                case 'l':
                    if (prefix(p, "ib-exec"))
                        return 8;
                }
            }
            return -1;
        case 'r':
            if (prefix(p, "om-ast-print"))
                return 2;
        }
        return -1;
    case 'h':
        if (prefix("elp", p)) {
            p += 3;
            if (*p == 0)
#ifndef DEBUG
                return 101;
#else
                return 110;
#endif
            if (prefix(p, "-sizes"))
#ifndef DEBUG
                return 100;
#else
                return 109;
#endif
        }
        return -1;
    case 'i':
        if (prefix("nput-", p)) {
            p += 5;
            switch (*p ++) {
            case 'b':
                if (prefix(p, "uf-size"))
                    return 62;
                return -1;
            case 'f':
                if (prefix(p, "ile"))
                    return 17;
            }
        }
        return -1;
    case 'j':
        if (prefix(p, "son2"))
            return 10;
        return -1;
    case 'l':
        if (prefix("iter", p)) {
            p += 4;
            switch (*p ++) {
            case '-':
                if (prefix(p, "value"))
                    return 23;
                return -1;
            case 'a':
                if (prefix(p, "l-value"))
                    return 25;
            }
        }
        return -1;
    case 'm':
        if (prefix(p, "ulti-object"))
            return 37;
        return -1;
    case 'n':
        switch (*p ++) {
        case 'e':
            if (prefix(p, "wline-sep"))
                return 47;
            return -1;
        case 'o':
            if (*p ++ == '-') {
                switch (*p ++) {
                case 'a':
                    if (prefix(p, "scii-only"))
                        return 36;
                    return -1;
                case 'd':
#ifndef DEBUG
                    if (prefix(p, "ots"))
                        return 42;
#else
                    switch (*p ++) {
                    case 'e':
                        if (prefix(p, "bug"))
                            return 108;
                        return -1;
                    case 'o':
                        if (prefix(p, "ts"))
                            return 42;
                    }
#endif
                    return -1;
                case 'e':
                    switch (*p ++) {
                    case 'm':
                        if (prefix(p, "pty-input"))
                            return 20;
                        return -1;
                    case 'r':
                        if (prefix(p, "ror"))
                            return 56;
                        return -1;
                    case 's':
                        if (prefix(p, "cape-utf8"))
                            return 32;
                    }
                    return -1;
                case 'f':
                    if (prefix(p, "ilter-libs"))
                        return 16;
                    return -1;
                case 'l':
                    if (prefix("iter", p)) {
                        p += 4;
                        switch (*p ++) {
                        case '-':
                            if (prefix(p, "value"))
                                return 24;
                            return -1;
                        case 'a':
                            if (prefix(p, "l-value"))
                                return 26;
                        }
                    }
                    return -1;
                case 'm':
                    if (prefix(p, "ulti-object"))
                        return 38;
                    return -1;
                case 'n':
                    if (prefix(p, "ewline-sep"))
                        return 48;
                    return -1;
                case 'p':
                    if (prefix(p, "rint-dots"))
                        return 40;
                    return -1;
                case 'q':
                    if (prefix(p, "uote-text"))
                        return 44;
                    return -1;
                case 'r':
                    if (prefix(p, "aw-strings"))
                        return 22;
                    return -1;
                case 's':
                    switch (*p ++) {
                    case 'o':
                        if (prefix(p, "rt-keys"))
                            return 50;
                        return -1;
                    case 'u':
                        if (prefix(p, "rrogate-pairs"))
                            return 34;
                    }
                    return -1;
                case 't':
                    switch (*p ++) {
                    case 'r':
                        if (prefix(p, "im-spaces"))
                            return 46;
                        return -1;
                    case 'y':
                        if (prefix(p, "pe-check"))
                            return 14;
                    }
                    return -1;
                case 'v':
                    switch (*p ++) {
                    case 'a':
                        if (prefix("lid", p)) {
                            p += 3;
                            switch (*p ++) {
                            case '-':
                                if (prefix(p, "utf8"))
                                    return 28;
                                return -1;
                            case 'a':
                                if (prefix(p, "te-utf8"))
                                    return 30;
                            }
                        }
                        return -1;
                    case 'e':
                        if (prefix(p, "rbose"))
                            return 58;
                    }
                    return -1;
                case 'w':
                    if (prefix(p, "arning"))
                        return 54;
                }
            }
        }
        return -1;
    case 'p':
        switch (*p ++) {
        case 'a':
            if (prefix(p, "rse-only"))
                return 0;
            return -1;
        case 'r':
            switch (*p ++) {
            case 'e':
                if (prefix(p, "tty-print"))
                    return 3;
                return -1;
            case 'i':
                if (prefix(p, "nt-dots"))
                    return 39;
            }
        }
        return -1;
    case 'q':
        if (prefix(p, "uote-text"))
            return 43;
        return -1;
    case 'r':
        if (prefix(p, "aw-strings"))
            return 21;
        return -1;
    case 's':
        switch (*p ++) {
        case 'i':
            switch (*p ++) {
            case 'g':
                if (prefix(p, "pipe-action"))
                    return 18;
                return -1;
            case 'z':
                if (prefix("es-", p)) {
                    p += 3;
                    switch (*p ++) {
                    case 'a':
                        if (prefix("st-", p)) {
                            p += 3;
                            switch (*p ++) {
                            case 'b':
                                if (prefix("uf-", p)) {
                                    p += 3;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 72;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 71;
                                    }
                                }
                                return -1;
                            case 'p':
                                if (prefix(p, "ool-size"))
                                    return 75;
                                return -1;
                            case 's':
                                if (prefix("tack-", p)) {
                                    p += 5;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 74;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 73;
                                    }
                                }
                            }
                        }
                        return -1;
                    case 'e':
                        switch (*p ++) {
                        case 'r':
                            if (prefix("ror-", p)) {
                                p += 4;
                                switch (*p ++) {
                                case 'b':
                                    if (prefix("uf-", p)) {
                                        p += 3;
                                        switch (*p ++) {
                                        case 'i':
                                            if (prefix(p, "nit"))
                                                return 60;
                                            return -1;
                                        case 'm':
                                            if (prefix(p, "ax"))
                                                return 59;
                                        }
                                    }
                                    return -1;
                                case 'c':
                                    if (prefix(p, "ontext-size"))
                                        return 65;
                                }
                            }
                            return -1;
                        case 's':
                            if (prefix("c-buf-", p)) {
                                p += 6;
                                switch (*p ++) {
                                case 'i':
                                    if (prefix(p, "nit"))
                                        return 99;
                                    return -1;
                                case 'm':
                                    if (prefix(p, "ax"))
                                        return 98;
                                }
                            }
                        }
                        return -1;
                    case 'i':
                        if (prefix(p, "nput-buf-size"))
                            return 61;
                        return -1;
                    case 'j':
                        if (prefix("son2-stack-", p)) {
                            p += 11;
                            switch (*p ++) {
                            case 'i':
                                if (prefix(p, "nit"))
                                    return 64;
                                return -1;
                            case 'm':
                                if (prefix(p, "ax"))
                                    return 63;
                            }
                        }
                        return -1;
                    case 'l':
                        if (prefix("ib-", p)) {
                            p += 3;
                            switch (*p ++) {
                            case 'b':
                                if (prefix("uf-", p)) {
                                    p += 3;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 91;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 90;
                                    }
                                }
                                return -1;
                            case 'o':
                                if (prefix(p, "wn-pool-size"))
                                    return 95;
                                return -1;
                            case 'p':
                                switch (*p ++) {
                                case 'o':
                                    if (prefix(p, "ol-size"))
                                        return 94;
                                    return -1;
                                case 't':
                                    if (prefix(p, "r-space-size"))
                                        return 96;
                                }
                                return -1;
                            case 's':
                                if (prefix("tack-", p)) {
                                    p += 5;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 93;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 92;
                                    }
                                }
                                return -1;
                            case 't':
                                if (prefix(p, "ext-max-size"))
                                    return 97;
                            }
                        }
                        return -1;
                    case 'o':
                        if (prefix("bj-", p)) {
                            p += 3;
                            switch (*p ++) {
                            case 'b':
                                if (prefix("uf-", p)) {
                                    p += 3;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 68;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 67;
                                    }
                                }
                                return -1;
                            case 's':
                                if (prefix("tack-", p)) {
                                    p += 5;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 70;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 69;
                                    }
                                }
                            }
                        }
                        return -1;
                    case 't':
                        if (prefix("ype-", p)) {
                            p += 4;
                            switch (*p ++) {
                            case 'l':
                                if (prefix("ib-", p)) {
                                    p += 3;
                                    switch (*p ++) {
                                    case 'b':
                                        if (prefix("uf-", p)) {
                                            p += 3;
                                            switch (*p ++) {
                                            case 'i':
                                                if (prefix(p, "nit"))
                                                    return 81;
                                                return -1;
                                            case 'm':
                                                if (prefix(p, "ax"))
                                                    return 80;
                                            }
                                        }
                                        return -1;
                                    case 'o':
                                        if (prefix(p, "wn-pool-size"))
                                            return 85;
                                        return -1;
                                    case 'p':
                                        switch (*p ++) {
                                        case 'o':
                                            if (prefix(p, "ol-size"))
                                                return 84;
                                            return -1;
                                        case 't':
                                            if (prefix(p, "r-space-size"))
                                                return 86;
                                        }
                                        return -1;
                                    case 's':
                                        if (prefix("tack-", p)) {
                                            p += 5;
                                            switch (*p ++) {
                                            case 'i':
                                                if (prefix(p, "nit"))
                                                    return 83;
                                                return -1;
                                            case 'm':
                                                if (prefix(p, "ax"))
                                                    return 82;
                                            }
                                        }
                                        return -1;
                                    case 't':
                                        if (prefix(p, "ext-max-size"))
                                            return 87;
                                    }
                                }
                                return -1;
                            case 'o':
                                if (prefix("bj-", p)) {
                                    p += 3;
                                    switch (*p ++) {
                                    case 'b':
                                        if (prefix("uf-", p)) {
                                            p += 3;
                                            switch (*p ++) {
                                            case 'i':
                                                if (prefix(p, "nit"))
                                                    return 77;
                                                return -1;
                                            case 'm':
                                                if (prefix(p, "ax"))
                                                    return 76;
                                            }
                                        }
                                        return -1;
                                    case 's':
                                        if (prefix("tack-", p)) {
                                            p += 5;
                                            switch (*p ++) {
                                            case 'i':
                                                if (prefix(p, "nit"))
                                                    return 79;
                                                return -1;
                                            case 'm':
                                                if (prefix(p, "ax"))
                                                    return 78;
                                            }
                                        }
                                    }
                                }
                                return -1;
                            case 'v':
                                if (prefix("ar-stack-", p)) {
                                    p += 9;
                                    switch (*p ++) {
                                    case 'i':
                                        if (prefix(p, "nit"))
                                            return 89;
                                        return -1;
                                    case 'm':
                                        if (prefix(p, "ax"))
                                            return 88;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return -1;
        case 'o':
            if (prefix(p, "rt-keys"))
                return 49;
            return -1;
        case 'u':
            if (prefix(p, "rrogate-pairs"))
                return 33;
        }
        return -1;
    case 't':
        switch (*p ++) {
        case 'e':
            if (prefix(p, "rse-print"))
                return 4;
            return -1;
        case 'r':
            if (prefix(p, "im-spaces"))
                return 45;
            return -1;
        case 'y':
            if (prefix("pe", p)) {
                p += 2;
                switch (*p ++) {
                case '-':
                    switch (*p ++) {
                    case 'd':
                        if (prefix(p, "ef"))
                            return 11;
                        return -1;
                    case 'l':
                        if (prefix(p, "ib"))
                            return 12;
                        return -1;
                    case 'n':
                        if (prefix(p, "ame"))
                            return 13;
                        return -1;
                    case 'p':
                        if (prefix(p, "rint"))
                            return 5;
                    }
                    return -1;
                case 'l':
                    if (prefix(p, "ib-func"))
                        return 7;
                }
            }
        }
        return -1;
    case 'v':
        switch (*p ++) {
        case 'a':
            if (prefix("lid", p)) {
                p += 3;
                switch (*p ++) {
                case '-':
                    if (prefix(p, "utf8"))
                        return 27;
                    return -1;
                case 'a':
                    if (prefix(p, "te-utf8"))
                        return 29;
                }
            }
            return -1;
        case 'e':
            if (*p ++ == 'r') {
                switch (*p ++) {
                case 'b':
                    if (prefix(p, "ose"))
                        return 57;
                    return -1;
                case 's':
                    if (prefix(p, "ion"))
                        return 52;
                }
            }
        }
        return -1;
    case 'w':
        if (prefix(p, "arning"))
            return 53;
    }
    return -1;
}


