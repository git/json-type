#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C parser:string-unicode6
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L string-unicode6.old <(echo \
'$ json0() { LD_LIBRARY_PATH=../lib ../src/json --pretty --verbose "$@"; }
$ json() { json0 --literal-value --escape-utf8 "$@"; }
$ echo '\''"\u00f0"'\''|json -b 1
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 2
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 3
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 4
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 5
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 6
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 7
"\u00f0"
$ echo '\''"\u00f0"'\''|json -b 8
"\u00f0"
$ echo '\''"\u00f1"'\''|json -b 1
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 2
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 3
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 4
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 5
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 6
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 7
"\u00f1"
$ echo '\''"\u00f1"'\''|json -b 8
"\u00f1"
$ echo '\''"\u00f2"'\''|json -b 1
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 2
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 3
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 4
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 5
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 6
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 7
"\u00f2"
$ echo '\''"\u00f2"'\''|json -b 8
"\u00f2"
$ echo '\''"\u00f3"'\''|json -b 1
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 2
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 3
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 4
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 5
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 6
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 7
"\u00f3"
$ echo '\''"\u00f3"'\''|json -b 8
"\u00f3"
$ echo '\''"\u00f4"'\''|json -b 1
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 2
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 3
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 4
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 5
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 6
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 7
"\u00f4"
$ echo '\''"\u00f4"'\''|json -b 8
"\u00f4"
$ echo '\''"\u00f5"'\''|json -b 1
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 2
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 3
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 4
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 5
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 6
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 7
"\u00f5"
$ echo '\''"\u00f5"'\''|json -b 8
"\u00f5"
$ echo '\''"\u00f6"'\''|json -b 1
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 2
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 3
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 4
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 5
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 6
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 7
"\u00f6"
$ echo '\''"\u00f6"'\''|json -b 8
"\u00f6"
$ echo '\''"\u00f7"'\''|json -b 1
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 2
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 3
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 4
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 5
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 6
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 7
"\u00f7"
$ echo '\''"\u00f7"'\''|json -b 8
"\u00f7"
$ echo '\''"\u00f8"'\''|json -b 1
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 2
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 3
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 4
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 5
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 6
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 7
"\u00f8"
$ echo '\''"\u00f8"'\''|json -b 8
"\u00f8"
$ echo '\''"\u00f9"'\''|json -b 1
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 2
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 3
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 4
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 5
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 6
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 7
"\u00f9"
$ echo '\''"\u00f9"'\''|json -b 8
"\u00f9"
$ echo '\''"\u00fa"'\''|json -b 1
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 2
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 3
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 4
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 5
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 6
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 7
"\u00fa"
$ echo '\''"\u00fa"'\''|json -b 8
"\u00fa"
$ echo '\''"\u00fb"'\''|json -b 1
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 2
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 3
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 4
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 5
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 6
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 7
"\u00fb"
$ echo '\''"\u00fb"'\''|json -b 8
"\u00fb"
$ echo '\''"\u00fc"'\''|json -b 1
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 2
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 3
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 4
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 5
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 6
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 7
"\u00fc"
$ echo '\''"\u00fc"'\''|json -b 8
"\u00fc"
$ echo '\''"\u00fd"'\''|json -b 1
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 2
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 3
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 4
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 5
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 6
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 7
"\u00fd"
$ echo '\''"\u00fd"'\''|json -b 8
"\u00fd"
$ echo '\''"\u00fe"'\''|json -b 1
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 2
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 3
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 4
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 5
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 6
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 7
"\u00fe"
$ echo '\''"\u00fe"'\''|json -b 8
"\u00fe"
$ echo '\''"\u00ff"'\''|json -b 1
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 2
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 3
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 4
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 5
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 6
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 7
"\u00ff"
$ echo '\''"\u00ff"'\''|json -b 8
"\u00ff"'
) -L string-unicode6.new <(
echo '$ json0() { LD_LIBRARY_PATH=../lib ../src/json --pretty --verbose "$@"; }'
json0() { LD_LIBRARY_PATH=../lib ../src/json --pretty --verbose "$@"; } 2>&1 ||
echo 'command failed: json0() { LD_LIBRARY_PATH=../lib ../src/json --pretty --verbose "$@"; }'

echo '$ json() { json0 --literal-value --escape-utf8 "$@"; }'
json() { json0 --literal-value --escape-utf8 "$@"; } 2>&1 ||
echo 'command failed: json() { json0 --literal-value --escape-utf8 "$@"; }'

echo '$ echo '\''"\u00f0"'\''|json -b 1'
echo '"\u00f0"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 1'

echo '$ echo '\''"\u00f0"'\''|json -b 2'
echo '"\u00f0"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 2'

echo '$ echo '\''"\u00f0"'\''|json -b 3'
echo '"\u00f0"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 3'

echo '$ echo '\''"\u00f0"'\''|json -b 4'
echo '"\u00f0"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 4'

echo '$ echo '\''"\u00f0"'\''|json -b 5'
echo '"\u00f0"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 5'

echo '$ echo '\''"\u00f0"'\''|json -b 6'
echo '"\u00f0"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 6'

echo '$ echo '\''"\u00f0"'\''|json -b 7'
echo '"\u00f0"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 7'

echo '$ echo '\''"\u00f0"'\''|json -b 8'
echo '"\u00f0"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f0"'\''|json -b 8'

echo '$ echo '\''"\u00f1"'\''|json -b 1'
echo '"\u00f1"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 1'

echo '$ echo '\''"\u00f1"'\''|json -b 2'
echo '"\u00f1"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 2'

echo '$ echo '\''"\u00f1"'\''|json -b 3'
echo '"\u00f1"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 3'

echo '$ echo '\''"\u00f1"'\''|json -b 4'
echo '"\u00f1"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 4'

echo '$ echo '\''"\u00f1"'\''|json -b 5'
echo '"\u00f1"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 5'

echo '$ echo '\''"\u00f1"'\''|json -b 6'
echo '"\u00f1"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 6'

echo '$ echo '\''"\u00f1"'\''|json -b 7'
echo '"\u00f1"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 7'

echo '$ echo '\''"\u00f1"'\''|json -b 8'
echo '"\u00f1"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f1"'\''|json -b 8'

echo '$ echo '\''"\u00f2"'\''|json -b 1'
echo '"\u00f2"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 1'

echo '$ echo '\''"\u00f2"'\''|json -b 2'
echo '"\u00f2"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 2'

echo '$ echo '\''"\u00f2"'\''|json -b 3'
echo '"\u00f2"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 3'

echo '$ echo '\''"\u00f2"'\''|json -b 4'
echo '"\u00f2"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 4'

echo '$ echo '\''"\u00f2"'\''|json -b 5'
echo '"\u00f2"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 5'

echo '$ echo '\''"\u00f2"'\''|json -b 6'
echo '"\u00f2"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 6'

echo '$ echo '\''"\u00f2"'\''|json -b 7'
echo '"\u00f2"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 7'

echo '$ echo '\''"\u00f2"'\''|json -b 8'
echo '"\u00f2"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f2"'\''|json -b 8'

echo '$ echo '\''"\u00f3"'\''|json -b 1'
echo '"\u00f3"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 1'

echo '$ echo '\''"\u00f3"'\''|json -b 2'
echo '"\u00f3"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 2'

echo '$ echo '\''"\u00f3"'\''|json -b 3'
echo '"\u00f3"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 3'

echo '$ echo '\''"\u00f3"'\''|json -b 4'
echo '"\u00f3"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 4'

echo '$ echo '\''"\u00f3"'\''|json -b 5'
echo '"\u00f3"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 5'

echo '$ echo '\''"\u00f3"'\''|json -b 6'
echo '"\u00f3"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 6'

echo '$ echo '\''"\u00f3"'\''|json -b 7'
echo '"\u00f3"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 7'

echo '$ echo '\''"\u00f3"'\''|json -b 8'
echo '"\u00f3"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f3"'\''|json -b 8'

echo '$ echo '\''"\u00f4"'\''|json -b 1'
echo '"\u00f4"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 1'

echo '$ echo '\''"\u00f4"'\''|json -b 2'
echo '"\u00f4"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 2'

echo '$ echo '\''"\u00f4"'\''|json -b 3'
echo '"\u00f4"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 3'

echo '$ echo '\''"\u00f4"'\''|json -b 4'
echo '"\u00f4"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 4'

echo '$ echo '\''"\u00f4"'\''|json -b 5'
echo '"\u00f4"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 5'

echo '$ echo '\''"\u00f4"'\''|json -b 6'
echo '"\u00f4"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 6'

echo '$ echo '\''"\u00f4"'\''|json -b 7'
echo '"\u00f4"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 7'

echo '$ echo '\''"\u00f4"'\''|json -b 8'
echo '"\u00f4"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f4"'\''|json -b 8'

echo '$ echo '\''"\u00f5"'\''|json -b 1'
echo '"\u00f5"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 1'

echo '$ echo '\''"\u00f5"'\''|json -b 2'
echo '"\u00f5"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 2'

echo '$ echo '\''"\u00f5"'\''|json -b 3'
echo '"\u00f5"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 3'

echo '$ echo '\''"\u00f5"'\''|json -b 4'
echo '"\u00f5"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 4'

echo '$ echo '\''"\u00f5"'\''|json -b 5'
echo '"\u00f5"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 5'

echo '$ echo '\''"\u00f5"'\''|json -b 6'
echo '"\u00f5"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 6'

echo '$ echo '\''"\u00f5"'\''|json -b 7'
echo '"\u00f5"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 7'

echo '$ echo '\''"\u00f5"'\''|json -b 8'
echo '"\u00f5"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f5"'\''|json -b 8'

echo '$ echo '\''"\u00f6"'\''|json -b 1'
echo '"\u00f6"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 1'

echo '$ echo '\''"\u00f6"'\''|json -b 2'
echo '"\u00f6"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 2'

echo '$ echo '\''"\u00f6"'\''|json -b 3'
echo '"\u00f6"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 3'

echo '$ echo '\''"\u00f6"'\''|json -b 4'
echo '"\u00f6"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 4'

echo '$ echo '\''"\u00f6"'\''|json -b 5'
echo '"\u00f6"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 5'

echo '$ echo '\''"\u00f6"'\''|json -b 6'
echo '"\u00f6"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 6'

echo '$ echo '\''"\u00f6"'\''|json -b 7'
echo '"\u00f6"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 7'

echo '$ echo '\''"\u00f6"'\''|json -b 8'
echo '"\u00f6"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f6"'\''|json -b 8'

echo '$ echo '\''"\u00f7"'\''|json -b 1'
echo '"\u00f7"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 1'

echo '$ echo '\''"\u00f7"'\''|json -b 2'
echo '"\u00f7"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 2'

echo '$ echo '\''"\u00f7"'\''|json -b 3'
echo '"\u00f7"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 3'

echo '$ echo '\''"\u00f7"'\''|json -b 4'
echo '"\u00f7"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 4'

echo '$ echo '\''"\u00f7"'\''|json -b 5'
echo '"\u00f7"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 5'

echo '$ echo '\''"\u00f7"'\''|json -b 6'
echo '"\u00f7"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 6'

echo '$ echo '\''"\u00f7"'\''|json -b 7'
echo '"\u00f7"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 7'

echo '$ echo '\''"\u00f7"'\''|json -b 8'
echo '"\u00f7"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f7"'\''|json -b 8'

echo '$ echo '\''"\u00f8"'\''|json -b 1'
echo '"\u00f8"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 1'

echo '$ echo '\''"\u00f8"'\''|json -b 2'
echo '"\u00f8"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 2'

echo '$ echo '\''"\u00f8"'\''|json -b 3'
echo '"\u00f8"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 3'

echo '$ echo '\''"\u00f8"'\''|json -b 4'
echo '"\u00f8"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 4'

echo '$ echo '\''"\u00f8"'\''|json -b 5'
echo '"\u00f8"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 5'

echo '$ echo '\''"\u00f8"'\''|json -b 6'
echo '"\u00f8"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 6'

echo '$ echo '\''"\u00f8"'\''|json -b 7'
echo '"\u00f8"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 7'

echo '$ echo '\''"\u00f8"'\''|json -b 8'
echo '"\u00f8"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f8"'\''|json -b 8'

echo '$ echo '\''"\u00f9"'\''|json -b 1'
echo '"\u00f9"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 1'

echo '$ echo '\''"\u00f9"'\''|json -b 2'
echo '"\u00f9"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 2'

echo '$ echo '\''"\u00f9"'\''|json -b 3'
echo '"\u00f9"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 3'

echo '$ echo '\''"\u00f9"'\''|json -b 4'
echo '"\u00f9"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 4'

echo '$ echo '\''"\u00f9"'\''|json -b 5'
echo '"\u00f9"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 5'

echo '$ echo '\''"\u00f9"'\''|json -b 6'
echo '"\u00f9"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 6'

echo '$ echo '\''"\u00f9"'\''|json -b 7'
echo '"\u00f9"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 7'

echo '$ echo '\''"\u00f9"'\''|json -b 8'
echo '"\u00f9"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00f9"'\''|json -b 8'

echo '$ echo '\''"\u00fa"'\''|json -b 1'
echo '"\u00fa"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 1'

echo '$ echo '\''"\u00fa"'\''|json -b 2'
echo '"\u00fa"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 2'

echo '$ echo '\''"\u00fa"'\''|json -b 3'
echo '"\u00fa"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 3'

echo '$ echo '\''"\u00fa"'\''|json -b 4'
echo '"\u00fa"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 4'

echo '$ echo '\''"\u00fa"'\''|json -b 5'
echo '"\u00fa"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 5'

echo '$ echo '\''"\u00fa"'\''|json -b 6'
echo '"\u00fa"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 6'

echo '$ echo '\''"\u00fa"'\''|json -b 7'
echo '"\u00fa"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 7'

echo '$ echo '\''"\u00fa"'\''|json -b 8'
echo '"\u00fa"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00fa"'\''|json -b 8'

echo '$ echo '\''"\u00fb"'\''|json -b 1'
echo '"\u00fb"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 1'

echo '$ echo '\''"\u00fb"'\''|json -b 2'
echo '"\u00fb"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 2'

echo '$ echo '\''"\u00fb"'\''|json -b 3'
echo '"\u00fb"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 3'

echo '$ echo '\''"\u00fb"'\''|json -b 4'
echo '"\u00fb"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 4'

echo '$ echo '\''"\u00fb"'\''|json -b 5'
echo '"\u00fb"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 5'

echo '$ echo '\''"\u00fb"'\''|json -b 6'
echo '"\u00fb"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 6'

echo '$ echo '\''"\u00fb"'\''|json -b 7'
echo '"\u00fb"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 7'

echo '$ echo '\''"\u00fb"'\''|json -b 8'
echo '"\u00fb"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00fb"'\''|json -b 8'

echo '$ echo '\''"\u00fc"'\''|json -b 1'
echo '"\u00fc"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 1'

echo '$ echo '\''"\u00fc"'\''|json -b 2'
echo '"\u00fc"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 2'

echo '$ echo '\''"\u00fc"'\''|json -b 3'
echo '"\u00fc"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 3'

echo '$ echo '\''"\u00fc"'\''|json -b 4'
echo '"\u00fc"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 4'

echo '$ echo '\''"\u00fc"'\''|json -b 5'
echo '"\u00fc"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 5'

echo '$ echo '\''"\u00fc"'\''|json -b 6'
echo '"\u00fc"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 6'

echo '$ echo '\''"\u00fc"'\''|json -b 7'
echo '"\u00fc"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 7'

echo '$ echo '\''"\u00fc"'\''|json -b 8'
echo '"\u00fc"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00fc"'\''|json -b 8'

echo '$ echo '\''"\u00fd"'\''|json -b 1'
echo '"\u00fd"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 1'

echo '$ echo '\''"\u00fd"'\''|json -b 2'
echo '"\u00fd"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 2'

echo '$ echo '\''"\u00fd"'\''|json -b 3'
echo '"\u00fd"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 3'

echo '$ echo '\''"\u00fd"'\''|json -b 4'
echo '"\u00fd"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 4'

echo '$ echo '\''"\u00fd"'\''|json -b 5'
echo '"\u00fd"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 5'

echo '$ echo '\''"\u00fd"'\''|json -b 6'
echo '"\u00fd"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 6'

echo '$ echo '\''"\u00fd"'\''|json -b 7'
echo '"\u00fd"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 7'

echo '$ echo '\''"\u00fd"'\''|json -b 8'
echo '"\u00fd"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00fd"'\''|json -b 8'

echo '$ echo '\''"\u00fe"'\''|json -b 1'
echo '"\u00fe"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 1'

echo '$ echo '\''"\u00fe"'\''|json -b 2'
echo '"\u00fe"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 2'

echo '$ echo '\''"\u00fe"'\''|json -b 3'
echo '"\u00fe"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 3'

echo '$ echo '\''"\u00fe"'\''|json -b 4'
echo '"\u00fe"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 4'

echo '$ echo '\''"\u00fe"'\''|json -b 5'
echo '"\u00fe"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 5'

echo '$ echo '\''"\u00fe"'\''|json -b 6'
echo '"\u00fe"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 6'

echo '$ echo '\''"\u00fe"'\''|json -b 7'
echo '"\u00fe"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 7'

echo '$ echo '\''"\u00fe"'\''|json -b 8'
echo '"\u00fe"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00fe"'\''|json -b 8'

echo '$ echo '\''"\u00ff"'\''|json -b 1'
echo '"\u00ff"'|json -b 1 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 1'

echo '$ echo '\''"\u00ff"'\''|json -b 2'
echo '"\u00ff"'|json -b 2 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 2'

echo '$ echo '\''"\u00ff"'\''|json -b 3'
echo '"\u00ff"'|json -b 3 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 3'

echo '$ echo '\''"\u00ff"'\''|json -b 4'
echo '"\u00ff"'|json -b 4 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 4'

echo '$ echo '\''"\u00ff"'\''|json -b 5'
echo '"\u00ff"'|json -b 5 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 5'

echo '$ echo '\''"\u00ff"'\''|json -b 6'
echo '"\u00ff"'|json -b 6 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 6'

echo '$ echo '\''"\u00ff"'\''|json -b 7'
echo '"\u00ff"'|json -b 7 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 7'

echo '$ echo '\''"\u00ff"'\''|json -b 8'
echo '"\u00ff"'|json -b 8 2>&1 ||
echo 'command failed: echo '\''"\u00ff"'\''|json -b 8'
)

