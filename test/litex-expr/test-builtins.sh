#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -p litex -C expr:builtins
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L builtins.old <(echo \
'$ shopt -s expand_aliases
$ [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"
$ test-expr() { LD_LIBRARY_PATH="$JSON_LITEX_TEST_PCRE2_LIB" ../lib/test-expr "$@" -vb default -E; }
$ test-expr '\''null'\'' '\''null'\''
1
$ test-expr '\''null'\'' '\''false'\''
0
$ test-expr '\''null'\'' '\''true'\''
0
$ test-expr '\''null'\'' '\''0'\''
0
$ test-expr '\''null'\'' '\''1'\''
0
$ test-expr '\''null'\'' '\''"foo"'\''
0
$ test-expr '\''boolean'\'' '\''null'\''
0
$ test-expr '\''boolean'\'' '\''false'\''
1
$ test-expr '\''boolean'\'' '\''true'\''
1
$ test-expr '\''boolean'\'' '\''0'\''
0
$ test-expr '\''boolean'\'' '\''1'\''
0
$ test-expr '\''boolean'\'' '\''"foo"'\''
0
$ test-expr '\''number'\'' '\''null'\''
0
$ test-expr '\''number'\'' '\''false'\''
0
$ test-expr '\''number'\'' '\''true'\''
0
$ test-expr '\''number'\'' '\''0'\''
1
$ test-expr '\''number'\'' '\''1'\''
1
$ test-expr '\''number'\'' '\''"foo"'\''
0
$ test-expr '\''string'\'' '\''null'\''
0
$ test-expr '\''string'\'' '\''false'\''
0
$ test-expr '\''string'\'' '\''true'\''
0
$ test-expr '\''string'\'' '\''0'\''
0
$ test-expr '\''string'\'' '\''1'\''
0
$ test-expr '\''string'\'' '\''"foo"'\''
1
$ test-expr '\''len'\'' '\''null'\''
test-expr: error: executor error: function '\''len'\'' applied to '\''null'\''
command failed: test-expr '\''len'\'' '\''null'\''
$ test-expr '\''len'\'' '\''false'\''
test-expr: error: executor error: function '\''len'\'' applied to '\''boolean'\''
command failed: test-expr '\''len'\'' '\''false'\''
$ test-expr '\''len'\'' '\''true'\''
test-expr: error: executor error: function '\''len'\'' applied to '\''boolean'\''
command failed: test-expr '\''len'\'' '\''true'\''
$ test-expr '\''len'\'' '\''0'\''
1
$ test-expr '\''len'\'' '\''1'\''
1
$ test-expr '\''len'\'' '\''"foo"'\''
3
$ test-expr '\''float'\'' '\''null'\''
0
$ test-expr '\''float'\'' '\''false'\''
0
$ test-expr '\''float'\'' '\''true'\''
0
$ test-expr '\''float'\'' '\''0'\''
1
$ test-expr '\''float'\'' '\''1'\''
1
$ test-expr '\''float'\'' '\''"foo"'\''
0
$ test-expr '\''double'\'' '\''null'\''
0
$ test-expr '\''double'\'' '\''false'\''
0
$ test-expr '\''double'\'' '\''true'\''
0
$ test-expr '\''double'\'' '\''0'\''
1
$ test-expr '\''double'\'' '\''1'\''
1
$ test-expr '\''double'\'' '\''"foo"'\''
0
$ test-expr '\''ldouble'\'' '\''null'\''
0
$ test-expr '\''ldouble'\'' '\''false'\''
0
$ test-expr '\''ldouble'\'' '\''true'\''
0
$ test-expr '\''ldouble'\'' '\''0'\''
1
$ test-expr '\''ldouble'\'' '\''1'\''
1
$ test-expr '\''ldouble'\'' '\''"foo"'\''
0
$ test-expr '\''int(0)'\'' '\''null'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''null'\''
$ test-expr '\''int(0)'\'' '\''false'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''false'\''
$ test-expr '\''int(0)'\'' '\''true'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''true'\''
$ test-expr '\''int(0)'\'' '\''0'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''0'\''
$ test-expr '\''int(0)'\'' '\''1'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''1'\''
$ test-expr '\''int(0)'\'' '\''"foo"'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(0)'\'' '\''"foo"'\''
$ test-expr '\''int(8)'\'' '\''null'\''
0
$ test-expr '\''int(8)'\'' '\''false'\''
0
$ test-expr '\''int(8)'\'' '\''true'\''
0
$ test-expr '\''int(8)'\'' '\''0'\''
1
$ test-expr '\''int(8)'\'' '\''1'\''
1
$ test-expr '\''int(8)'\'' '\''"foo"'\''
0
$ test-expr '\''int(16)'\'' '\''null'\''
0
$ test-expr '\''int(16)'\'' '\''false'\''
0
$ test-expr '\''int(16)'\'' '\''true'\''
0
$ test-expr '\''int(16)'\'' '\''0'\''
1
$ test-expr '\''int(16)'\'' '\''1'\''
1
$ test-expr '\''int(16)'\'' '\''"foo"'\''
0
$ test-expr '\''int(32)'\'' '\''null'\''
0
$ test-expr '\''int(32)'\'' '\''false'\''
0
$ test-expr '\''int(32)'\'' '\''true'\''
0
$ test-expr '\''int(32)'\'' '\''0'\''
1
$ test-expr '\''int(32)'\'' '\''1'\''
1
$ test-expr '\''int(32)'\'' '\''"foo"'\''
0
$ test-expr '\''int(64)'\'' '\''null'\''
0
$ test-expr '\''int(64)'\'' '\''false'\''
0
$ test-expr '\''int(64)'\'' '\''true'\''
0
$ test-expr '\''int(64)'\'' '\''0'\''
1
$ test-expr '\''int(64)'\'' '\''1'\''
1
$ test-expr '\''int(64)'\'' '\''"foo"'\''
0
$ test-expr '\''int(128)'\'' '\''null'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''null'\''
$ test-expr '\''int(128)'\'' '\''false'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''false'\''
$ test-expr '\''int(128)'\'' '\''true'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''true'\''
$ test-expr '\''int(128)'\'' '\''0'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''0'\''
$ test-expr '\''int(128)'\'' '\''1'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''1'\''
$ test-expr '\''int(128)'\'' '\''"foo"'\''
test-expr: error: executor error: invalid argument of function '\''int'\''
command failed: test-expr '\''int(128)'\'' '\''"foo"'\''
$ test-expr '\''uint(0)'\'' '\''null'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''null'\''
$ test-expr '\''uint(0)'\'' '\''false'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''false'\''
$ test-expr '\''uint(0)'\'' '\''true'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''true'\''
$ test-expr '\''uint(0)'\'' '\''0'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''0'\''
$ test-expr '\''uint(0)'\'' '\''1'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''1'\''
$ test-expr '\''uint(0)'\'' '\''"foo"'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(0)'\'' '\''"foo"'\''
$ test-expr '\''uint(8)'\'' '\''null'\''
0
$ test-expr '\''uint(8)'\'' '\''false'\''
0
$ test-expr '\''uint(8)'\'' '\''true'\''
0
$ test-expr '\''uint(8)'\'' '\''0'\''
1
$ test-expr '\''uint(8)'\'' '\''1'\''
1
$ test-expr '\''uint(8)'\'' '\''"foo"'\''
0
$ test-expr '\''uint(16)'\'' '\''null'\''
0
$ test-expr '\''uint(16)'\'' '\''false'\''
0
$ test-expr '\''uint(16)'\'' '\''true'\''
0
$ test-expr '\''uint(16)'\'' '\''0'\''
1
$ test-expr '\''uint(16)'\'' '\''1'\''
1
$ test-expr '\''uint(16)'\'' '\''"foo"'\''
0
$ test-expr '\''uint(32)'\'' '\''null'\''
0
$ test-expr '\''uint(32)'\'' '\''false'\''
0
$ test-expr '\''uint(32)'\'' '\''true'\''
0
$ test-expr '\''uint(32)'\'' '\''0'\''
1
$ test-expr '\''uint(32)'\'' '\''1'\''
1
$ test-expr '\''uint(32)'\'' '\''"foo"'\''
0
$ test-expr '\''uint(64)'\'' '\''null'\''
0
$ test-expr '\''uint(64)'\'' '\''false'\''
0
$ test-expr '\''uint(64)'\'' '\''true'\''
0
$ test-expr '\''uint(64)'\'' '\''0'\''
1
$ test-expr '\''uint(64)'\'' '\''1'\''
1
$ test-expr '\''uint(64)'\'' '\''"foo"'\''
0
$ test-expr '\''uint(128)'\'' '\''null'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''null'\''
$ test-expr '\''uint(128)'\'' '\''false'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''false'\''
$ test-expr '\''uint(128)'\'' '\''true'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''true'\''
$ test-expr '\''uint(128)'\'' '\''0'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''0'\''
$ test-expr '\''uint(128)'\'' '\''1'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''1'\''
$ test-expr '\''uint(128)'\'' '\''"foo"'\''
test-expr: error: executor error: invalid argument of function '\''uint'\''
command failed: test-expr '\''uint(128)'\'' '\''"foo"'\''
$ test-expr '\''date(`foo`)'\'' '\''null'\''
0
$ test-expr '\''date(`foo`)'\'' '\''false'\''
0
$ test-expr '\''date(`foo`)'\'' '\''true'\''
0
$ test-expr '\''date(`foo`)'\'' '\''0'\''
0
$ test-expr '\''date(`foo`)'\'' '\''1'\''
0
$ test-expr '\''date(`foo`)'\'' '\''"foo"'\''
1
$ test-expr '\''date(`bar`)'\'' '\''null'\''
0
$ test-expr '\''date(`bar`)'\'' '\''false'\''
0
$ test-expr '\''date(`bar`)'\'' '\''true'\''
0
$ test-expr '\''date(`bar`)'\'' '\''0'\''
0
$ test-expr '\''date(`bar`)'\'' '\''1'\''
0
$ test-expr '\''date(`bar`)'\'' '\''"foo"'\''
0'
) -L builtins.new <(
echo '$ shopt -s expand_aliases'
shopt -s expand_aliases 2>&1 ||
echo 'command failed: shopt -s expand_aliases'

echo '$ [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"'
[ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib" 2>&1 ||
echo 'command failed: [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"'

echo '$ test-expr() { LD_LIBRARY_PATH="$JSON_LITEX_TEST_PCRE2_LIB" ../lib/test-expr "$@" -vb default -E; }'
test-expr() { LD_LIBRARY_PATH="$JSON_LITEX_TEST_PCRE2_LIB" ../lib/test-expr "$@" -vb default -E; } 2>&1 ||
echo 'command failed: test-expr() { LD_LIBRARY_PATH="$JSON_LITEX_TEST_PCRE2_LIB" ../lib/test-expr "$@" -vb default -E; }'

echo '$ test-expr '\''null'\'' '\''null'\'''
test-expr 'null' 'null' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''null'\'''

echo '$ test-expr '\''null'\'' '\''false'\'''
test-expr 'null' 'false' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''false'\'''

echo '$ test-expr '\''null'\'' '\''true'\'''
test-expr 'null' 'true' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''true'\'''

echo '$ test-expr '\''null'\'' '\''0'\'''
test-expr 'null' '0' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''0'\'''

echo '$ test-expr '\''null'\'' '\''1'\'''
test-expr 'null' '1' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''1'\'''

echo '$ test-expr '\''null'\'' '\''"foo"'\'''
test-expr 'null' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''null'\'' '\''"foo"'\'''

echo '$ test-expr '\''boolean'\'' '\''null'\'''
test-expr 'boolean' 'null' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''null'\'''

echo '$ test-expr '\''boolean'\'' '\''false'\'''
test-expr 'boolean' 'false' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''false'\'''

echo '$ test-expr '\''boolean'\'' '\''true'\'''
test-expr 'boolean' 'true' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''true'\'''

echo '$ test-expr '\''boolean'\'' '\''0'\'''
test-expr 'boolean' '0' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''0'\'''

echo '$ test-expr '\''boolean'\'' '\''1'\'''
test-expr 'boolean' '1' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''1'\'''

echo '$ test-expr '\''boolean'\'' '\''"foo"'\'''
test-expr 'boolean' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''boolean'\'' '\''"foo"'\'''

echo '$ test-expr '\''number'\'' '\''null'\'''
test-expr 'number' 'null' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''null'\'''

echo '$ test-expr '\''number'\'' '\''false'\'''
test-expr 'number' 'false' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''false'\'''

echo '$ test-expr '\''number'\'' '\''true'\'''
test-expr 'number' 'true' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''true'\'''

echo '$ test-expr '\''number'\'' '\''0'\'''
test-expr 'number' '0' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''0'\'''

echo '$ test-expr '\''number'\'' '\''1'\'''
test-expr 'number' '1' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''1'\'''

echo '$ test-expr '\''number'\'' '\''"foo"'\'''
test-expr 'number' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''number'\'' '\''"foo"'\'''

echo '$ test-expr '\''string'\'' '\''null'\'''
test-expr 'string' 'null' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''null'\'''

echo '$ test-expr '\''string'\'' '\''false'\'''
test-expr 'string' 'false' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''false'\'''

echo '$ test-expr '\''string'\'' '\''true'\'''
test-expr 'string' 'true' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''true'\'''

echo '$ test-expr '\''string'\'' '\''0'\'''
test-expr 'string' '0' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''0'\'''

echo '$ test-expr '\''string'\'' '\''1'\'''
test-expr 'string' '1' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''1'\'''

echo '$ test-expr '\''string'\'' '\''"foo"'\'''
test-expr 'string' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''string'\'' '\''"foo"'\'''

echo '$ test-expr '\''len'\'' '\''null'\'''
test-expr 'len' 'null' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''null'\'''

echo '$ test-expr '\''len'\'' '\''false'\'''
test-expr 'len' 'false' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''false'\'''

echo '$ test-expr '\''len'\'' '\''true'\'''
test-expr 'len' 'true' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''true'\'''

echo '$ test-expr '\''len'\'' '\''0'\'''
test-expr 'len' '0' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''0'\'''

echo '$ test-expr '\''len'\'' '\''1'\'''
test-expr 'len' '1' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''1'\'''

echo '$ test-expr '\''len'\'' '\''"foo"'\'''
test-expr 'len' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''len'\'' '\''"foo"'\'''

echo '$ test-expr '\''float'\'' '\''null'\'''
test-expr 'float' 'null' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''null'\'''

echo '$ test-expr '\''float'\'' '\''false'\'''
test-expr 'float' 'false' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''false'\'''

echo '$ test-expr '\''float'\'' '\''true'\'''
test-expr 'float' 'true' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''true'\'''

echo '$ test-expr '\''float'\'' '\''0'\'''
test-expr 'float' '0' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''0'\'''

echo '$ test-expr '\''float'\'' '\''1'\'''
test-expr 'float' '1' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''1'\'''

echo '$ test-expr '\''float'\'' '\''"foo"'\'''
test-expr 'float' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''float'\'' '\''"foo"'\'''

echo '$ test-expr '\''double'\'' '\''null'\'''
test-expr 'double' 'null' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''null'\'''

echo '$ test-expr '\''double'\'' '\''false'\'''
test-expr 'double' 'false' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''false'\'''

echo '$ test-expr '\''double'\'' '\''true'\'''
test-expr 'double' 'true' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''true'\'''

echo '$ test-expr '\''double'\'' '\''0'\'''
test-expr 'double' '0' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''0'\'''

echo '$ test-expr '\''double'\'' '\''1'\'''
test-expr 'double' '1' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''1'\'''

echo '$ test-expr '\''double'\'' '\''"foo"'\'''
test-expr 'double' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''double'\'' '\''"foo"'\'''

echo '$ test-expr '\''ldouble'\'' '\''null'\'''
test-expr 'ldouble' 'null' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''null'\'''

echo '$ test-expr '\''ldouble'\'' '\''false'\'''
test-expr 'ldouble' 'false' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''false'\'''

echo '$ test-expr '\''ldouble'\'' '\''true'\'''
test-expr 'ldouble' 'true' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''true'\'''

echo '$ test-expr '\''ldouble'\'' '\''0'\'''
test-expr 'ldouble' '0' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''0'\'''

echo '$ test-expr '\''ldouble'\'' '\''1'\'''
test-expr 'ldouble' '1' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''1'\'''

echo '$ test-expr '\''ldouble'\'' '\''"foo"'\'''
test-expr 'ldouble' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''ldouble'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(0)'\'' '\''null'\'''
test-expr 'int(0)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''null'\'''

echo '$ test-expr '\''int(0)'\'' '\''false'\'''
test-expr 'int(0)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''false'\'''

echo '$ test-expr '\''int(0)'\'' '\''true'\'''
test-expr 'int(0)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''true'\'''

echo '$ test-expr '\''int(0)'\'' '\''0'\'''
test-expr 'int(0)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''0'\'''

echo '$ test-expr '\''int(0)'\'' '\''1'\'''
test-expr 'int(0)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''1'\'''

echo '$ test-expr '\''int(0)'\'' '\''"foo"'\'''
test-expr 'int(0)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(0)'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(8)'\'' '\''null'\'''
test-expr 'int(8)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''null'\'''

echo '$ test-expr '\''int(8)'\'' '\''false'\'''
test-expr 'int(8)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''false'\'''

echo '$ test-expr '\''int(8)'\'' '\''true'\'''
test-expr 'int(8)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''true'\'''

echo '$ test-expr '\''int(8)'\'' '\''0'\'''
test-expr 'int(8)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''0'\'''

echo '$ test-expr '\''int(8)'\'' '\''1'\'''
test-expr 'int(8)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''1'\'''

echo '$ test-expr '\''int(8)'\'' '\''"foo"'\'''
test-expr 'int(8)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(8)'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(16)'\'' '\''null'\'''
test-expr 'int(16)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''null'\'''

echo '$ test-expr '\''int(16)'\'' '\''false'\'''
test-expr 'int(16)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''false'\'''

echo '$ test-expr '\''int(16)'\'' '\''true'\'''
test-expr 'int(16)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''true'\'''

echo '$ test-expr '\''int(16)'\'' '\''0'\'''
test-expr 'int(16)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''0'\'''

echo '$ test-expr '\''int(16)'\'' '\''1'\'''
test-expr 'int(16)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''1'\'''

echo '$ test-expr '\''int(16)'\'' '\''"foo"'\'''
test-expr 'int(16)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(16)'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(32)'\'' '\''null'\'''
test-expr 'int(32)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''null'\'''

echo '$ test-expr '\''int(32)'\'' '\''false'\'''
test-expr 'int(32)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''false'\'''

echo '$ test-expr '\''int(32)'\'' '\''true'\'''
test-expr 'int(32)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''true'\'''

echo '$ test-expr '\''int(32)'\'' '\''0'\'''
test-expr 'int(32)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''0'\'''

echo '$ test-expr '\''int(32)'\'' '\''1'\'''
test-expr 'int(32)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''1'\'''

echo '$ test-expr '\''int(32)'\'' '\''"foo"'\'''
test-expr 'int(32)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(32)'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(64)'\'' '\''null'\'''
test-expr 'int(64)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''null'\'''

echo '$ test-expr '\''int(64)'\'' '\''false'\'''
test-expr 'int(64)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''false'\'''

echo '$ test-expr '\''int(64)'\'' '\''true'\'''
test-expr 'int(64)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''true'\'''

echo '$ test-expr '\''int(64)'\'' '\''0'\'''
test-expr 'int(64)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''0'\'''

echo '$ test-expr '\''int(64)'\'' '\''1'\'''
test-expr 'int(64)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''1'\'''

echo '$ test-expr '\''int(64)'\'' '\''"foo"'\'''
test-expr 'int(64)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(64)'\'' '\''"foo"'\'''

echo '$ test-expr '\''int(128)'\'' '\''null'\'''
test-expr 'int(128)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''null'\'''

echo '$ test-expr '\''int(128)'\'' '\''false'\'''
test-expr 'int(128)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''false'\'''

echo '$ test-expr '\''int(128)'\'' '\''true'\'''
test-expr 'int(128)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''true'\'''

echo '$ test-expr '\''int(128)'\'' '\''0'\'''
test-expr 'int(128)' '0' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''0'\'''

echo '$ test-expr '\''int(128)'\'' '\''1'\'''
test-expr 'int(128)' '1' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''1'\'''

echo '$ test-expr '\''int(128)'\'' '\''"foo"'\'''
test-expr 'int(128)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''int(128)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(0)'\'' '\''null'\'''
test-expr 'uint(0)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''null'\'''

echo '$ test-expr '\''uint(0)'\'' '\''false'\'''
test-expr 'uint(0)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''false'\'''

echo '$ test-expr '\''uint(0)'\'' '\''true'\'''
test-expr 'uint(0)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''true'\'''

echo '$ test-expr '\''uint(0)'\'' '\''0'\'''
test-expr 'uint(0)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''0'\'''

echo '$ test-expr '\''uint(0)'\'' '\''1'\'''
test-expr 'uint(0)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''1'\'''

echo '$ test-expr '\''uint(0)'\'' '\''"foo"'\'''
test-expr 'uint(0)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(0)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(8)'\'' '\''null'\'''
test-expr 'uint(8)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''null'\'''

echo '$ test-expr '\''uint(8)'\'' '\''false'\'''
test-expr 'uint(8)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''false'\'''

echo '$ test-expr '\''uint(8)'\'' '\''true'\'''
test-expr 'uint(8)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''true'\'''

echo '$ test-expr '\''uint(8)'\'' '\''0'\'''
test-expr 'uint(8)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''0'\'''

echo '$ test-expr '\''uint(8)'\'' '\''1'\'''
test-expr 'uint(8)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''1'\'''

echo '$ test-expr '\''uint(8)'\'' '\''"foo"'\'''
test-expr 'uint(8)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(8)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(16)'\'' '\''null'\'''
test-expr 'uint(16)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''null'\'''

echo '$ test-expr '\''uint(16)'\'' '\''false'\'''
test-expr 'uint(16)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''false'\'''

echo '$ test-expr '\''uint(16)'\'' '\''true'\'''
test-expr 'uint(16)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''true'\'''

echo '$ test-expr '\''uint(16)'\'' '\''0'\'''
test-expr 'uint(16)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''0'\'''

echo '$ test-expr '\''uint(16)'\'' '\''1'\'''
test-expr 'uint(16)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''1'\'''

echo '$ test-expr '\''uint(16)'\'' '\''"foo"'\'''
test-expr 'uint(16)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(16)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(32)'\'' '\''null'\'''
test-expr 'uint(32)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''null'\'''

echo '$ test-expr '\''uint(32)'\'' '\''false'\'''
test-expr 'uint(32)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''false'\'''

echo '$ test-expr '\''uint(32)'\'' '\''true'\'''
test-expr 'uint(32)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''true'\'''

echo '$ test-expr '\''uint(32)'\'' '\''0'\'''
test-expr 'uint(32)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''0'\'''

echo '$ test-expr '\''uint(32)'\'' '\''1'\'''
test-expr 'uint(32)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''1'\'''

echo '$ test-expr '\''uint(32)'\'' '\''"foo"'\'''
test-expr 'uint(32)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(32)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(64)'\'' '\''null'\'''
test-expr 'uint(64)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''null'\'''

echo '$ test-expr '\''uint(64)'\'' '\''false'\'''
test-expr 'uint(64)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''false'\'''

echo '$ test-expr '\''uint(64)'\'' '\''true'\'''
test-expr 'uint(64)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''true'\'''

echo '$ test-expr '\''uint(64)'\'' '\''0'\'''
test-expr 'uint(64)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''0'\'''

echo '$ test-expr '\''uint(64)'\'' '\''1'\'''
test-expr 'uint(64)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''1'\'''

echo '$ test-expr '\''uint(64)'\'' '\''"foo"'\'''
test-expr 'uint(64)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(64)'\'' '\''"foo"'\'''

echo '$ test-expr '\''uint(128)'\'' '\''null'\'''
test-expr 'uint(128)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''null'\'''

echo '$ test-expr '\''uint(128)'\'' '\''false'\'''
test-expr 'uint(128)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''false'\'''

echo '$ test-expr '\''uint(128)'\'' '\''true'\'''
test-expr 'uint(128)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''true'\'''

echo '$ test-expr '\''uint(128)'\'' '\''0'\'''
test-expr 'uint(128)' '0' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''0'\'''

echo '$ test-expr '\''uint(128)'\'' '\''1'\'''
test-expr 'uint(128)' '1' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''1'\'''

echo '$ test-expr '\''uint(128)'\'' '\''"foo"'\'''
test-expr 'uint(128)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''uint(128)'\'' '\''"foo"'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''null'\'''
test-expr 'date(`foo`)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''null'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''false'\'''
test-expr 'date(`foo`)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''false'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''true'\'''
test-expr 'date(`foo`)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''true'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''0'\'''
test-expr 'date(`foo`)' '0' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''0'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''1'\'''
test-expr 'date(`foo`)' '1' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''1'\'''

echo '$ test-expr '\''date(`foo`)'\'' '\''"foo"'\'''
test-expr 'date(`foo`)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''date(`foo`)'\'' '\''"foo"'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''null'\'''
test-expr 'date(`bar`)' 'null' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''null'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''false'\'''
test-expr 'date(`bar`)' 'false' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''false'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''true'\'''
test-expr 'date(`bar`)' 'true' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''true'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''0'\'''
test-expr 'date(`bar`)' '0' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''0'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''1'\'''
test-expr 'date(`bar`)' '1' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''1'\'''

echo '$ test-expr '\''date(`bar`)'\'' '\''"foo"'\'''
test-expr 'date(`bar`)' '"foo"' 2>&1 ||
echo 'command failed: test-expr '\''date(`bar`)'\'' '\''"foo"'\'''
)

