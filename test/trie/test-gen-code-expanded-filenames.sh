#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C trie:gen-code-expanded-filenames
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L gen-code-expanded-filenames.old <(echo \
'$ test -x ../lib/test-trie
$ print() { printf '\''%s\n'\'' "$@"; }
$ set -o pipefail
$ ../lib/test-trie -G --expanded-path < filenames.txt
    if (*p ++ == '\''c'\'' &&
        *p ++ == '\''+'\'' &&
        *p ++ == '\''+'\'' &&
        *p ++ == '\''p'\'' &&
        *p ++ == '\''y'\'' &&
        *p ++ == '\''-'\'' &&
        *p ++ == '\''t'\'' &&
        *p ++ == '\''r'\'' &&
        *p ++ == '\''e'\'' &&
        *p ++ == '\''e'\'' &&
        *p ++ == '\''-'\'' &&
        *p ++ == '\''d'\'' &&
        *p ++ == '\''e'\'' &&
        *p ++ == '\''m'\'' &&
        *p ++ == '\''o'\'' &&
        *p ++ == '\''/'\'') {
        switch (*p ++) {
        case '\''.'\'':
            if (*p ++ == '\''s'\'' &&
                *p ++ == '\''r'\'' &&
                *p ++ == '\''c'\'' &&
                *p ++ == '\''-'\'') {
                switch (*p ++) {
                case '\''d'\'':
                    if (*p ++ == '\''i'\'' &&
                        *p ++ == '\''r'\'' &&
                        *p ++ == '\''s'\'' &&
                        *p == 0)
                        return "c++py-tree-demo/.src-dirs";
                    return NULL;
                case '\''f'\'':
                    if (*p ++ == '\''i'\'' &&
                        *p ++ == '\''l'\'' &&
                        *p ++ == '\''e'\'' &&
                        *p ++ == '\''s'\'' &&
                        *p == 0)
                        return "c++py-tree-demo/.src-files";
                }
            }
            return NULL;
        case '\''b'\'':
            if (*p ++ == '\''i'\'' &&
                *p ++ == '\''n'\'' &&
                *p ++ == '\''.'\'' &&
                *p ++ == '\''p'\'' &&
                *p ++ == '\''a'\'' &&
                *p ++ == '\''t'\'' &&
                *p ++ == '\''c'\'' &&
                *p ++ == '\''h'\'' &&
                *p == 0)
                return "c++py-tree-demo/bin.patch";
            return NULL;
        case '\''c'\'':
            if (*p ++ == '\''+'\'' &&
                *p ++ == '\''+'\'' &&
                *p ++ == '\''p'\'' &&
                *p ++ == '\''y'\'') {
                switch (*p ++) {
                case '\''-'\'':
                    if (*p ++ == '\''t'\'' &&
                        *p ++ == '\''r'\'' &&
                        *p ++ == '\''e'\'' &&
                        *p ++ == '\''e'\'') {
                        switch (*p ++) {
                        case '\''-'\'':
                            switch (*p ++) {
                            case '\''a'\'':
                                if (*p ++ == '\''u'\'' &&
                                    *p ++ == '\''t'\'' &&
                                    *p ++ == '\''h'\'' &&
                                    *p ++ == '\''o'\'' &&
                                    *p ++ == '\''r'\'' &&
                                    *p ++ == '\''.'\'' &&
                                    *p ++ == '\''t'\'' &&
                                    *p ++ == '\''x'\'' &&
                                    *p ++ == '\''t'\'' &&
                                    *p == 0)
                                    return "c++py-tree-demo/c++py-tree-author.txt";
                                return NULL;
                            case '\''d'\'':
                                if (*p ++ == '\''e'\'' &&
                                    *p ++ == '\''m'\'' &&
                                    *p ++ == '\''o'\'' &&
                                    *p ++ == '\''.'\'' &&
                                    *p ++ == '\''t'\'' &&
                                    *p ++ == '\''x'\'' &&
                                    *p ++ == '\''t'\'' &&
                                    *p == 0)
                                    return "c++py-tree-demo/c++py-tree-demo.txt";
                                return NULL;
                            case '\''r'\'':
                                if (*p ++ == '\''e'\'' &&
                                    *p ++ == '\''a'\'' &&
                                    *p ++ == '\''d'\'' &&
                                    *p ++ == '\''m'\'' &&
                                    *p ++ == '\''e'\'' &&
                                    *p ++ == '\''.'\'') {
                                    switch (*p ++) {
                                    case '\''p'\'':
                                        if (*p ++ == '\''d'\'' &&
                                            *p ++ == '\''f'\'' &&
                                            *p == 0)
                                            return "c++py-tree-demo/c++py-tree-readme.pdf";
                                        return NULL;
                                    case '\''t'\'':
                                        switch (*p ++) {
                                        case '\''e'\'':
                                            if (*p ++ == '\''x'\'' &&
                                                *p == 0)
                                                return "c++py-tree-demo/c++py-tree-readme.tex";
                                            return NULL;
                                        case '\''x'\'':
                                            if (*p ++ == '\''t'\'' &&
                                                *p == 0)
                                                return "c++py-tree-demo/c++py-tree-readme.txt";
                                        }
                                    }
                                }
                            }
                            return NULL;
                        case '\''.'\'':
                            switch (*p ++) {
                            case '\''p'\'':
                                if (*p ++ == '\''d'\'' &&
                                    *p ++ == '\''f'\'' &&
                                    *p == 0)
                                    return "c++py-tree-demo/c++py-tree.pdf";
                                return NULL;
                            case '\''t'\'':
                                switch (*p ++) {
                                case '\''e'\'':
                                    if (*p ++ == '\''x'\'' &&
                                        *p == 0)
                                        return "c++py-tree-demo/c++py-tree.tex";
                                    return NULL;
                                case '\''x'\'':
                                    if (*p ++ == '\''t'\'' &&
                                        *p == 0)
                                        return "c++py-tree-demo/c++py-tree.txt";
                                }
                            }
                        }
                    }
                    return NULL;
                case '\''/'\'':
                    switch (*p ++) {
                    case '\''.'\'':
                        if (*p ++ == '\''s'\'' &&
                            *p ++ == '\''r'\'' &&
                            *p ++ == '\''c'\'' &&
                            *p ++ == '\''-'\'') {
                            switch (*p ++) {
                            case '\''d'\'':
                                if (*p ++ == '\''i'\'' &&
                                    *p ++ == '\''r'\'' &&
                                    *p ++ == '\''s'\'' &&
                                    *p == 0)
                                    return "c++py-tree-demo/c++py/.src-dirs";
                                return NULL;
                            case '\''f'\'':
                                if (*p ++ == '\''i'\'' &&
                                    *p ++ == '\''l'\'' &&
                                    *p ++ == '\''e'\'' &&
                                    *p ++ == '\''s'\'' &&
                                    *p == 0)
                                    return "c++py-tree-demo/c++py/.src-files";
                            }
                        }
                        return NULL;
                    case '\''A'\'':
                        if (*p ++ == '\''U'\'' &&
                            *p ++ == '\''T'\'' &&
                            *p ++ == '\''H'\'' &&
                            *p ++ == '\''O'\'' &&
                            *p ++ == '\''R'\'' &&
                            *p == 0)
                            return "c++py-tree-demo/c++py/AUTHOR";
                        return NULL;
                    case '\''M'\'':
                        if (*p ++ == '\''a'\'' &&
                            *p ++ == '\''k'\'' &&
                            *p ++ == '\''e'\'' &&
                            *p ++ == '\''f'\'' &&
                            *p ++ == '\''i'\'' &&
                            *p ++ == '\''l'\'' &&
                            *p ++ == '\''e'\'' &&
                            *p == 0)
                            return "c++py-tree-demo/c++py/Makefile";
                        return NULL;
                    case '\''R'\'':
                        if (*p ++ == '\''E'\'' &&
                            *p ++ == '\''A'\'' &&
                            *p ++ == '\''D'\'' &&
                            *p ++ == '\''M'\'' &&
                            *p ++ == '\''E'\'' &&
                            *p == 0)
                            return "c++py-tree-demo/c++py/README";
                    }
                }
            }
        }
    }
    return NULL;'
) -L gen-code-expanded-filenames.new <(
echo '$ test -x ../lib/test-trie'
test -x ../lib/test-trie 2>&1 ||
echo 'command failed: test -x ../lib/test-trie'

echo '$ print() { printf '\''%s\n'\'' "$@"; }'
print() { printf '%s\n' "$@"; } 2>&1 ||
echo 'command failed: print() { printf '\''%s\n'\'' "$@"; }'

echo '$ set -o pipefail'
set -o pipefail 2>&1 ||
echo 'command failed: set -o pipefail'

echo '$ ../lib/test-trie -G --expanded-path < filenames.txt'
../lib/test-trie -G --expanded-path < filenames.txt 2>&1 ||
echo 'command failed: ../lib/test-trie -G --expanded-path < filenames.txt'
)

