#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C type-gen-def-gcc:node-dict-expr
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L node-dict-expr.old <(echo \
'$ set -o pipefail
$ json() { ../lib/test-gen --dict -T -d1 -k4 -e "$1"|LD_LIBRARY_PATH=../lib ../src/json -Td|sed -r $'\''1i\\\\\\#include "json-type.h"\n'\'''\'';s/^#/\\#/;/^\s*\/\//d;/^\s*$/d'\''|tee /dev/fd/2|sed -r '\''s/^\\//'\''|gcc -Wall -Wextra -std=gnu99 -I ../lib -xc -c - -o /dev/null; }
$ json '\''0'\''
\#include "json-type.h"
\#define EQ  cell.eq
\#define VAL cell.val
static const struct json_type_dict_expr_t __0 = {
    .text = (const uchar_t*) "0",
};
static const struct json_type_node_t __1 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __2 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __3 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __4 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_dict_trie_node_t __5 = {
    .sym = '\''\0'\'',
    .VAL = 0
};
static const struct json_type_dict_trie_node_t __6 = {
    .sym = '\''0'\'',
    .EQ = &__5
};
static const struct json_type_dict_trie_node_t __7 = {
    .sym = '\''\0'\'',
    .VAL = 1
};
static const struct json_type_dict_trie_node_t __8 = {
    .sym = '\''1'\'',
    .lo = &__6,
    .EQ = &__7
};
static const struct json_type_dict_trie_node_t __9 = {
    .sym = '\''\0'\'',
    .VAL = 2
};
static const struct json_type_dict_trie_node_t __10 = {
    .sym = '\''\0'\'',
    .VAL = 3
};
static const struct json_type_dict_trie_node_t __11 = {
    .sym = '\''3'\'',
    .EQ = &__10
};
static const struct json_type_dict_trie_node_t __12 = {
    .sym = '\''2'\'',
    .lo = &__8,
    .EQ = &__9,
    .hi = &__11
};
static const struct json_type_dict_trie_t __13 = {
    .root = &__12
};
static const struct bit_set_t __14 = {
    .size = 4,
    .u.val = 0x0ul
};
static const struct bit_set_t* __15[] = {
    &__14,
    NULL
};
static const struct json_type_dict_attr_t __16 = {
    .args = &__13,
    .expr = __15,
    .size = 1
};
static const struct json_type_dict_node_arg_t __17[] = {
    {
        .name = (const uchar_t*) "0",
        .type = &__1
    },
    {
        .name = (const uchar_t*) "1",
        .type = &__2
    },
    {
        .name = (const uchar_t*) "2",
        .type = &__3
    },
    {
        .name = (const uchar_t*) "3",
        .type = &__4
    }
};
static const struct json_type_node_t __18 = {
    .type = json_type_dict_node_type,
    .attr.dict = &__16,
    .node.dict = {
        .expr = &__0,
        .args = __17,
        .size = 4
    }
};
static const struct json_type_def_t MODULE_TYPE_DEF = {
    .type = json_type_def_node_type,
    .val.node = &__18
};
$ json '\''1'\''
\#include "json-type.h"
\#define EQ  cell.eq
\#define VAL cell.val
static const struct json_type_dict_expr_t __0 = {
    .text = (const uchar_t*) "1",
};
static const struct json_type_node_t __1 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __2 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __3 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_node_t __4 = {
    .type = json_type_any_node_type,
    .node.any = {
        .type = json_type_any_number_type
    }
};
static const struct json_type_dict_trie_node_t __5 = {
    .sym = '\''\0'\'',
    .VAL = 0
};
static const struct json_type_dict_trie_node_t __6 = {
    .sym = '\''0'\'',
    .EQ = &__5
};
static const struct json_type_dict_trie_node_t __7 = {
    .sym = '\''\0'\'',
    .VAL = 1
};
static const struct json_type_dict_trie_node_t __8 = {
    .sym = '\''1'\'',
    .lo = &__6,
    .EQ = &__7
};
static const struct json_type_dict_trie_node_t __9 = {
    .sym = '\''\0'\'',
    .VAL = 2
};
static const struct json_type_dict_trie_node_t __10 = {
    .sym = '\''\0'\'',
    .VAL = 3
};
static const struct json_type_dict_trie_node_t __11 = {
    .sym = '\''3'\'',
    .EQ = &__10
};
static const struct json_type_dict_trie_node_t __12 = {
    .sym = '\''2'\'',
    .lo = &__8,
    .EQ = &__9,
    .hi = &__11
};
static const struct json_type_dict_trie_t __13 = {
    .root = &__12
};
static const struct bit_set_t __14 = {
    .size = 4,
    .u.val = 0xful
};
static const struct bit_set_t* __15[] = {
    &__14,
    NULL
};
static const struct json_type_dict_attr_t __16 = {
    .args = &__13,
    .expr = __15,
    .size = 1
};
static const struct json_type_dict_node_arg_t __17[] = {
    {
        .name = (const uchar_t*) "0",
        .type = &__1
    },
    {
        .name = (const uchar_t*) "1",
        .type = &__2
    },
    {
        .name = (const uchar_t*) "2",
        .type = &__3
    },
    {
        .name = (const uchar_t*) "3",
        .type = &__4
    }
};
static const struct json_type_node_t __18 = {
    .type = json_type_dict_node_type,
    .attr.dict = &__16,
    .node.dict = {
        .expr = &__0,
        .args = __17,
        .size = 4
    }
};
static const struct json_type_def_t MODULE_TYPE_DEF = {
    .type = json_type_def_node_type,
    .val.node = &__18
};'
) -L node-dict-expr.new <(
echo '$ set -o pipefail'
set -o pipefail 2>&1 ||
echo 'command failed: set -o pipefail'

echo '$ json() { ../lib/test-gen --dict -T -d1 -k4 -e "$1"|LD_LIBRARY_PATH=../lib ../src/json -Td|sed -r $'\''1i\\\\\\#include "json-type.h"\n'\'''\'';s/^#/\\#/;/^\s*\/\//d;/^\s*$/d'\''|tee /dev/fd/2|sed -r '\''s/^\\//'\''|gcc -Wall -Wextra -std=gnu99 -I ../lib -xc -c - -o /dev/null; }'
json() { ../lib/test-gen --dict -T -d1 -k4 -e "$1"|LD_LIBRARY_PATH=../lib ../src/json -Td|sed -r $'1i\\\\\\#include "json-type.h"\n'';s/^#/\\#/;/^\s*\/\//d;/^\s*$/d'|tee /dev/fd/2|sed -r 's/^\\//'|gcc -Wall -Wextra -std=gnu99 -I ../lib -xc -c - -o /dev/null; } 2>&1 ||
echo 'command failed: json() { ../lib/test-gen --dict -T -d1 -k4 -e "$1"|LD_LIBRARY_PATH=../lib ../src/json -Td|sed -r $'\''1i\\\\\\#include "json-type.h"\n'\'''\'';s/^#/\\#/;/^\s*\/\//d;/^\s*$/d'\''|tee /dev/fd/2|sed -r '\''s/^\\//'\''|gcc -Wall -Wextra -std=gnu99 -I ../lib -xc -c - -o /dev/null; }'

echo '$ json '\''0'\'''
json '0' 2>&1 ||
echo 'command failed: json '\''0'\'''

echo '$ json '\''1'\'''
json '1' 2>&1 ||
echo 'command failed: json '\''1'\'''
)

