#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C type-lib:sobj-type-lib
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L sobj-type-lib.old <(echo \
'$ json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json --literal-value -V -TA "$@"|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }
$ libs/test-type/test-type.so|sed -r '\''1s/[0-9]+\.[0-9]+\.[0-9]+/x.y.z/'\''
test-type.so: library version: x.y.z
$ json -TA libs/test-type/test-type.so
[
    {
        "name": "foo",
        "type": "type"
    }
]
$ json() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }
$ json -Tv libs/test-type/test-type.so
$ json -Tc libs/test-type/test-type.so
$ json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json -V "$@"|LD_LIBRARY_PATH=../lib ../src/json --parse-only --verbose; }
$ json -Tp libs/test-type/test-type.so
$ json -Ta libs/test-type/test-type.so'
) -L sobj-type-lib.new <(
echo '$ json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json --literal-value -V -TA "$@"|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }'
json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json --literal-value -V -TA "$@"|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; } 2>&1 ||
echo 'command failed: json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json --literal-value -V -TA "$@"|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }'

echo '$ libs/test-type/test-type.so|sed -r '\''1s/[0-9]+\.[0-9]+\.[0-9]+/x.y.z/'\'''
libs/test-type/test-type.so|sed -r '1s/[0-9]+\.[0-9]+\.[0-9]+/x.y.z/' 2>&1 ||
echo 'command failed: libs/test-type/test-type.so|sed -r '\''1s/[0-9]+\.[0-9]+\.[0-9]+/x.y.z/'\'''

echo '$ json -TA libs/test-type/test-type.so'
json -TA libs/test-type/test-type.so 2>&1 ||
echo 'command failed: json -TA libs/test-type/test-type.so'

echo '$ json() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'
json() { LD_LIBRARY_PATH=../lib ../src/json "$@"; } 2>&1 ||
echo 'command failed: json() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'

echo '$ json -Tv libs/test-type/test-type.so'
json -Tv libs/test-type/test-type.so 2>&1 ||
echo 'command failed: json -Tv libs/test-type/test-type.so'

echo '$ json -Tc libs/test-type/test-type.so'
json -Tc libs/test-type/test-type.so 2>&1 ||
echo 'command failed: json -Tc libs/test-type/test-type.so'

echo '$ json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json -V "$@"|LD_LIBRARY_PATH=../lib ../src/json --parse-only --verbose; }'
json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json -V "$@"|LD_LIBRARY_PATH=../lib ../src/json --parse-only --verbose; } 2>&1 ||
echo 'command failed: json() { set -o pipefail && LD_LIBRARY_PATH=../lib ../src/json -V "$@"|LD_LIBRARY_PATH=../lib ../src/json --parse-only --verbose; }'

echo '$ json -Tp libs/test-type/test-type.so'
json -Tp libs/test-type/test-type.so 2>&1 ||
echo 'command failed: json -Tp libs/test-type/test-type.so'

echo '$ json -Ta libs/test-type/test-type.so'
json -Ta libs/test-type/test-type.so 2>&1 ||
echo 'command failed: json -Ta libs/test-type/test-type.so'
)

