#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C filter:no-groups
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L no-groups.old <(echo \
'$ json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }
$ shopt -s expand_aliases
$ alias json=json0
$ json -F
json: error: action `-F|--filterlib-exec'\'' requires at least one filter command line arguments group
command failed: json -F
$ json -f
json: error: option `-f|--filter-libs'\'' requires at least one filter command line arguments group
command failed: json -f'
) -L no-groups.new <(
echo '$ json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'
json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; } 2>&1 ||
echo 'command failed: json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'

echo '$ shopt -s expand_aliases'
shopt -s expand_aliases 2>&1 ||
echo 'command failed: shopt -s expand_aliases'

echo '$ alias json=json0'
alias json=json0 2>&1 ||
echo 'command failed: alias json=json0'

echo '$ json -F'
json -F 2>&1 ||
echo 'command failed: json -F'

echo '$ json -f'
json -f 2>&1 ||
echo 'command failed: json -f'
)

