#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -C filter:no-fail
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L no-fail.old <(echo \
'$ json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }
$ json() { json0 -F "$@"; }
$ json -- ../lib/test-filter.so --no-fail
test-filter.so: generic action
$ json() { json0 -f "$@" <<< '\''{"a":{"b":"c"}}'\''; }
$ json -- ../lib/test-filter.so --no-fail
/a/b=c'
) -L no-fail.new <(
echo '$ json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'
json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; } 2>&1 ||
echo 'command failed: json0() { LD_LIBRARY_PATH=../lib ../src/json "$@"; }'

echo '$ json() { json0 -F "$@"; }'
json() { json0 -F "$@"; } 2>&1 ||
echo 'command failed: json() { json0 -F "$@"; }'

echo '$ json -- ../lib/test-filter.so --no-fail'
json -- ../lib/test-filter.so --no-fail 2>&1 ||
echo 'command failed: json -- ../lib/test-filter.so --no-fail'

echo '$ json() { json0 -f "$@" <<< '\''{"a":{"b":"c"}}'\''; }'
json() { json0 -f "$@" <<< '{"a":{"b":"c"}}'; } 2>&1 ||
echo 'command failed: json() { json0 -f "$@" <<< '\''{"a":{"b":"c"}}'\''; }'

echo '$ json -- ../lib/test-filter.so --no-fail'
json -- ../lib/test-filter.so --no-fail 2>&1 ||
echo 'command failed: json -- ../lib/test-filter.so --no-fail'
)

