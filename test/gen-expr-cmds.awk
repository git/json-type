#!/bin/awk -f

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

function error(s)
{
    printf("error:%d: %s\n", FNR, s) > "/dev/stderr"
    exit 1
}

function assert(v, m)
{
    if (!v) error(sprintf("assertion failed: %s", m))
}

function unexpect_var(f, v)
{
    error(sprintf("unexpected " f "\n", v))
}

function insert_sort(a, n,  i, j, t)
{
    for (i = 2; i <= n; i ++) {
        t = a[i]
        for (j = i - 1; j > 0 && t < a[j]; j --)
            a[j + 1] = a[j]
        a[j + 1] = t
    }
}

BEGIN {
    o = split(oper, O, /,/)
    assert(o == 4, "o == 4")

    m = int(m)
    assert(m == 3 || m == 4, \
          "m == 3 || m == 4")

    r = m == 3 \
        ? "^[0-2],[0-2],[0-2]$" \
        : "^[0-3],[0-3],[0-3],[0-3]$"

    if (length(nots)) {
        n = split(nots, N, /,/)
        assert(n > 0, "n > 0")
        assert(n < m, "n < m")

        for (i = 1; i <= n; i ++) {
            N[i] = int(N[i])
            assert(N[i] > 0, "N[i] > 0")
            assert(N[i] < m, "N[i] < m")
        }

        insert_sort(N, n)

        for (i = 1; i < n; i ++) {
            assert(N[i] != N[i + 1], \
                  "N[i] != N[i + 1]")
        }
    }

    FS = "\n"
}

function gen_diff_cmd3(A,	a, b, c, d, i)
{
    for (a = 0x61; a >= 0x41; a -= 0x20)   # 'a' 'A'
    for (b = 0x62; b >= 0x42; b -= 0x20)   # 'b' 'B'
    for (c = 0x63; c >= 0x43; c -= 0x20)   # 'c' 'C'
    for (d = 0x64; d >= 0x44; d -= 0x20) { # 'd' 'D'
        printf("test-expr '" E[1] "'\n", \
            a, O[A[P[1]]], b, O[A[P[2]]], c, O[A[P[3]]], d)

        for (i = 2; i <= e; i ++)
            printf("diff-expr '" E[1] "' '" E[i] "'\n", \
                a, O[A[P[1]]], b, O[A[P[2]]], c, O[A[P[3]]], d, \
                a, O[A[P[1]]], b, O[A[P[2]]], c, O[A[P[3]]], d)
    }
}

function gen_exec_cmd3(A,	i, j, k, l)
{
    for (i = 0; i < 3; i ++)
    for (j = 0; j < 3; j ++)
    for (k = 0; k < 3; k ++)
    for (l = 0; l < 3; l ++) {
        printf("test-expr '" E[1] "' -a %d,%d,%d,%d\n", \
            "a", O[A[P[1]]], "b", O[A[P[2]]], "c", O[A[P[3]]], "d", \
            i, j, k, l)
    }
}

function gen_diff_cmd4(	i)
{
    printf("test-expr '" E[1] "'\n", \
        "a", O[P[1]], "b", O[P[2]], "c", O[P[3]], "d", O[P[4]], "e")

    for (i = 2; i <= e; i ++)
        printf("diff-expr '" E[1] "' '" E[i] "'\n", \
            "a", O[P[1]], "b", O[P[2]], "c", O[P[3]], "d", O[P[4]], "e", \
            "a", O[P[1]], "b", O[P[2]], "c", O[P[3]], "d", O[P[4]], "e")
}

function gen_exec_cmd4(	i, j, k, l, m)
{
    for (i = 0; i < 3; i ++)
    for (j = 0; j < 3; j ++)
    for (k = 0; k < 3; k ++)
    for (l = 0; l < 3; l ++)
    for (m = 0; m < 3; m ++) {
        printf("test-expr '" E[1] "' -a %d,%d,%d,%d,%d\n", \
            "a", O[P[1]], "b", O[P[2]], "c", O[P[3]], "d", O[P[4]], "e", \
            i, j, k, l, m)
    }
}

function gen_cmd3(A)
{ if (exec) gen_exec_cmd3(A); else gen_diff_cmd3(A) }

function gen_cmd4()
{ if (exec) gen_exec_cmd4(); else gen_diff_cmd4() }

{
    if (match($1, r)) {
        p = split($1, P, /,/)
        assert(p == m, "p == m")

        for (i = 1; i <= m; i ++)
            P[i] = int(P[i]) + (m > 3)
    }
    else
    if (match($1, /^[(x]/)) {
        e = split($1, E, /[ \t]+/)
        if (exec || n)
            assert(e == 1, "e == 1")
        else
            assert(e > 0, "e > 0")

        for (i = 1; i <= e; i ++) {
            E[i] = \
                gensub(/\./, "", "g", \
                gensub(/o/, " %s ", "g", \
                gensub(/x/, "%c", "g", E[i])))
        }

        for (i = 1; i <= n; i ++) {
            E[1] = \
                gensub(/\(/, "!(", N[i], E[1])
        }

        if (m == 3) {
            A[0] = 1; A[1] = 2; A[2] = 3
            gen_cmd3(A)

            A[0] = 1; A[1] = 2; A[2] = 4
            gen_cmd3(A)

            A[0] = 1; A[1] = 3; A[2] = 4
            gen_cmd3(A)

            A[0] = 2; A[1] = 3; A[2] = 4
            gen_cmd3(A)
        }
        else
        if (m == 4)
            gen_cmd4()
        else
            unexpect_var("m=%d", m)
    }
    else
        error("invalid line")
}


