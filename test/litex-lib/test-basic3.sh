#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -p litex -C lib:basic3
#

[[ "$1" =~ ^-u[0-9]+$ ]] &&
u="${1:2}" ||
u=""

diff -u$u -L basic3.old <(echo \
'$ [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"
$ json-litex() { set -o pipefail && LD_LIBRARY_PATH=../lib:"$JSON_LITEX_TEST_PCRE2_LIB" ../src/json --literal-value -VF -- ../lib/json-litex.so -A "$@"|sed -r '\''s/"size":[0-9]+,"bytes":\["[0-9a-f]+"(,"[0-9a-f]+")*\]//;s/("lookup":"0x)[0-9a-f]+(")/\1...\2/g'\''|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }
$ json-litex -d '\''{"bar":[null]}'\''
json: error: <text>:1:9: filter library: meta error: values must be either strings, objects or arrays
json: error: <text>:1:9: {"bar":[null]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[null]}'\''
$ json-litex -d '\''{"bar":[false]}'\''
json: error: <text>:1:9: filter library: meta error: values must be either strings, objects or arrays
json: error: <text>:1:9: {"bar":[false]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[false]}'\''
$ json-litex -d '\''{"bar":[true]}'\''
json: error: <text>:1:9: filter library: meta error: values must be either strings, objects or arrays
json: error: <text>:1:9: {"bar":[true]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[true]}'\''
$ json-litex -d '\''{"bar":[123]}'\''
json: error: <text>:1:9: filter library: meta error: values must be either strings, objects or arrays
json: error: <text>:1:9: {"bar":[123]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[123]}'\''
$ json-litex -d '\''{"bar":[""]}'\''
json: error: <text>:1:9: filter library: attribute error: invalid string: is empty
json: error: <text>:1:9: {"bar":[""]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[""]}'\''
$ json-litex -d '\''{"bar":["``"]}'\''
json: error: <text>:1:10: filter library: expression error: empty string
json: error: <text>:1:10: {"bar":["``"]}
json: error: <text>:1:10:          ^
command failed: json-litex -d '\''{"bar":["``"]}'\''
$ json-litex -d '\''{"bar":["//"]}'\''
json: error: <text>:1:10: filter library: expression error: empty regex
json: error: <text>:1:10: {"bar":["//"]}
json: error: <text>:1:10:          ^
command failed: json-litex -d '\''{"bar":["//"]}'\''
$ json-litex -d '\''{"bar":["'\''\'\'''\'''\''\'\'''\''"]}'\''
json: error: <text>:1:10: filter library: expression error: empty regex
json: error: <text>:1:10: {"bar":["'\'''\''"]}
json: error: <text>:1:10:          ^
command failed: json-litex -d '\''{"bar":["'\''\'\'''\'''\''\'\'''\''"]}'\''
$ json-litex -d '\''{"bar":["foo"]}'\''
json: error: <text>:1:10: filter library: expression error: unknown builtin
json: error: <text>:1:10: {"bar":["foo"]}
json: error: <text>:1:10:          ^
command failed: json-litex -d '\''{"bar":["foo"]}'\''
$ json-litex -d '\''{"bar":["`foo`"]}'\''
{
    "rexes": null,
    "node": {
        "id": 9,
        "type": "object",
        "node": {
            "size": 1,
            "args": [
                {
                    "key": {
                        "val": "bar",
                        "delim": null
                    },
                    "val": {
                        "id": 5,
                        "type": "array",
                        "node": {
                            "size": 1,
                            "args": [
                                {
                                    "id": 2,
                                    "type": "string",
                                    "node": {
                                        "val": "`foo`",
                                        "delim": null
                                    },
                                    "attr": {
                                        "size": 1,
                                        "nodes": [
                                            {
                                                "type": "match_str",
                                                "val": "foo"
                                            }
                                        ]
                                    },
                                    "parent": 5,
                                    "path": null
                                }
                            ]
                        },
                        "attr": {
                            "string": 2,
                            "object": null
                        },
                        "parent": 9,
                        "path": 0
                    }
                }
            ]
        },
        "attr": {
            "lookup": "0x...",
            "root": {
                "sym": "b",
                "lo": null,
                "eq": {
                    "sym": "a",
                    "lo": null,
                    "eq": {
                        "sym": "r",
                        "lo": null,
                        "eq": {
                            "val": [
                                "`foo`"
                            ],
                            "lo": null,
                            "hi": null
                        },
                        "hi": null
                    },
                    "hi": null
                },
                "hi": null
            }
        },
        "parent": null,
        "path": null
    }
}
$ json-litex -d '\''{"bar":["/foo/"]}'\''
{
    "rexes": {
        "size": 1,
        "codes": {
            "type": "patterns",
            "code": {}
        },
        "elems": [
            {
                "text": "foo",
                "opts": ""
            }
        ]
    },
    "node": {
        "id": 12,
        "type": "object",
        "node": {
            "size": 1,
            "args": [
                {
                    "key": {
                        "val": "bar",
                        "delim": null
                    },
                    "val": {
                        "id": 8,
                        "type": "array",
                        "node": {
                            "size": 1,
                            "args": [
                                {
                                    "id": 5,
                                    "type": "string",
                                    "node": {
                                        "val": "/foo/",
                                        "delim": null
                                    },
                                    "attr": {
                                        "size": 1,
                                        "nodes": [
                                            {
                                                "type": "match_rex",
                                                "val": 0
                                            }
                                        ]
                                    },
                                    "parent": 8,
                                    "path": null
                                }
                            ]
                        },
                        "attr": {
                            "string": 5,
                            "object": null
                        },
                        "parent": 12,
                        "path": 0
                    }
                }
            ]
        },
        "attr": {
            "lookup": "0x...",
            "root": {
                "sym": "b",
                "lo": null,
                "eq": {
                    "sym": "a",
                    "lo": null,
                    "eq": {
                        "sym": "r",
                        "lo": null,
                        "eq": {
                            "val": [
                                "/foo/"
                            ],
                            "lo": null,
                            "hi": null
                        },
                        "hi": null
                    },
                    "hi": null
                },
                "hi": null
            }
        },
        "parent": null,
        "path": null
    }
}
$ json-litex -d '\''{"bar":["'\''\'\'''\''foo'\''\'\'''\''"]}'\''
{
    "rexes": {
        "size": 1,
        "codes": {
            "type": "patterns",
            "code": {}
        },
        "elems": [
            {
                "text": "foo",
                "opts": ""
            }
        ]
    },
    "node": {
        "id": 12,
        "type": "object",
        "node": {
            "size": 1,
            "args": [
                {
                    "key": {
                        "val": "bar",
                        "delim": null
                    },
                    "val": {
                        "id": 8,
                        "type": "array",
                        "node": {
                            "size": 1,
                            "args": [
                                {
                                    "id": 5,
                                    "type": "string",
                                    "node": {
                                        "val": "'\''foo'\''",
                                        "delim": null
                                    },
                                    "attr": {
                                        "size": 1,
                                        "nodes": [
                                            {
                                                "type": "match_rex",
                                                "val": 0
                                            }
                                        ]
                                    },
                                    "parent": 8,
                                    "path": null
                                }
                            ]
                        },
                        "attr": {
                            "string": 5,
                            "object": null
                        },
                        "parent": 12,
                        "path": 0
                    }
                }
            ]
        },
        "attr": {
            "lookup": "0x...",
            "root": {
                "sym": "b",
                "lo": null,
                "eq": {
                    "sym": "a",
                    "lo": null,
                    "eq": {
                        "sym": "r",
                        "lo": null,
                        "eq": {
                            "val": [
                                "'\''foo'\''"
                            ],
                            "lo": null,
                            "hi": null
                        },
                        "hi": null
                    },
                    "hi": null
                },
                "hi": null
            }
        },
        "parent": null,
        "path": null
    }
}
$ json-litex -d '\''{"bar":[{}]}'\''
json: error: <text>:1:9: filter library: meta error: empty objects are not allowed
json: error: <text>:1:9: {"bar":[{}]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[{}]}'\''
$ json-litex -d '\''{"bar":[[]]}'\''
json: error: <text>:1:9: filter library: meta error: empty arrays are not allowed
json: error: <text>:1:9: {"bar":[[]]}
json: error: <text>:1:9:         ^
command failed: json-litex -d '\''{"bar":[[]]}'\'''
) -L basic3.new <(
echo '$ [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"'
[ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib" 2>&1 ||
echo 'command failed: [ -n "$JSON_LITEX_TEST_PCRE2_LIB" ] || JSON_LITEX_TEST_PCRE2_LIB="/usr/local/lib"'

echo '$ json-litex() { set -o pipefail && LD_LIBRARY_PATH=../lib:"$JSON_LITEX_TEST_PCRE2_LIB" ../src/json --literal-value -VF -- ../lib/json-litex.so -A "$@"|sed -r '\''s/"size":[0-9]+,"bytes":\["[0-9a-f]+"(,"[0-9a-f]+")*\]//;s/("lookup":"0x)[0-9a-f]+(")/\1...\2/g'\''|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }'
json-litex() { set -o pipefail && LD_LIBRARY_PATH=../lib:"$JSON_LITEX_TEST_PCRE2_LIB" ../src/json --literal-value -VF -- ../lib/json-litex.so -A "$@"|sed -r 's/"size":[0-9]+,"bytes":\["[0-9a-f]+"(,"[0-9a-f]+")*\]//;s/("lookup":"0x)[0-9a-f]+(")/\1...\2/g'|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; } 2>&1 ||
echo 'command failed: json-litex() { set -o pipefail && LD_LIBRARY_PATH=../lib:"$JSON_LITEX_TEST_PCRE2_LIB" ../src/json --literal-value -VF -- ../lib/json-litex.so -A "$@"|sed -r '\''s/"size":[0-9]+,"bytes":\["[0-9a-f]+"(,"[0-9a-f]+")*\]//;s/("lookup":"0x)[0-9a-f]+(")/\1...\2/g'\''|LD_LIBRARY_PATH=../lib ../src/json --literal-value --from-ast-print --verbose --no-error; }'

echo '$ json-litex -d '\''{"bar":[null]}'\'''
json-litex -d '{"bar":[null]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[null]}'\'''

echo '$ json-litex -d '\''{"bar":[false]}'\'''
json-litex -d '{"bar":[false]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[false]}'\'''

echo '$ json-litex -d '\''{"bar":[true]}'\'''
json-litex -d '{"bar":[true]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[true]}'\'''

echo '$ json-litex -d '\''{"bar":[123]}'\'''
json-litex -d '{"bar":[123]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[123]}'\'''

echo '$ json-litex -d '\''{"bar":[""]}'\'''
json-litex -d '{"bar":[""]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[""]}'\'''

echo '$ json-litex -d '\''{"bar":["``"]}'\'''
json-litex -d '{"bar":["``"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["``"]}'\'''

echo '$ json-litex -d '\''{"bar":["//"]}'\'''
json-litex -d '{"bar":["//"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["//"]}'\'''

echo '$ json-litex -d '\''{"bar":["'\''\'\'''\'''\''\'\'''\''"]}'\'''
json-litex -d '{"bar":["'\'''\''"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["'\''\'\'''\'''\''\'\'''\''"]}'\'''

echo '$ json-litex -d '\''{"bar":["foo"]}'\'''
json-litex -d '{"bar":["foo"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["foo"]}'\'''

echo '$ json-litex -d '\''{"bar":["`foo`"]}'\'''
json-litex -d '{"bar":["`foo`"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["`foo`"]}'\'''

echo '$ json-litex -d '\''{"bar":["/foo/"]}'\'''
json-litex -d '{"bar":["/foo/"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["/foo/"]}'\'''

echo '$ json-litex -d '\''{"bar":["'\''\'\'''\''foo'\''\'\'''\''"]}'\'''
json-litex -d '{"bar":["'\''foo'\''"]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":["'\''\'\'''\''foo'\''\'\'''\''"]}'\'''

echo '$ json-litex -d '\''{"bar":[{}]}'\'''
json-litex -d '{"bar":[{}]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[{}]}'\'''

echo '$ json-litex -d '\''{"bar":[[]]}'\'''
json-litex -d '{"bar":[[]]}' 2>&1 ||
echo 'command failed: json-litex -d '\''{"bar":[[]]}'\'''
)

