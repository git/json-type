#!/bin/bash

# Copyright (C) 2016, 2017, 2018, 2019, 2020, 2021  Stefan Vargyas
# 
# This file is part of Json-Type.
# 
# Json-Type is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Json-Type is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Json-Type.  If not, see <http://www.gnu.org/licenses/>.

#
# File generated by a command like:
# $ json-gentest -w39 -G trie
#

w=39
c=""
q=""

if [[ "$1" =~ ^-w[1-9][0-9]*$ ]]; then
    w="${1:2}"
    shift
fi
if [ "$1" == "-c" ]; then
    c="c"
    shift
fi
if [ "$1" == "-q" ]; then
    q="q"
fi

p=0
f=0

for t in \
    empty \
    base \
    shuf \
    depths \
    sgb-words \
    rebalance \
    rebalance2 \
    gen-code-expanded-base0 \
    gen-code-expanded-base \
    gen-code-expanded-filenames \
    gen-code-function-base0 \
    gen-code-function-base \
    gen-code-function-filenames \
    gen-code-function-strcmp-base0 \
    gen-code-function-strcmp-base \
    gen-code-function-strcmp-filenames
do
    test -z "$q" &&
    printf >&2 "%-$((w + 6))s " "test: trie:$t"
    if ! trie/test-$t.sh &>/dev/null; then
        (( f ++ ))

        test -n "$q" &&
        echo >&2 -n "test: trie:$t "
        echo >&2 failed
    else
        (( p ++ ))

        test -z "$q" &&
        echo >&2 OK
    fi
done

if [ -z "$c" ]; then
    [ -z "$q" -o "$f" -gt 0 ] &&
    echo >&2
    echo "tests passed: $p"
    echo "tests failed: $f"
else
    echo "$p"
    echo "$f"
fi

exit $(( f != 0 ))

